﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;
using System.IO;
using DevExpress.XtraEditors;

namespace RAkademia_v1.Raporty
{
    public partial class DesignerForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public DesignerForm(/*object datasource = null,*/ XtraReport report = null,String reportName=null,bool CustomSave=false, string directory = null)
        {
            InitializeComponent();
            reportDesigner1.DesignPanelLoaded += (s, e) =>
            {
                /*if (s is XRDesignPanel panel)
                {
                    panel.Report.DataSource = datasource;
                }*/
            };
            DevExpress.XtraReports.Configuration.Settings.Default.StorageOptions.RootDirectory = Path.Combine(Application.StartupPath, "Raporty");
            if (CustomSave)
            {
                _name = reportName;
                reportDesigner1.DesignPanelLoaded += mdiController_DesignPanelLoaded;
            }
            if (report != null)
                reportDesigner1.OpenReport(report);
        }
        private static string _name;
        private static void mdiController_DesignPanelLoaded(object sender, DesignerLoadedEventArgs e)
        {
            var panel = (XRDesignPanel)sender;
            panel.AddCommandHandler(new SaveCommandHandler(panel, _name));
        }
    }
    public class SaveCommandHandler : ICommandHandler
    {
        private readonly XRDesignPanel _panel;
        public String _name;
        public SaveCommandHandler(XRDesignPanel panel,String name)
        {
            _panel = panel;
            _name = name;
        }

        public void HandleCommand(ReportCommand command, object[] args)
        {
            Save();
        }

        public bool CanHandleCommand(ReportCommand command, ref bool useNextHandler)
        {
            useNextHandler = !(command == ReportCommand.SaveFile || command == ReportCommand.SaveFileAs);
            return !useNextHandler;
        }

        private void Save()
        {
            _panel.Report.SaveLayoutToXml(Path.Combine(Application.StartupPath, "Raporty",_name));
            _panel.ReportState = ReportState.Saved;
            XtraMessageBox.Show(
                    "Raport Został zapisany",
                    "Zapisano!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

    }
}
