﻿namespace RAkademia_v1
{
    partial class RaportViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener1 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener2 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener3 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener4 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener5 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener6 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener7 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.XRDesignPanelListener xrDesignPanelListener8 = new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener();
            DevExpress.XtraReports.UserDesigner.RecentlyUsedItemsComboBox recentlyUsedItemsComboBox2 = new DevExpress.XtraReports.UserDesigner.RecentlyUsedItemsComboBox();
            DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox designRepositoryItemComboBox3 = new DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RaportViewerForm));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.documentViewer1 = new DevExpress.XtraPrinting.Preview.DocumentViewer();
            this.reportDesigner1 = new DevExpress.XtraReports.UserDesigner.XRDesignMdiController(this.components);
            this.xrDesignBarManager1 = new DevExpress.XtraReports.UserDesigner.XRDesignBarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelContainer3 = new DevExpress.XtraBars.Docking.DockPanel();
            this.panelContainer2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.designControlContainer1 = new DevExpress.XtraReports.UserDesigner.DesignControlContainer();
            this.errorListDockPanel1 = new DevExpress.XtraReports.UserDesigner.ErrorListDockPanel();
            this.groupAndSortDockPanel1 = new DevExpress.XtraReports.UserDesigner.GroupAndSortDockPanel();
            this.reportGalleryDockPanel1 = new DevExpress.XtraReports.UserDesigner.ReportGalleryDockPanel();
            this.reportExplorerDockPanel1 = new DevExpress.XtraReports.UserDesigner.ReportExplorerDockPanel();
            this.propertyGridDockPanel1 = new DevExpress.XtraReports.UserDesigner.PropertyGridDockPanel();
            this.fieldListDockPanel1 = new DevExpress.XtraReports.UserDesigner.FieldListDockPanel();
            this.xrDesignDockManager1 = new DevExpress.XtraReports.UserDesigner.XRDesignDockManager(this.components);
            this.bbiZoomIn = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiZoomOut = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem7 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem5 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem4 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem3 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem2 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiRedo = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiUndo = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiPaste = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiCopy = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiCut = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiSaveFile = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiOpenFile = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem1 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiSendToBack = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiBringToFront = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiCenterVertically = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiCenterHorizontally = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiVertSpaceConcatenate = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiVertSpaceDecrease = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiVertSpaceIncrease = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiVertSpaceMakeEqual = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiHorizSpaceConcatenate = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiHorizSpaceDecrease = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiHorizSpaceIncrease = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiHorizSpaceMakeEqual = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiSizeToControl = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiSizeToControlHeight = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiSizeToGrid = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiSizeToControlWidth = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiAlignBottom = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiAlignHorizontalCenters = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiAlignTop = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiAlignRight = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiAlignVerticalCenters = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiAlignLeft = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiAlignToGrid = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiJustifyJustify = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiJustifyRight = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiJustifyCenter = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiJustifyLeft = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiBackColor = new DevExpress.XtraReports.UserDesigner.CommandColorBarItem();
            this.bbiForeColor = new DevExpress.XtraReports.UserDesigner.CommandColorBarItem();
            this.bbiFontUnderline = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiFontItalic = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bbiFontBold = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.designRepositoryItemComboBox2 = new DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox();
            this.bbiZoom = new DevExpress.XtraReports.UserDesigner.XRZoomBarEditItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.commandBarItem11 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.msiWindows = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.commandBarItem10 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem9 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.commandBarItem8 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.msiWindow = new DevExpress.XtraBars.BarSubItem();
            this.msiWindowInterface = new DevExpress.XtraReports.UserDesigner.CommandBarCheckItem();
            this.msiOrder = new DevExpress.XtraBars.BarSubItem();
            this.bsiCenter = new DevExpress.XtraBars.BarSubItem();
            this.msiVerticalSpacing = new DevExpress.XtraBars.BarSubItem();
            this.msiHorizontalSpacing = new DevExpress.XtraBars.BarSubItem();
            this.msiSameSize = new DevExpress.XtraBars.BarSubItem();
            this.msiAlign = new DevExpress.XtraBars.BarSubItem();
            this.msiJustify = new DevExpress.XtraBars.BarSubItem();
            this.msiFont = new DevExpress.XtraBars.BarSubItem();
            this.msiEdit = new DevExpress.XtraBars.BarSubItem();
            this.msiFile = new DevExpress.XtraBars.BarSubItem();
            this.commandBarItem6 = new DevExpress.XtraReports.UserDesigner.CommandBarItem();
            this.msiFormat = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barDockPanelsListItem1 = new DevExpress.XtraReports.UserDesigner.BarDockPanelsListItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.xrBarToolbarsListItem1 = new DevExpress.XtraReports.UserDesigner.XRBarToolbarsListItem();
            this.msiTabButtons = new DevExpress.XtraBars.BarSubItem();
            this.barReportTabButtonsListItem1 = new DevExpress.XtraReports.UserDesigner.BarReportTabButtonsListItem();
            this.bsiHint = new DevExpress.XtraBars.BarStaticItem();
            this.beiFontSize = new DevExpress.XtraBars.BarEditItem();
            this.beiFontName = new DevExpress.XtraBars.BarEditItem();
            this.designRepositoryItemComboBox1 = new DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox();
            this.recentlyUsedItemsComboBox1 = new DevExpress.XtraReports.UserDesigner.RecentlyUsedItemsComboBox();
            this.designBar5 = new DevExpress.XtraReports.UserDesigner.DesignBar();
            this.designBar4 = new DevExpress.XtraReports.UserDesigner.DesignBar();
            this.designBar3 = new DevExpress.XtraReports.UserDesigner.DesignBar();
            this.designBar2 = new DevExpress.XtraReports.UserDesigner.DesignBar();
            this.designBar1 = new DevExpress.XtraReports.UserDesigner.DesignBar();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrDesignBarManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(recentlyUsedItemsComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(designRepositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrDesignDockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.designRepositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.designRepositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recentlyUsedItemsComboBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 1;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.Size = new System.Drawing.Size(1017, 144);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 525);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1017, 32);
            // 
            // documentViewer1
            // 
            this.documentViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.documentViewer1.IsMetric = true;
            this.documentViewer1.Location = new System.Drawing.Point(0, 144);
            this.documentViewer1.Name = "documentViewer1";
            this.documentViewer1.Size = new System.Drawing.Size(1017, 381);
            this.documentViewer1.TabIndex = 2;
            // 
            // reportDesigner1
            // 
            this.reportDesigner1.ContainerControl = null;
            xrDesignPanelListener1.DesignControl = this.xrDesignBarManager1;
            xrDesignPanelListener2.DesignControl = this.errorListDockPanel1;
            xrDesignPanelListener3.DesignControl = this.groupAndSortDockPanel1;
            xrDesignPanelListener4.DesignControl = this.reportGalleryDockPanel1;
            xrDesignPanelListener5.DesignControl = this.reportExplorerDockPanel1;
            xrDesignPanelListener6.DesignControl = this.propertyGridDockPanel1;
            xrDesignPanelListener7.DesignControl = this.fieldListDockPanel1;
            xrDesignPanelListener8.DesignControl = this.xrDesignDockManager1;
            this.reportDesigner1.DesignPanelListeners.AddRange(new DevExpress.XtraReports.UserDesigner.XRDesignPanelListener[] {
            xrDesignPanelListener1,
            xrDesignPanelListener2,
            xrDesignPanelListener3,
            xrDesignPanelListener4,
            xrDesignPanelListener5,
            xrDesignPanelListener6,
            xrDesignPanelListener7,
            xrDesignPanelListener8});
            this.reportDesigner1.Form = this;
            // 
            // xrDesignBarManager1
            // 
            this.xrDesignBarManager1.DockControls.Add(this.barDockControlTop);
            this.xrDesignBarManager1.DockControls.Add(this.barDockControlBottom);
            this.xrDesignBarManager1.DockControls.Add(this.barDockControlLeft);
            this.xrDesignBarManager1.DockControls.Add(this.barDockControlRight);
            recentlyUsedItemsComboBox2.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11.25F);
            recentlyUsedItemsComboBox2.AppearanceDropDown.Options.UseFont = true;
            recentlyUsedItemsComboBox2.AutoHeight = false;
            recentlyUsedItemsComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            recentlyUsedItemsComboBox2.DropDownRows = 12;
            recentlyUsedItemsComboBox2.LookAndFeel.SkinName = "Office 2010 Blue";
            this.xrDesignBarManager1.FontNameBox = recentlyUsedItemsComboBox2;
            designRepositoryItemComboBox3.AutoHeight = false;
            designRepositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.xrDesignBarManager1.FontSizeBox = designRepositoryItemComboBox3;
            this.xrDesignBarManager1.Form = this;
            this.xrDesignBarManager1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("xrDesignBarManager1.ImageStream")));
            this.xrDesignBarManager1.MaxItemId = 76;
            this.xrDesignBarManager1.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            this.xrDesignBarManager1.Updates.AddRange(new string[] {
            "Toolbox"});
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.xrDesignBarManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1017, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Manager = this.xrDesignBarManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1017, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.xrDesignBarManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 557);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1017, 0);
            this.barDockControlRight.Manager = this.xrDesignBarManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 557);
            // 
            // panelContainer3
            // 
            this.panelContainer3.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.panelContainer3.ID = new System.Guid("788435f2-597d-4eaf-938b-e85a3e8f407a");
            this.panelContainer3.Location = new System.Drawing.Point(0, 0);
            this.panelContainer3.Name = "panelContainer3";
            this.panelContainer3.OriginalSize = new System.Drawing.Size(0, 0);
            this.panelContainer3.SavedSizeFactor = 0D;
            this.panelContainer3.Size = new System.Drawing.Size(200, 200);
            // 
            // panelContainer2
            // 
            this.panelContainer2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.panelContainer2.ID = new System.Guid("ed4b0819-6df4-4e6b-a3c7-b89a222f19b6");
            this.panelContainer2.Location = new System.Drawing.Point(0, 0);
            this.panelContainer2.Name = "panelContainer2";
            this.panelContainer2.OriginalSize = new System.Drawing.Size(0, 0);
            this.panelContainer2.SavedSizeFactor = 0D;
            this.panelContainer2.Size = new System.Drawing.Size(200, 200);
            // 
            // dockPanel1
            // 
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel1.ID = new System.Guid("78c027d8-af11-4046-9e9a-4ad94735c696");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(0, 0);
            this.dockPanel1.SavedSizeFactor = 0D;
            this.dockPanel1.Size = new System.Drawing.Size(200, 200);
            // 
            // designControlContainer1
            // 
            this.designControlContainer1.Location = new System.Drawing.Point(0, 0);
            this.designControlContainer1.Name = "designControlContainer1";
            this.designControlContainer1.Size = new System.Drawing.Size(200, 100);
            this.designControlContainer1.TabIndex = 0;
            // 
            // errorListDockPanel1
            // 
            this.errorListDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.errorListDockPanel1.ID = new System.Guid("059b4372-f17c-4ec7-9611-b61efa9abc22");
            this.errorListDockPanel1.Location = new System.Drawing.Point(0, 0);
            this.errorListDockPanel1.Name = "errorListDockPanel1";
            this.errorListDockPanel1.OriginalSize = new System.Drawing.Size(0, 0);
            this.errorListDockPanel1.SavedSizeFactor = 0D;
            this.errorListDockPanel1.Size = new System.Drawing.Size(200, 200);
            this.errorListDockPanel1.Text = "Scripts Errors";
            // 
            // groupAndSortDockPanel1
            // 
            this.groupAndSortDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.groupAndSortDockPanel1.ID = new System.Guid("ad9847a8-fd82-4399-990d-3275e6c05af6");
            this.groupAndSortDockPanel1.Location = new System.Drawing.Point(0, 0);
            this.groupAndSortDockPanel1.Name = "groupAndSortDockPanel1";
            this.groupAndSortDockPanel1.OriginalSize = new System.Drawing.Size(0, 0);
            this.groupAndSortDockPanel1.SavedSizeFactor = 0D;
            this.groupAndSortDockPanel1.Size = new System.Drawing.Size(200, 200);
            this.groupAndSortDockPanel1.Text = "Group and Sort";
            // 
            // reportGalleryDockPanel1
            // 
            this.reportGalleryDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.reportGalleryDockPanel1.ID = new System.Guid("4ab7f09b-597c-4b81-9408-08d61c6a5d19");
            this.reportGalleryDockPanel1.Location = new System.Drawing.Point(0, 0);
            this.reportGalleryDockPanel1.Name = "reportGalleryDockPanel1";
            this.reportGalleryDockPanel1.OriginalSize = new System.Drawing.Size(0, 0);
            this.reportGalleryDockPanel1.SavedSizeFactor = 0D;
            this.reportGalleryDockPanel1.Size = new System.Drawing.Size(200, 200);
            this.reportGalleryDockPanel1.Text = "Report Gallery";
            // 
            // reportExplorerDockPanel1
            // 
            this.reportExplorerDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.reportExplorerDockPanel1.ID = new System.Guid("28336f6f-aba5-4e9d-8047-85dcc93bb7ce");
            this.reportExplorerDockPanel1.Location = new System.Drawing.Point(0, 0);
            this.reportExplorerDockPanel1.Name = "reportExplorerDockPanel1";
            this.reportExplorerDockPanel1.OriginalSize = new System.Drawing.Size(0, 0);
            this.reportExplorerDockPanel1.SavedSizeFactor = 0D;
            this.reportExplorerDockPanel1.Size = new System.Drawing.Size(200, 200);
            this.reportExplorerDockPanel1.Text = "Report Explorer";
            // 
            // propertyGridDockPanel1
            // 
            this.propertyGridDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.propertyGridDockPanel1.ID = new System.Guid("7bc06091-5a18-471c-887a-dfefc65a57d0");
            this.propertyGridDockPanel1.Location = new System.Drawing.Point(0, 0);
            this.propertyGridDockPanel1.Name = "propertyGridDockPanel1";
            this.propertyGridDockPanel1.OriginalSize = new System.Drawing.Size(0, 0);
            this.propertyGridDockPanel1.SavedSizeFactor = 0D;
            this.propertyGridDockPanel1.Size = new System.Drawing.Size(200, 200);
            this.propertyGridDockPanel1.Text = "Property Grid";
            // 
            // fieldListDockPanel1
            // 
            this.fieldListDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.fieldListDockPanel1.ID = new System.Guid("ad88ef05-5de1-4af4-8bba-b7decbe58af4");
            this.fieldListDockPanel1.Location = new System.Drawing.Point(0, 0);
            this.fieldListDockPanel1.Name = "fieldListDockPanel1";
            this.fieldListDockPanel1.OriginalSize = new System.Drawing.Size(0, 0);
            this.fieldListDockPanel1.SavedSizeFactor = 0D;
            this.fieldListDockPanel1.Size = new System.Drawing.Size(200, 200);
            this.fieldListDockPanel1.Text = "Field List";
            // 
            // xrDesignDockManager1
            // 
            this.xrDesignDockManager1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("xrDesignDockManager1.ImageStream")));
            this.xrDesignDockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // bbiZoomIn
            // 
            this.bbiZoomIn.Caption = "Zoom In";
            this.bbiZoomIn.Hint = "Zoom in the design surface";
            this.bbiZoomIn.Id = 75;
            this.bbiZoomIn.Name = "bbiZoomIn";
            // 
            // bbiZoomOut
            // 
            this.bbiZoomOut.Caption = "Zoom Out";
            this.bbiZoomOut.Hint = "Zoom out the design surface";
            this.bbiZoomOut.Id = 73;
            this.bbiZoomOut.Name = "bbiZoomOut";
            // 
            // commandBarItem7
            // 
            this.commandBarItem7.Caption = "Save A&ll";
            this.commandBarItem7.Hint = "Save all reports";
            this.commandBarItem7.Id = 65;
            this.commandBarItem7.Name = "commandBarItem7";
            // 
            // commandBarItem5
            // 
            this.commandBarItem5.Caption = "&Delete";
            this.commandBarItem5.Hint = "Delete the control";
            this.commandBarItem5.Id = 63;
            this.commandBarItem5.Name = "commandBarItem5";
            // 
            // commandBarItem4
            // 
            this.commandBarItem4.Caption = "E&xit";
            this.commandBarItem4.Hint = "Close the designer";
            this.commandBarItem4.Id = 62;
            this.commandBarItem4.Name = "commandBarItem4";
            // 
            // commandBarItem3
            // 
            this.commandBarItem3.Caption = "Save &As...";
            this.commandBarItem3.Hint = "Save the report with a new name";
            this.commandBarItem3.Id = 61;
            this.commandBarItem3.Name = "commandBarItem3";
            // 
            // commandBarItem2
            // 
            this.commandBarItem2.Caption = "New via &Wizard...";
            this.commandBarItem2.Hint = "Create a new report using the Wizard";
            this.commandBarItem2.Id = 60;
            this.commandBarItem2.Name = "commandBarItem2";
            // 
            // bbiRedo
            // 
            this.bbiRedo.Caption = "&Redo";
            this.bbiRedo.Hint = "Redo the last operation";
            this.bbiRedo.Id = 41;
            this.bbiRedo.Name = "bbiRedo";
            // 
            // bbiUndo
            // 
            this.bbiUndo.Caption = "&Undo";
            this.bbiUndo.Hint = "Undo the last operation";
            this.bbiUndo.Id = 40;
            this.bbiUndo.Name = "bbiUndo";
            // 
            // bbiPaste
            // 
            this.bbiPaste.Caption = "&Paste";
            this.bbiPaste.Hint = "Add the control from the clipboard";
            this.bbiPaste.Id = 39;
            this.bbiPaste.Name = "bbiPaste";
            // 
            // bbiCopy
            // 
            this.bbiCopy.Caption = "&Copy";
            this.bbiCopy.Hint = "Copy the control to the clipboard";
            this.bbiCopy.Id = 38;
            this.bbiCopy.Name = "bbiCopy";
            // 
            // bbiCut
            // 
            this.bbiCut.Caption = "Cu&t";
            this.bbiCut.Hint = "Delete the control and copy it to the clipboard";
            this.bbiCut.Id = 37;
            this.bbiCut.Name = "bbiCut";
            // 
            // bbiSaveFile
            // 
            this.bbiSaveFile.Caption = "&Save";
            this.bbiSaveFile.Hint = "Save the report";
            this.bbiSaveFile.Id = 36;
            this.bbiSaveFile.Name = "bbiSaveFile";
            // 
            // bbiOpenFile
            // 
            this.bbiOpenFile.Caption = "&Open...";
            this.bbiOpenFile.Hint = "Open a report";
            this.bbiOpenFile.Id = 35;
            this.bbiOpenFile.Name = "bbiOpenFile";
            // 
            // commandBarItem1
            // 
            this.commandBarItem1.Caption = "&New";
            this.commandBarItem1.Hint = "Create a new blank report";
            this.commandBarItem1.Id = 34;
            this.commandBarItem1.Name = "commandBarItem1";
            // 
            // bbiSendToBack
            // 
            this.bbiSendToBack.Caption = "&Send to Back";
            this.bbiSendToBack.Hint = "Move the selected controls to the back";
            this.bbiSendToBack.Id = 33;
            this.bbiSendToBack.Name = "bbiSendToBack";
            // 
            // bbiBringToFront
            // 
            this.bbiBringToFront.Caption = "&Bring to Front";
            this.bbiBringToFront.Hint = "Bring the selected controls to the front";
            this.bbiBringToFront.Id = 32;
            this.bbiBringToFront.Name = "bbiBringToFront";
            // 
            // bbiCenterVertically
            // 
            this.bbiCenterVertically.Caption = "&Vertically";
            this.bbiCenterVertically.Hint = "Vertically center the selected controls within a band";
            this.bbiCenterVertically.Id = 31;
            this.bbiCenterVertically.Name = "bbiCenterVertically";
            // 
            // bbiCenterHorizontally
            // 
            this.bbiCenterHorizontally.Caption = "&Horizontally";
            this.bbiCenterHorizontally.Hint = "Horizontally center the selected controls within a band";
            this.bbiCenterHorizontally.Id = 30;
            this.bbiCenterHorizontally.Name = "bbiCenterHorizontally";
            // 
            // bbiVertSpaceConcatenate
            // 
            this.bbiVertSpaceConcatenate.Caption = "&Remove";
            this.bbiVertSpaceConcatenate.Hint = "Remove the spacing between the selected controls";
            this.bbiVertSpaceConcatenate.Id = 29;
            this.bbiVertSpaceConcatenate.Name = "bbiVertSpaceConcatenate";
            // 
            // bbiVertSpaceDecrease
            // 
            this.bbiVertSpaceDecrease.Caption = "&Decrease";
            this.bbiVertSpaceDecrease.Hint = "Decrease the spacing between the selected controls";
            this.bbiVertSpaceDecrease.Id = 28;
            this.bbiVertSpaceDecrease.Name = "bbiVertSpaceDecrease";
            // 
            // bbiVertSpaceIncrease
            // 
            this.bbiVertSpaceIncrease.Caption = "&Increase";
            this.bbiVertSpaceIncrease.Hint = "Increase the spacing between the selected controls";
            this.bbiVertSpaceIncrease.Id = 27;
            this.bbiVertSpaceIncrease.Name = "bbiVertSpaceIncrease";
            // 
            // bbiVertSpaceMakeEqual
            // 
            this.bbiVertSpaceMakeEqual.Caption = "Make &Equal";
            this.bbiVertSpaceMakeEqual.Hint = "Make the spacing between the selected controls equal";
            this.bbiVertSpaceMakeEqual.Id = 26;
            this.bbiVertSpaceMakeEqual.Name = "bbiVertSpaceMakeEqual";
            // 
            // bbiHorizSpaceConcatenate
            // 
            this.bbiHorizSpaceConcatenate.Caption = "&Remove";
            this.bbiHorizSpaceConcatenate.Hint = "Remove the spacing between the selected controls";
            this.bbiHorizSpaceConcatenate.Id = 25;
            this.bbiHorizSpaceConcatenate.Name = "bbiHorizSpaceConcatenate";
            // 
            // bbiHorizSpaceDecrease
            // 
            this.bbiHorizSpaceDecrease.Caption = "&Decrease";
            this.bbiHorizSpaceDecrease.Hint = "Decrease the spacing between the selected controls";
            this.bbiHorizSpaceDecrease.Id = 24;
            this.bbiHorizSpaceDecrease.Name = "bbiHorizSpaceDecrease";
            // 
            // bbiHorizSpaceIncrease
            // 
            this.bbiHorizSpaceIncrease.Caption = "&Increase";
            this.bbiHorizSpaceIncrease.Hint = "Increase the spacing between the selected controls";
            this.bbiHorizSpaceIncrease.Id = 23;
            this.bbiHorizSpaceIncrease.Name = "bbiHorizSpaceIncrease";
            // 
            // bbiHorizSpaceMakeEqual
            // 
            this.bbiHorizSpaceMakeEqual.Caption = "Make &Equal";
            this.bbiHorizSpaceMakeEqual.Hint = "Make the spacing between the selected controls equal";
            this.bbiHorizSpaceMakeEqual.Id = 22;
            this.bbiHorizSpaceMakeEqual.Name = "bbiHorizSpaceMakeEqual";
            // 
            // bbiSizeToControl
            // 
            this.bbiSizeToControl.Caption = "&Both";
            this.bbiSizeToControl.Hint = "Make the selected controls the same size";
            this.bbiSizeToControl.Id = 21;
            this.bbiSizeToControl.Name = "bbiSizeToControl";
            // 
            // bbiSizeToControlHeight
            // 
            this.bbiSizeToControlHeight.Caption = "&Height";
            this.bbiSizeToControlHeight.Hint = "Make the selected controls have the same height";
            this.bbiSizeToControlHeight.Id = 20;
            this.bbiSizeToControlHeight.Name = "bbiSizeToControlHeight";
            // 
            // bbiSizeToGrid
            // 
            this.bbiSizeToGrid.Caption = "Size to Gri&d";
            this.bbiSizeToGrid.Hint = "Size the selected controls to the grid";
            this.bbiSizeToGrid.Id = 19;
            this.bbiSizeToGrid.Name = "bbiSizeToGrid";
            // 
            // bbiSizeToControlWidth
            // 
            this.bbiSizeToControlWidth.Caption = "&Width";
            this.bbiSizeToControlWidth.Hint = "Make the selected controls have the same width";
            this.bbiSizeToControlWidth.Id = 18;
            this.bbiSizeToControlWidth.Name = "bbiSizeToControlWidth";
            // 
            // bbiAlignBottom
            // 
            this.bbiAlignBottom.Caption = "&Bottoms";
            this.bbiAlignBottom.Hint = "Align the bottoms of the selected controls";
            this.bbiAlignBottom.Id = 17;
            this.bbiAlignBottom.Name = "bbiAlignBottom";
            // 
            // bbiAlignHorizontalCenters
            // 
            this.bbiAlignHorizontalCenters.Caption = "&Middles";
            this.bbiAlignHorizontalCenters.Hint = "Align the centers of the selected controls horizontally";
            this.bbiAlignHorizontalCenters.Id = 16;
            this.bbiAlignHorizontalCenters.Name = "bbiAlignHorizontalCenters";
            // 
            // bbiAlignTop
            // 
            this.bbiAlignTop.Caption = "&Tops";
            this.bbiAlignTop.Hint = "Align the tops of the selected controls";
            this.bbiAlignTop.Id = 15;
            this.bbiAlignTop.Name = "bbiAlignTop";
            // 
            // bbiAlignRight
            // 
            this.bbiAlignRight.Caption = "&Rights";
            this.bbiAlignRight.Hint = "Right align the selected controls";
            this.bbiAlignRight.Id = 14;
            this.bbiAlignRight.Name = "bbiAlignRight";
            // 
            // bbiAlignVerticalCenters
            // 
            this.bbiAlignVerticalCenters.Caption = "&Centers";
            this.bbiAlignVerticalCenters.Hint = "Align the centers of the selected controls vertically";
            this.bbiAlignVerticalCenters.Id = 13;
            this.bbiAlignVerticalCenters.Name = "bbiAlignVerticalCenters";
            // 
            // bbiAlignLeft
            // 
            this.bbiAlignLeft.Caption = "&Lefts";
            this.bbiAlignLeft.Hint = "Left align the selected controls";
            this.bbiAlignLeft.Id = 12;
            this.bbiAlignLeft.Name = "bbiAlignLeft";
            // 
            // bbiAlignToGrid
            // 
            this.bbiAlignToGrid.Caption = "to &Grid";
            this.bbiAlignToGrid.Hint = "Align the positions of the selected controls to the grid";
            this.bbiAlignToGrid.Id = 11;
            this.bbiAlignToGrid.Name = "bbiAlignToGrid";
            // 
            // bbiJustifyJustify
            // 
            this.bbiJustifyJustify.Caption = "&Justify";
            this.bbiJustifyJustify.Hint = "Justify the control\'s text";
            this.bbiJustifyJustify.Id = 10;
            this.bbiJustifyJustify.Name = "bbiJustifyJustify";
            // 
            // bbiJustifyRight
            // 
            this.bbiJustifyRight.Caption = "&Rights";
            this.bbiJustifyRight.Hint = "Align the control\'s text to the right";
            this.bbiJustifyRight.Id = 9;
            this.bbiJustifyRight.Name = "bbiJustifyRight";
            // 
            // bbiJustifyCenter
            // 
            this.bbiJustifyCenter.Caption = "&Center";
            this.bbiJustifyCenter.Hint = "Align the control\'s text to the center";
            this.bbiJustifyCenter.Id = 8;
            this.bbiJustifyCenter.Name = "bbiJustifyCenter";
            // 
            // bbiJustifyLeft
            // 
            this.bbiJustifyLeft.Caption = "&Left";
            this.bbiJustifyLeft.Hint = "Align the control\'s text to the left";
            this.bbiJustifyLeft.Id = 7;
            this.bbiJustifyLeft.Name = "bbiJustifyLeft";
            // 
            // bbiBackColor
            // 
            this.bbiBackColor.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiBackColor.Caption = "Bac&kground Color";
            this.bbiBackColor.CloseSubMenuOnClick = false;
            this.bbiBackColor.Hint = "Set the background color of the control";
            this.bbiBackColor.Id = 6;
            this.bbiBackColor.Name = "bbiBackColor";
            // 
            // bbiForeColor
            // 
            this.bbiForeColor.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiForeColor.Caption = "For&eground Color";
            this.bbiForeColor.CloseSubMenuOnClick = false;
            this.bbiForeColor.Hint = "Set the foreground color of the control";
            this.bbiForeColor.Id = 5;
            this.bbiForeColor.Name = "bbiForeColor";
            // 
            // bbiFontUnderline
            // 
            this.bbiFontUnderline.Caption = "&Underline";
            this.bbiFontUnderline.Hint = "Underline the font";
            this.bbiFontUnderline.Id = 4;
            this.bbiFontUnderline.Name = "bbiFontUnderline";
            // 
            // bbiFontItalic
            // 
            this.bbiFontItalic.Caption = "&Italic";
            this.bbiFontItalic.Hint = "Make the font italic";
            this.bbiFontItalic.Id = 3;
            this.bbiFontItalic.Name = "bbiFontItalic";
            // 
            // bbiFontBold
            // 
            this.bbiFontBold.Caption = "&Bold";
            this.bbiFontBold.Hint = "Make the font bold";
            this.bbiFontBold.Id = 2;
            this.bbiFontBold.Name = "bbiFontBold";
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 7";
            this.bar2.DockCol = 0;
            this.bar2.HideWhenMerging = DevExpress.Utils.DefaultBoolean.True;
            this.bar2.Text = "Custom 7";
            // 
            // designRepositoryItemComboBox2
            // 
            this.designRepositoryItemComboBox2.Name = "designRepositoryItemComboBox2";
            // 
            // bbiZoom
            // 
            this.bbiZoom.Caption = "Zoom";
            this.bbiZoom.Edit = null;
            this.bbiZoom.Hint = "Select or input the zoom factor";
            this.bbiZoom.Id = 74;
            this.bbiZoom.Name = "bbiZoom";
            // 
            // bar1
            // 
            this.bar1.BarName = "Zoom Toolbar";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 2;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Zoom Toolbar";
            // 
            // commandBarItem11
            // 
            this.commandBarItem11.Caption = "&Close";
            this.commandBarItem11.Hint = "Close the report";
            this.commandBarItem11.Id = 72;
            this.commandBarItem11.Name = "commandBarItem11";
            // 
            // msiWindows
            // 
            this.msiWindows.Caption = "Windows";
            this.msiWindows.Id = 71;
            this.msiWindows.Name = "msiWindows";
            // 
            // commandBarItem10
            // 
            this.commandBarItem10.Caption = "Tile &Vertical";
            this.commandBarItem10.Hint = "Arrange all open documents from left to right";
            this.commandBarItem10.Id = 70;
            this.commandBarItem10.Name = "commandBarItem10";
            // 
            // commandBarItem9
            // 
            this.commandBarItem9.Caption = "Tile &Horizontal";
            this.commandBarItem9.Hint = "Arrange all open documents from top to bottom";
            this.commandBarItem9.Id = 69;
            this.commandBarItem9.Name = "commandBarItem9";
            // 
            // commandBarItem8
            // 
            this.commandBarItem8.Caption = "&Cascade";
            this.commandBarItem8.Hint = "Arrange all open documents cascaded, so that they overlap each other";
            this.commandBarItem8.Id = 68;
            this.commandBarItem8.Name = "commandBarItem8";
            // 
            // msiWindow
            // 
            this.msiWindow.Caption = "&Window";
            this.msiWindow.Id = 66;
            this.msiWindow.Name = "msiWindow";
            // 
            // msiWindowInterface
            // 
            this.msiWindowInterface.BindableChecked = true;
            this.msiWindowInterface.Caption = "&Tabbed Interface";
            this.msiWindowInterface.Checked = true;
            this.msiWindowInterface.CheckedCommand = DevExpress.XtraReports.UserDesigner.ReportCommand.ShowTabbedInterface;
            this.msiWindowInterface.Hint = "Switch between tabbed and window MDI layout modes";
            this.msiWindowInterface.Id = 67;
            this.msiWindowInterface.Name = "msiWindowInterface";
            this.msiWindowInterface.UncheckedCommand = DevExpress.XtraReports.UserDesigner.ReportCommand.ShowWindowInterface;
            // 
            // msiOrder
            // 
            this.msiOrder.Caption = "&Order";
            this.msiOrder.Id = 59;
            this.msiOrder.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBringToFront, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendToBack)});
            this.msiOrder.Name = "msiOrder";
            // 
            // bsiCenter
            // 
            this.bsiCenter.Caption = "&Center in Form";
            this.bsiCenter.Id = 58;
            this.bsiCenter.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCenterHorizontally, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCenterVertically)});
            this.bsiCenter.Name = "bsiCenter";
            // 
            // msiVerticalSpacing
            // 
            this.msiVerticalSpacing.Caption = "&Vertical Spacing";
            this.msiVerticalSpacing.Id = 57;
            this.msiVerticalSpacing.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVertSpaceMakeEqual, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVertSpaceIncrease),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVertSpaceDecrease),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVertSpaceConcatenate)});
            this.msiVerticalSpacing.Name = "msiVerticalSpacing";
            // 
            // msiHorizontalSpacing
            // 
            this.msiHorizontalSpacing.Caption = "&Horizontal Spacing";
            this.msiHorizontalSpacing.Id = 56;
            this.msiHorizontalSpacing.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHorizSpaceMakeEqual, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHorizSpaceIncrease),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHorizSpaceDecrease),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHorizSpaceConcatenate)});
            this.msiHorizontalSpacing.Name = "msiHorizontalSpacing";
            // 
            // msiSameSize
            // 
            this.msiSameSize.Caption = "&Make Same Size";
            this.msiSameSize.Id = 55;
            this.msiSameSize.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSizeToControlWidth, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSizeToGrid),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSizeToControlHeight),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSizeToControl)});
            this.msiSameSize.Name = "msiSameSize";
            // 
            // msiAlign
            // 
            this.msiAlign.Caption = "&Align";
            this.msiAlign.Id = 54;
            this.msiAlign.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAlignLeft, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAlignVerticalCenters),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAlignRight),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAlignTop, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAlignHorizontalCenters),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAlignBottom),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAlignToGrid, true)});
            this.msiAlign.Name = "msiAlign";
            // 
            // msiJustify
            // 
            this.msiJustify.Caption = "&Justify";
            this.msiJustify.Id = 53;
            this.msiJustify.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiJustifyLeft, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiJustifyCenter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiJustifyRight),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiJustifyJustify)});
            this.msiJustify.Name = "msiJustify";
            // 
            // msiFont
            // 
            this.msiFont.Caption = "&Font";
            this.msiFont.Id = 52;
            this.msiFont.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFontBold, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFontItalic),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFontUnderline)});
            this.msiFont.Name = "msiFont";
            // 
            // msiEdit
            // 
            this.msiEdit.Caption = "&Edit";
            this.msiEdit.Id = 44;
            this.msiEdit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUndo, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRedo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCut, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopy),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.commandBarItem5)});
            this.msiEdit.Name = "msiEdit";
            // 
            // msiFile
            // 
            this.msiFile.Caption = "&File";
            this.msiFile.Id = 43;
            this.msiFile.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.commandBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.commandBarItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiOpenFile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveFile, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.commandBarItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.commandBarItem4, true)});
            this.msiFile.Name = "msiFile";
            // 
            // commandBarItem6
            // 
            this.commandBarItem6.Caption = "Select &All";
            this.commandBarItem6.Hint = "Select all the controls in the document";
            this.commandBarItem6.Id = 64;
            this.commandBarItem6.Name = "commandBarItem6";
            // 
            // msiFormat
            // 
            this.msiFormat.Caption = "Fo&rmat";
            this.msiFormat.Id = 51;
            this.msiFormat.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiForeColor),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBackColor)});
            this.msiFormat.Name = "msiFormat";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "&Windows";
            this.barSubItem2.Id = 49;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barDockPanelsListItem1
            // 
            this.barDockPanelsListItem1.Caption = "&Windows";
            this.barDockPanelsListItem1.Id = 50;
            this.barDockPanelsListItem1.Name = "barDockPanelsListItem1";
            this.barDockPanelsListItem1.ShowCustomizationItem = false;
            this.barDockPanelsListItem1.ShowDockPanels = true;
            this.barDockPanelsListItem1.ShowToolbars = false;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&Toolbars";
            this.barSubItem1.Id = 47;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // xrBarToolbarsListItem1
            // 
            this.xrBarToolbarsListItem1.Caption = "&Toolbars";
            this.xrBarToolbarsListItem1.Id = 48;
            this.xrBarToolbarsListItem1.Name = "xrBarToolbarsListItem1";
            // 
            // msiTabButtons
            // 
            this.msiTabButtons.Caption = "&View";
            this.msiTabButtons.Id = 45;
            this.msiTabButtons.Name = "msiTabButtons";
            // 
            // barReportTabButtonsListItem1
            // 
            this.barReportTabButtonsListItem1.Caption = "Tab Buttons";
            this.barReportTabButtonsListItem1.Id = 46;
            this.barReportTabButtonsListItem1.Name = "barReportTabButtonsListItem1";
            // 
            // bsiHint
            // 
            this.bsiHint.Id = 42;
            this.bsiHint.Name = "bsiHint";
            // 
            // beiFontSize
            // 
            this.beiFontSize.Caption = "Font Size";
            this.beiFontSize.Edit = null;
            this.beiFontSize.Hint = "Font Size";
            this.beiFontSize.Id = 1;
            this.beiFontSize.Name = "beiFontSize";
            // 
            // beiFontName
            // 
            this.beiFontName.Caption = "Font Name";
            this.beiFontName.Edit = null;
            this.beiFontName.Hint = "Font Name";
            this.beiFontName.Id = 0;
            this.beiFontName.Name = "beiFontName";
            // 
            // designRepositoryItemComboBox1
            // 
            this.designRepositoryItemComboBox1.Name = "designRepositoryItemComboBox1";
            // 
            // recentlyUsedItemsComboBox1
            // 
            this.recentlyUsedItemsComboBox1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.recentlyUsedItemsComboBox1.AppearanceDropDown.Options.UseFont = true;
            this.recentlyUsedItemsComboBox1.Name = "recentlyUsedItemsComboBox1";
            // 
            // designBar5
            // 
            this.designBar5.BarName = "Status Bar";
            this.designBar5.DockCol = 0;
            this.designBar5.DockRow = 0;
            this.designBar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.designBar5.Text = "Status Bar";
            // 
            // designBar4
            // 
            this.designBar4.BarName = "Layout Toolbar";
            this.designBar4.DockCol = 0;
            this.designBar4.DockRow = 2;
            this.designBar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.designBar4.Text = "Layout Toolbar";
            // 
            // designBar3
            // 
            this.designBar3.BarName = "Formatting Toolbar";
            this.designBar3.DockCol = 1;
            this.designBar3.DockRow = 1;
            this.designBar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.designBar3.Text = "Formatting Toolbar";
            // 
            // designBar2
            // 
            this.designBar2.BarName = "Toolbar";
            this.designBar2.DockCol = 0;
            this.designBar2.DockRow = 1;
            this.designBar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.designBar2.Text = "Toolbar";
            // 
            // designBar1
            // 
            this.designBar1.BarName = "Main Menu";
            this.designBar1.DockCol = 0;
            this.designBar1.DockRow = 0;
            this.designBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.designBar1.Text = "Main Menu";
            // 
            // RaportViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 557);
            this.Controls.Add(this.documentViewer1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "RaportViewerForm";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "RaportViewerForm";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(recentlyUsedItemsComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(designRepositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrDesignBarManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrDesignDockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.designRepositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.designRepositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recentlyUsedItemsComboBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraPrinting.Preview.DocumentViewer documentViewer1;
        private DevExpress.XtraReports.UserDesigner.XRDesignMdiController reportDesigner1;
        private DevExpress.XtraReports.UserDesigner.XRDesignBarManager xrDesignBarManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraReports.UserDesigner.ErrorListDockPanel errorListDockPanel1;
        private DevExpress.XtraReports.UserDesigner.GroupAndSortDockPanel groupAndSortDockPanel1;
        private DevExpress.XtraReports.UserDesigner.ReportGalleryDockPanel reportGalleryDockPanel1;
        private DevExpress.XtraReports.UserDesigner.ReportExplorerDockPanel reportExplorerDockPanel1;
        private DevExpress.XtraReports.UserDesigner.PropertyGridDockPanel propertyGridDockPanel1;
        private DevExpress.XtraReports.UserDesigner.FieldListDockPanel fieldListDockPanel1;
        private DevExpress.XtraReports.UserDesigner.XRDesignDockManager xrDesignDockManager1;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer3;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer2;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraReports.UserDesigner.DesignControlContainer designControlContainer1;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiZoomIn;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiZoomOut;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem7;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem5;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem4;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem3;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem2;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiRedo;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiUndo;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiPaste;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiCopy;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiCut;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiSaveFile;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiOpenFile;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem1;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiSendToBack;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiBringToFront;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiCenterVertically;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiCenterHorizontally;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiVertSpaceConcatenate;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiVertSpaceDecrease;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiVertSpaceIncrease;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiVertSpaceMakeEqual;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiHorizSpaceConcatenate;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiHorizSpaceDecrease;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiHorizSpaceIncrease;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiHorizSpaceMakeEqual;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiSizeToControl;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiSizeToControlHeight;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiSizeToGrid;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiSizeToControlWidth;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiAlignBottom;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiAlignHorizontalCenters;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiAlignTop;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiAlignRight;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiAlignVerticalCenters;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiAlignLeft;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiAlignToGrid;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiJustifyJustify;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiJustifyRight;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiJustifyCenter;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiJustifyLeft;
        private DevExpress.XtraReports.UserDesigner.CommandColorBarItem bbiBackColor;
        private DevExpress.XtraReports.UserDesigner.CommandColorBarItem bbiForeColor;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiFontUnderline;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiFontItalic;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem bbiFontBold;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox designRepositoryItemComboBox2;
        private DevExpress.XtraReports.UserDesigner.XRZoomBarEditItem bbiZoom;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem11;
        private DevExpress.XtraBars.BarMdiChildrenListItem msiWindows;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem10;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem9;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem8;
        private DevExpress.XtraBars.BarSubItem msiWindow;
        private DevExpress.XtraReports.UserDesigner.CommandBarCheckItem msiWindowInterface;
        private DevExpress.XtraBars.BarSubItem msiOrder;
        private DevExpress.XtraBars.BarSubItem bsiCenter;
        private DevExpress.XtraBars.BarSubItem msiVerticalSpacing;
        private DevExpress.XtraBars.BarSubItem msiHorizontalSpacing;
        private DevExpress.XtraBars.BarSubItem msiSameSize;
        private DevExpress.XtraBars.BarSubItem msiAlign;
        private DevExpress.XtraBars.BarSubItem msiJustify;
        private DevExpress.XtraBars.BarSubItem msiFont;
        private DevExpress.XtraBars.BarSubItem msiEdit;
        private DevExpress.XtraBars.BarSubItem msiFile;
        private DevExpress.XtraReports.UserDesigner.CommandBarItem commandBarItem6;
        private DevExpress.XtraBars.BarSubItem msiFormat;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraReports.UserDesigner.BarDockPanelsListItem barDockPanelsListItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraReports.UserDesigner.XRBarToolbarsListItem xrBarToolbarsListItem1;
        private DevExpress.XtraBars.BarSubItem msiTabButtons;
        private DevExpress.XtraReports.UserDesigner.BarReportTabButtonsListItem barReportTabButtonsListItem1;
        private DevExpress.XtraBars.BarStaticItem bsiHint;
        private DevExpress.XtraBars.BarEditItem beiFontSize;
        private DevExpress.XtraBars.BarEditItem beiFontName;
        private DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox designRepositoryItemComboBox1;
        private DevExpress.XtraReports.UserDesigner.RecentlyUsedItemsComboBox recentlyUsedItemsComboBox1;
        private DevExpress.XtraReports.UserDesigner.DesignBar designBar5;
        private DevExpress.XtraReports.UserDesigner.DesignBar designBar4;
        private DevExpress.XtraReports.UserDesigner.DesignBar designBar3;
        private DevExpress.XtraReports.UserDesigner.DesignBar designBar2;
        private DevExpress.XtraReports.UserDesigner.DesignBar designBar1;
    }
}