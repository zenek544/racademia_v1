﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using RAkademia_v1.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace RAkademia_v1
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow( ));            
        }
    }
    public class DataReport
    {
        public String Seria { get; set; }
        public List<RAkademiaDynamiczne> rAkademiaDynamiczne { get; set; }
        public List<RAkademiaStatyczne> rAkademiaStatyczne { get; set; }
        public String PrecentStatic { get; set; }
        public String PrecentDynamic { get; set; }
    }

    public class Calculate
    {
        public String Seria { get; set; }
        public int Ilosc { get; set; }
        public decimal Max { get; set; }
        public decimal Min { get; set; }
        public decimal Avg { get; set; }
        public decimal Std { get; set; }
        public decimal Sum { get; set; }
        public decimal Mediana { get; set; }
        public decimal Var { get; set; }
    }
}
