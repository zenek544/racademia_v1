﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace RAkademia_v1
{
    public partial class ReportViewer : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public ReportViewer()
        {
            InitializeComponent();
        }
        public object _DocumentSource
        {
            get => documentViewer1.DocumentSource;
            set => documentViewer1.DocumentSource = value;
        }

    }
}