﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using RAkademia_v1.Model;
using RAkademia_v1.Raporty;
using System.IO;
using DevExpress.DataAccess.ObjectBinding;
using System.Data.SqlClient;
using System.Timers;
using System.Runtime.InteropServices.ComTypes;
using System.Xml;
using System.Configuration;
using System.Threading;
using System.Data.Sql;
using System.Data.Common;

namespace RAkademia_v1
{
    enum EGridType
    {
        Static,
        Dynamic,
        DynamicOnline,
        StaticOnline,
        Dual,
        Default
    }
    enum EreportType
    {
        File,
        Static,
        Dynamic,
        Dual,
        New
    }
    public partial class MainWindow : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public MainWindow()
        {
            //GetConnectionStrings();
            InitializeComponent();
            FormLoad();
        }
        private void FormLoad()
        {
            //Thread.Sleep(4000);
            if (!Directory.Exists(ReportsDirectory))
                Directory.CreateDirectory(ReportsDirectory);
            tabbed = TabbedMainGroup;
            CheckDataBase();
            AddItemReport();
            LoadWorkSpace();
            this.FormClosing += new FormClosingEventHandler(SaveWorkSpace);
            this.WindowState = FormWindowState.Maximized;
        }
        private void ButtonOff()
        {
            ButtonStatic.Enabled = false;
            ButtonDynamic.Enabled = false;
            ButtonStaticOnline.Enabled = false;
            ButtonDynamicOnline.Enabled = false;
            ButtonDualOnline.Enabled = false;
            ButtonDualOnline.Enabled = false;
            ButtonUpdate.Enabled = false;
            ButtonCreateReportList.Enabled = false;
            ButtonReportList.Enabled = false;
            ButtonSave.Enabled = false;
            ComboDataBase.Enabled = false;
        }
        private String[] BatchList(EGridType type)
        {
            List<String> Batch = new List<String>();
            switch (type)
            {
                case EGridType.Static:
                    Batch = db.Database.SqlQuery<string>("select seria from RAkademiaStatyczne group by seria order by seria desc").ToList();
                    break;
                case EGridType.Dynamic:
                    Batch = db.Database.SqlQuery<string>("select seria from RAkademiaDynamiczne group by seria order by seria desc").ToList();
                    break;
                case EGridType.Dual:
                    Batch = db.Database.SqlQuery<string>("select seria from RAkademiaStatyczne group by seria").ToList();
                    List<String> batch2 = db.Database.SqlQuery<string>("select seria from RAkademiaDynamiczne group by seria").ToList();
                    foreach (String x in batch2)
                    {
                        Batch.Add(x);
                    }
                    break;
                default:
                    Batch = null;
                    Batch = new List<String>();
                    break;
            }
            Batch.Sort();
            return Batch.Distinct().ToArray();

        }
        #region zmienne
        bool init = true;
        private System.Timers.Timer Time = new System.Timers.Timer();
        TabbedControlGroup tabbed;
        RakademiaDbContext db = new RakademiaDbContext();
        List<RAkademiaStatyczne> staticDb;
        List<RAkademiaDynamiczne> dynamicDb;
        EGridType Online;
        private static string ReportsDirectory = Path.Combine(Application.StartupPath, "Raporty");
        public static string LayoutDirectory = Path.Combine(Application.StartupPath, "Layout");
        #endregion

        #region dane

        private void LoadGridData(EGridType gridType)
        {
            switch (gridType)
            {
                case EGridType.Static:
                    Load(EGridType.Static);
                    GridStatic.DataSource = staticDb;
                    break;
                case EGridType.Dynamic:
                    Load(EGridType.Dynamic);
                    GridDynamic.DataSource = dynamicDb;
                    break;
            }
        }

        private new void Load(EGridType gridType,String batch=null)
        {
            try
            {
                switch (gridType)
                {
                    case EGridType.Static:
                        staticDb = db.RAkademiaStatyczne.ToList();
                        break;
                    case EGridType.Dynamic:
                        dynamicDb = db.RAkademiaDynamiczne.ToList();
                        break;
                    case EGridType.Dual:
                        staticDb = db.RAkademiaStatyczne.Where(x => x.seria == batch).ToList();
                        dynamicDb = db.RAkademiaDynamiczne.Where(x => x.seria == batch).ToList();
                        break;
                    default:
                        staticDb = db.RAkademiaStatyczne.ToList();
                        dynamicDb = db.RAkademiaDynamiczne.ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                if (!init)
                {
                    XtraMessageBox.Show(
                        "Ups! Coś poszło nie tak:" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace,
                        "Bład!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            finally
            {
                init = false; 
            }
        }
        
        private void LoadSeria(EGridType gridType)
        {
            try
            {
                ComboBoxEdit combo = TextChoiceBatch;
                combo.Properties.Items.Clear();
                String[] batch;
                switch (gridType)
                {
                    case EGridType.StaticOnline:
                        batch = BatchList(EGridType.Static);
                        break;
                    case EGridType.DynamicOnline:
                        batch = BatchList(EGridType.Dynamic);

                        break;
                    case EGridType.Dual:
                        combo = ComboDualBatch;
                        batch = BatchList(EGridType.Dual);
                        break;
                    default:
                        batch =new String[]{ "0"};
                        break;

                }
                for (int i = batch.Length - 1; i >= 0; i--)
                    combo.Properties.Items.Add(batch[i]);
                combo.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(
                    "Ups! Coś poszło nie tak:" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace,
                    "Bład!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void Timer()
        {
            if (Time.Enabled == true)
            {
                Time.Dispose();
                Time = new System.Timers.Timer();
            }
            Time.Interval = 10000;
            Time.Elapsed += new ElapsedEventHandler(LoadOnline);
            Time.Start();
            LoadOnline();
        }

        private void LoadOnline(object sender = null, EventArgs e = null)
        {
            Time.Stop();
            try
            {
                if (Online != EGridType.Dual)
                {
                    List<TextEdit> TextEditList = new List<TextEdit>()
                    {
                        this.Count,
                        this.TextSum,
                        this.TextAvg,
                        this.TextMedian,
                        this.TextMin,
                        this.TextMax,
                        this.TextDev,
                        this.TextVar
                    };
                    Calculate Calc = new Calculate();
                    String Batch = "";
                    if (TextChoiceBatch.SelectedItem == null)
                        Batch = "brak danych";
                    else
                        Batch = TextChoiceBatch.SelectedItem.ToString();
                    var param = new SqlParameter()
                    {
                        ParameterName = "@Seria",
                        SqlDbType = SqlDbType.VarChar,
                        SqlValue = Batch
                    };
                    switch (Online)
                    {
                        case EGridType.StaticOnline:
                            Load(EGridType.Static);
                            Calc = db.Database.SqlQuery<Calculate>("exec StatyczneLiczenie @Seria", param).FirstOrDefault();
                            GridOnline.Invoke(new Action(delegate ()
                            {
                                GridOnline.DataSource = staticDb.Where(x => x.seria == Batch).ToList();
                            }));
                            break;
                        case EGridType.DynamicOnline:
                            Load(EGridType.Dynamic);
                            Calc = db.Database.SqlQuery<Calculate>("exec DynamiczneLiczenie @Seria", param).FirstOrDefault();
                            GridOnline.Invoke(new Action(delegate ()
                            {
                                GridOnline.DataSource = db.RAkademiaDynamiczne.Where(x => x.seria == Batch).ToList();
                            }));

                            break;
                    }
                    #region widok
                    if (Calc == null)
                    {
                        foreach (var x in TextEditList)
                        {
                            x.Invoke(new Action(delegate ()
                            {
                                x.Text = "brak danych";
                            }));
                        }
                    }
                    else
                    {
                        Count.Invoke(new Action(delegate ()
                        {
                            Count.Text = string.IsNullOrEmpty(Calc.Ilosc.ToString()) ? "Brak danych" : Calc.Ilosc.ToString();
                        }));
                        TextSum.Invoke(new Action(delegate ()
                        {
                            TextSum.Text = string.IsNullOrEmpty(Calc.Sum.ToString()) ? "Brak danych" : Calc.Sum.ToString()+"kg";
                        }));
                        TextAvg.Invoke(new Action(delegate ()
                        {
                            TextAvg.Text = string.IsNullOrEmpty(Calc.Avg.ToString()) ? "Brak danych" : Calc.Avg.ToString()+"kg";
                        }));
                        TextMedian.Invoke(new Action(delegate ()
                        {
                            TextMedian.Text = string.IsNullOrEmpty(Calc.Mediana.ToString()) ? "Brak danych" : Calc.Mediana.ToString()+"kg";
                        }));
                        TextMin.Invoke(new Action(delegate ()
                        {
                            TextMin.Text = string.IsNullOrEmpty(Calc.Min.ToString()) ? "Brak danych" : Calc.Min.ToString()+"kg";
                        }));
                        TextMax.Invoke(new Action(delegate ()
                        {
                            TextMax.Text = string.IsNullOrEmpty(Calc.Max.ToString()) ? "Brak danych" : Calc.Max.ToString()+"kg";
                        }));
                        TextDev.Invoke(new Action(delegate ()
                        {
                            TextDev.Text = string.IsNullOrEmpty(Calc.Std.ToString()) ? "Brak danych" : Calc.Std.ToString()+"kg";
                        }));
                        TextVar.Invoke(new Action(delegate ()
                        {
                            TextVar.Text = string.IsNullOrEmpty(Calc.Var.ToString()) ? "Brak danych" : Calc.Var.ToString()+"kg";
                        }));
                    }
                    foreach (TextEdit text in TextEditList)
                        text.Invoke(new Action(delegate ()
                        {
                            text.ReadOnly = true;
                        }));
                    tabbed.SelectedTabPage = WeighingOnline;
                }
                #endregion
                else
                {
                    tabbed.SelectedTabPage = WeightingDual;
                    List<TextEdit> TextEditStatic = new List<TextEdit>
                    {
                        TextDualStaticCount,
                        TextDualStaticSum,
                        TextDualStaticMin,
                        TextDualStaticMax,
                        TextDualStaticAvg,
                        TextDualStaticVar,
                        TextDualStaticStd,
                        TextDualStaticMedian,
                        TextDualStaticPrecent,
                    };
                    List<TextEdit> TextEditDynamic = new List<TextEdit>
                    {
                        TextDualDynamicCount,
                        TextDualDynamicSum,
                        TextDualDynamicMin,
                        TextDualDynamicMax,
                        TextDualDynamicStd,
                        TextDualDynamicAvg,
                        TextDualDynamicVar,
                        TextDualDynamicMedian,
                        TextDualDynamicPrecent,
                    };
                    Calculate StaticCalc = new Calculate();
                    Calculate DynamicCalc = new Calculate();
                    String Batch = ComboDualBatch.SelectedItem.ToString();
                    Load(EGridType.Dual, Batch);
                    var param = new SqlParameter()
                    {
                        ParameterName = "@Seria",
                        SqlDbType = SqlDbType.VarChar,
                        SqlValue = Batch
                    };
                    var param2 = new SqlParameter()
                    {
                        ParameterName = "@Seria",
                        SqlDbType = SqlDbType.VarChar,
                        SqlValue = Batch
                    };
                    StaticCalc = db.Database.SqlQuery<Calculate>("exec StatyczneLiczenie @Seria", param).FirstOrDefault();
                    GridDualStatic.Invoke(new Action(delegate ()
                    {
                        GridDualStatic.DataSource = staticDb;
                    }));

                    DynamicCalc = db.Database.SqlQuery<Calculate>("exec DynamiczneLiczenie @Seria", param2).FirstOrDefault();
                    GridDualDynamic.Invoke(new Action(delegate ()
                    {
                        GridDualDynamic.DataSource = dynamicDb;
                    }));
                    if (DynamicCalc != null && StaticCalc != null)
                    {
                        TextDualDynamicPrecent.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicPrecent.Text = ((float)DynamicCalc.Ilosc / (float)StaticCalc.Ilosc * 100).ToString("0.00") + "%";
                        }));
                        TextDualStaticPrecent.Invoke(new Action(delegate ()
                        {
                            TextDualStaticPrecent.Text = ((float)StaticCalc.Ilosc / (float)DynamicCalc.Ilosc * 100).ToString("0.00") + "%";
                        }));
                    }
                    else
                    {
                        TextDualDynamicPrecent.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicPrecent.Text = "brak danych";
                        }));
                        TextDualStaticPrecent.Invoke(new Action(delegate ()
                        {
                            TextDualStaticPrecent.Text = "brak danych";
                        }));
                    }
                    #region widok
                    //string.IsNullOrEmpty(Calc.Var.ToString()) ? "Brak danych" : Calc.Var.ToString();
                    if (StaticCalc == null)
                    {
                        foreach (var x in TextEditStatic)
                        {
                            x.Invoke(new Action(delegate ()
                            {
                                x.Text = "brak danych";
                            }));
                        }
                    }
                    else
                    {
                        TextDualStaticCount.Invoke(new Action(delegate ()
                        {
                            TextDualStaticCount.Text = string.IsNullOrEmpty(StaticCalc.Ilosc.ToString()) ? "Brak danych" : StaticCalc.Ilosc.ToString();
                        }));
                        TextDualStaticSum.Invoke(new Action(delegate ()
                        {
                            TextDualStaticSum.Text = string.IsNullOrEmpty(StaticCalc.Sum.ToString()) ? "Brak danych" : StaticCalc.Sum.ToString()+"kg";
                        }));
                        TextDualStaticMin.Invoke(new Action(delegate ()
                        {
                            TextDualStaticMin.Text = string.IsNullOrEmpty(StaticCalc.Min.ToString()) ? "Brak danych" : StaticCalc.Min.ToString()+"kg";
                        }));
                        TextDualStaticMax.Invoke(new Action(delegate ()
                        {
                            TextDualStaticMax.Text = string.IsNullOrEmpty(StaticCalc.Max.ToString()) ? "Brak danych" : StaticCalc.Max.ToString()+"kg";
                        }));
                        TextDualStaticAvg.Invoke(new Action(delegate ()
                        {
                            TextDualStaticAvg.Text = string.IsNullOrEmpty(StaticCalc.Avg.ToString()) ? "Brak danych" : StaticCalc.Avg.ToString()+"kg";
                        }));
                        TextDualStaticVar.Invoke(new Action(delegate ()
                        {
                            TextDualStaticVar.Text = string.IsNullOrEmpty(StaticCalc.Var.ToString()) ? "Brak danych" : StaticCalc.Var.ToString()+"kg";
                        }));
                        TextDualStaticStd.Invoke(new Action(delegate ()
                        {
                            TextDualStaticStd.Text = string.IsNullOrEmpty(StaticCalc.Std.ToString()) ? "Brak danych" : StaticCalc.Std.ToString()+"kg";
                        }));
                        TextDualStaticMedian.Invoke(new Action(delegate ()
                        {
                            TextDualStaticMedian.Text = string.IsNullOrEmpty(StaticCalc.Mediana.ToString()) ? "Brak danych" : StaticCalc.Mediana.ToString()+"kg";
                        }));
                    }
                    if (DynamicCalc == null)
                    {
                        foreach (var x in TextEditDynamic)
                        {
                            x.Invoke(new Action(delegate ()
                            {
                                x.Text = "brak danych";
                            }));
                        }
                    }
                    else
                    {
                        TextDualDynamicCount.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicCount.Text = string.IsNullOrEmpty(DynamicCalc.Ilosc.ToString()) ? "Brak danych" : DynamicCalc.Ilosc.ToString();
                        }));
                        TextDualDynamicSum.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicSum.Text = string.IsNullOrEmpty(DynamicCalc.Sum.ToString()) ? "Brak danych" : DynamicCalc.Sum.ToString()+"kg";
                        }));
                        TextDualDynamicMin.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicMin.Text = string.IsNullOrEmpty(DynamicCalc.Min.ToString()) ? "Brak danych" : DynamicCalc.Min.ToString()+"kg";
                        }));
                        TextDualDynamicMax.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicMax.Text = string.IsNullOrEmpty(DynamicCalc.Max.ToString()) ? "Brak danych" : DynamicCalc.Max.ToString()+"kg";
                        }));
                        TextDualDynamicStd.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicStd.Text = string.IsNullOrEmpty(DynamicCalc.Std.ToString()) ? "Brak danych" : DynamicCalc.Std.ToString()+"kg";
                        }));
                        TextDualDynamicAvg.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicAvg.Text = string.IsNullOrEmpty(DynamicCalc.Avg.ToString()) ? "Brak danych" : DynamicCalc.Avg.ToString()+"kg";
                        }));
                        TextDualDynamicVar.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicVar.Text = string.IsNullOrEmpty(DynamicCalc.Var.ToString()) ? "Brak danych" : DynamicCalc.Var.ToString()+"kg";
                        }));
                        TextDualDynamicMedian.Invoke(new Action(delegate ()
                        {
                            TextDualDynamicMedian.Text = string.IsNullOrEmpty(DynamicCalc.Mediana.ToString()) ? "Brak danych" : DynamicCalc.Mediana.ToString()+"kg";
                        }));
                    }
                    #endregion
                    foreach (TextEdit text in TextEditStatic)
                    {
                        text.Invoke(new Action(delegate ()
                        {
                            text.ReadOnly = true;
                        }));
                    }
                    foreach (TextEdit text in TextEditDynamic)
                    {
                        text.Invoke(new Action(delegate ()
                        {
                            text.ReadOnly = true;
                        }));
                    }

                }
            }
            /*catch (NullReferenceException)
            {
                Time.Stop();
                XtraMessageBox.Show(
                    "Brak Danych do wyświetlenia",
                    "Bład!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            catch (InvalidOperationException)
            {
                Time.Stop();
                XtraMessageBox.Show(
                    "Brak Danych do wyświetlenia",
                    "Bład!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }*/
            catch (Exception ex)
            {
                Time.Stop();
                XtraMessageBox.Show(
                    "Ups! Coś poszło nie tak:" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace,
                    "Bład!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }

        #endregion

        #region raporty

        private void AddItemReport()
        {
            ButtonReportList.Strings.Clear();
            ButtonCreateReportList.Strings.Clear();
            var files = Directory.GetFiles(
                    ReportsDirectory,
                    "*.repx",
                    SearchOption.TopDirectoryOnly)
                    .ToDictionary<string, string>(x => Path.GetFileName(x));
            List<String> report = new List<String>();
            ButtonCreateReportList.Strings.Add("Nowy Raport");
            foreach (var file in files)
            {
                switch (file.Key)
                {
                    case "StaticReport.repx":
                        report.Add("Raport Serii Statycznych");
                        break;
                    case "DynamicReport.repx":
                        report.Add("Raport Serii Dynamicznych");
                        break;
                    case "DualReport.repx":
                        report.Add("Raport Ważeń Porównawczych");
                        break;
                    default:
                        report.Add(file.Key);
                        break;
                }
            }
            report.Add("Domyślny Serii Dynamicznych");
            report.Add("Domyślny Serii Statycznych");
            report.Add("Domyślny Ważeń Porównawczych");
            foreach (var x in report)
            {
                ButtonCreateReportList.Strings.Add(x);
                ButtonReportList.Strings.Add(x);
            }
        }

        private void LoadReport(EreportType type, string directory = null)
        {
            try
            {
                XtraReport raport;
                ObjectDataSource source;
                String[] batch;
                List<DataReport> dataReports = new List<DataReport>();
                
                switch (type)
                {
                    case EreportType.Static:
                        raport = new StaticReport();
                        if (directory != null)
                            raport.LoadLayoutFromXml(directory);
                        batch = BatchList(EGridType.Static);
                        break;
                    case EreportType.Dynamic:
                        raport = new DynamicReport();
                        if (directory != null)
                            raport.LoadLayoutFromXml(directory);
                        batch = BatchList(EGridType.Dynamic);
                        break;
                    case EreportType.File:
                        raport = new XtraReport();
                        if (directory != null)
                            raport.LoadLayoutFromXml(directory);
                        batch = BatchList(EGridType.Dual);
                        break;
                    case EreportType.Dual:
                        raport = new DualReport();
                        if (directory != null)
                            raport.LoadLayoutFromXml(directory);
                        batch = BatchList(EGridType.Dual);
                        break;
                    default:
                        batch = BatchList(EGridType.Dual);
                        raport = new XtraReport();
                        if (directory != null)
                            raport.LoadLayoutFromXml(directory);
                        break;
                }
                foreach (String Seria in batch)
                {
                    Load(EGridType.Dual, Seria);
                    DataReport ReportData = new DataReport();
                    if (staticDb.Count != 0 && dynamicDb.Count() != 0)
                    {
                        ReportData.Seria = Seria;
                        ReportData.rAkademiaDynamiczne = dynamicDb;
                        ReportData.rAkademiaStatyczne = staticDb;
                        ReportData.PrecentStatic = ((float)staticDb.Count() / (float)dynamicDb.Count() * 100).ToString("0.00") + "%";
                        ReportData.PrecentDynamic = ((float)dynamicDb.Count() / (float)staticDb.Count() * 100).ToString("0.00") + "%";
                    }
                    else
                    {
                        ReportData.Seria = Seria;
                        ReportData.rAkademiaDynamiczne = dynamicDb;
                        ReportData.rAkademiaStatyczne = staticDb;
                        ReportData.PrecentStatic = "";
                        ReportData.PrecentDynamic = "";

                    }
                    dataReports.Add(ReportData);
                }
                source = new ObjectDataSource()
                {
                    Constructor = new ObjectConstructorInfo(),
                    DataSource = dataReports,
                };
                raport.DataSource = source;
                using (ReportViewer form = new ReportViewer())
                {
                    form._DocumentSource = raport;
                    form.WindowState = FormWindowState.Maximized;
                    form.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(
                    "Ups! Coś poszło nie tak:" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace,
                    "Bład!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void EditReport(EreportType type, string directory = null)
        {
            try
            {
                XtraReport report;
                ObjectDataSource source;
                String name = "";
                bool CustomSave;
                switch (type)
                {
                    case EreportType.New:
                        report = new XtraReport();
                        CustomSave = false;
                        break;
                    case EreportType.File:
                        report = new XtraReport(); 
                            if (directory != null)
                            report.LoadLayoutFromXml(directory);
                        CustomSave = false;
                        break;
                    case EreportType.Static:
                        report = new StaticReport();
                        if (directory != null)
                            report.LoadLayoutFromXml(directory);
                        name = "StaticReport.repx";
                        CustomSave = true;
                        break;
                    case EreportType.Dual:
                        report = new DualReport();
                        if (directory != null)
                            report.LoadLayoutFromXml(directory);
                        name = "DualReport.repx";
                        CustomSave = true;
                        break;
                    case EreportType.Dynamic:
                        report = new DynamicReport();
                        if (directory != null)
                            report.LoadLayoutFromXml(directory);
                        CustomSave = true;
                        name = "DynamicReport.repx";
                        break;
                    default:
                        report = new XtraReport();
                        if (directory != null)
                            report.LoadLayoutFromXml(directory);
                        CustomSave = false;
                        break;
                }
                source = new ObjectDataSource()
                {
                    Constructor = new ObjectConstructorInfo(),
                    DataSource = new DataReport(),
                };
                report.DataSource = source;
                using (var form = new DesignerForm(report, name, CustomSave))
                {
                    form.WindowState = FormWindowState.Maximized;
                    form.ShowDialog();
                }
                AddItemReport();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(
                    "Ups! Coś poszło nie tak:" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace,
                    "Bład!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #endregion

        #region bazadanych
        private void GetDataBaseList()
        {
            DataTable dt = SqlDataSourceEnumerator.Instance.GetDataSources();
            foreach (DataRow d in dt.Rows)
            {
                ComboSerwer.Properties.Items.Add(d[0].ToString() + "\\" + d[1].ToString());
            }
        }

        private void CheckDataBase()
        {
            var connection = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            SqlHelper helper = new SqlHelper(connection);
            if (helper.IsConnected)
            {
                tabbed.SelectedTabPage = Static;
                LoadGridData(EGridType.Static);
            }
            else
            {
                if(!string.IsNullOrEmpty(connection))
                XtraMessageBox.Show(
                    "Nie można połączyć się z serwerem bazy danych", "Brak połączenia",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                tabbed.SelectedTabPage = layoutDataBaseSettings;
                ButtonOff();
                GetDataBaseList();
            }
        }

        private void EditConnectionString(String MyDbname, String InitCatalog, String MyUserId, String ConnPw)
        {
            ///string MyDbName = "localhost\\BAZARADWAG2012";
            //string InitCatalog = "WIPASZ";
            // string myuserid = "sa";
            //string connPw = "Radwag99";
            string[] Dbname = MyDbname.Split('\\');
            String connectionString = "data source=" + Dbname[0]+'\\'+ Dbname[1] + ";initial catalog=" + InitCatalog + ";user id=" + MyUserId +
              ";password=" + ConnPw + ";MultipleActiveResultSets=True;App=EntityFramework";
            try
            {
                SqlHelper helper = new SqlHelper(connectionString);
                AppSetting setting = new AppSetting();
                setting.SaveConnectionString("cn", connectionString);
                AddToDataBase(connectionString);
                XtraMessageBox.Show("w celu wykonania zmian program zostanie uruchomiony ponownie", "Restart", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Restart();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(
                    "Ups! Coś poszło nie tak:" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace,
                    "Bład!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }

        private void AddToDataBase( string ConnectionString)
        {
            String[] queryString = new String[]
                   {
                    "IF NOT EXISTS(SELECT 1 FROM sys.views WHERE Name = 'RAkademiaDynamiczne') BEGIN declare @query varchar(max)= " +
                        "'CREATE VIEW [dbo].[RAkademiaDynamiczne] AS SELECT dbo.ew1_op.sImie_nazw AS ImieNazwisko, dbo.wazenia_dyn.ID_KEY AS id, ISNULL(dbo.wazenia_dyn.Batch, ''0'')" +
                        " AS seria, dbo.ew1_towary.nazwa, CAST(CASE WHEN wazenia_dyn.Unit = ''kg'' THEN CAST(CAST(wazenia_dyn.Netto_mass AS decimal(26, 2)) AS varchar(50)) ELSE" +
                        " CAST(CAST(wazenia_dyn.Netto_mass / 1000 AS decimal(26, 2)) AS varchar(50)) END AS varchar(50)) + ''kg'' AS Masa, CAST(CASE WHEN wazenia_dyn.Unit = ''kg''" +
                        " THEN CAST(CAST(dbo.wazenia_dyn.Tare AS decimal(26, 2)) AS varchar(20)) ELSE CAST(CAST(dbo.wazenia_dyn.Tare / 1000 AS decimal(26, 2)) AS varchar(20)) END" +
                        " AS varchar(50)) + ''kg'' AS Tara, dbo.wazenia_dyn.Date, CAST(CASE WHEN wazenia_dyn.Unit = ''kg'' THEN CAST(CAST(wazenia_dyn.Netto_mass AS decimal(26, 2)) AS varchar(50)) ELSE" +
                        " CAST(CAST(wazenia_dyn.Netto_mass / 1000 AS decimal(26, 2)) AS varchar(50)) END AS decimal(26,2)) AS Netto_mass FROM dbo.wazenia_dyn" +
                        " LEFT OUTER JOIN dbo.ew1_towary ON dbo.wazenia_dyn.ASORTYMENT_ID = dbo.ew1_towary.id LEFT OUTER JOIN dbo.ew1_op ON dbo.wazenia_dyn.OPERATOR_ID = dbo.ew1_op.id'" +
                        " exec(@query) END ",

                    " IF NOT EXISTS(SELECT 1 FROM sys.views WHERE Name = 'RakademiaStatyczne') BEGIN declare @query varchar(max)='CREATE VIEW [dbo].[RAkademiaStatyczne] AS " +
                        " SELECT dbo.ew1_op.sImie_nazw AS ImieNazwisko, dbo.ew1_wazenia.id, ISNULL(dbo.ew1_wazenia.kod_serii, ''0'') AS seria, dbo.ew1_towary.nazwa," +
                        " CAST(CASE WHEN ew1_wazenia.jednoska_wazenia = ''kg'' THEN CAST(CAST(dbo.ew1_wazenia.masa_w_jednostce AS decimal(26, 2)) AS varchar(50)) ELSE" +
                        " CAST(CAST(dbo.ew1_wazenia.masa_w_jednostce / 1000 AS decimal(26,2)) AS varchar(50)) END AS varchar(50)) + ''kg'' AS Masa," +
                        " CAST(CASE WHEN ew1_wazenia.jednoska_wazenia = ''kg'' THEN CAST(CAST(dbo.ew1_wazenia.tara AS decimal(26, 2)) AS varchar(50)) ELSE" +
                        " CAST(CAST(dbo.ew1_wazenia.tara / 1000 AS decimal(26, 2)) AS varchar(50)) END AS varchar(50)) + ''kg'' AS Tara, dbo.ew1_wazenia.data_czas AS Date," +
                        "  CAST(CASE WHEN ew1_wazenia.jednoska_wazenia = ''kg'' THEN CAST(CAST(dbo.ew1_wazenia.masa_w_jednostce AS decimal(26, 2)) AS varchar(50)) ELSE" +
                        " CAST(CAST(dbo.ew1_wazenia.masa_w_jednostce / 1000 AS decimal(26,2)) AS varchar(50)) END AS decimal(26,2)) AS Netto_Mass FROM dbo.ew1_wazenia LEFT OUTER JOIN dbo.ew1_towary ON" +
                        " dbo.ew1_towary.id = dbo.ew1_wazenia.id_towar LEFT OUTER JOIN dbo.ew1_op ON dbo.ew1_wazenia.id_operator = dbo.ew1_op.id' exec(@query) END ",

                    "IF NOT EXISTS(select* from sys.procedures where name ='StatyczneLiczenie') BEGIN declare @query varchar(max)= " +
                        "'CREATE PROCEDURE [dbo].[StatyczneLiczenie](@Seria varchar(100)) AS select seria, COUNT(seria) ilosc,CAST(max(Netto_Mass) as decimal(26, 2)) max, " +
                        " Cast(min(Netto_Mass) as decimal(26, 2))min,CAST(AVG(Netto_Mass) as decimal(26, 2)) avg, Cast(STDEV(Netto_Mass) as decimal(26, 2)) std, " +
                        " Cast(VAR(Netto_Mass) as decimal(26, 2)) var, Cast(SUM(Netto_Mass) as decimal(26, 2)) sum,CAST((SELECT AVG(1.0 * netto_mass) FROM (SELECT netto_mass, " +
                        " ra = ROW_NUMBER() OVER(ORDER BY netto_mass, id), rd = ROW_NUMBER() OVER(ORDER BY netto_mass DESC, id DESC) FROM RAkademiaStatyczne where seria = @Seria) AS x " +
                        " WHERE ra BETWEEN rd - 1 AND rd + 1) as Decimal(26, 2)) as mediana from RAkademiaStatyczne seria where seria = @Seria group by seria' exec(@query) END ",

                    "IF NOT EXISTS(select* from sys.procedures where name ='DynamiczneLiczenie') BEGIN declare @query varchar(max)= " +
                        "' CREATE PROCEDURE [dbo].[DynamiczneLiczenie](@Seria varchar(100)) AS select seria, COUNT(seria) ilosc,CAST(max(Netto_Mass) as decimal(26, 2)) max, " +
                        " Cast(min(Netto_Mass) as decimal(26, 2)) min,CAST(AVG(Netto_Mass) as decimal(26, 2)) avg,Cast(STDEV(Netto_Mass) as decimal(26, 2)) std, " +
                        " Cast(VAR(Netto_Mass) as decimal(26, 2)) var, Cast(SUM(Netto_Mass) as decimal(26, 2)) sum, CAST((SELECT AVG(1.0 * netto_mass) FROM (SELECT netto_mass, " +
                        " ra = ROW_NUMBER() OVER(ORDER BY netto_mass, id), rd = ROW_NUMBER() OVER(ORDER BY netto_mass DESC, id DESC) FROM RAkademiaDynamiczne where seria = @Seria) AS x " +
                        " WHERE ra BETWEEN rd - 1 AND rd + 1) as Decimal(26, 2)) as mediana from RAkademiaDynamiczne seria where seria = @Seria group by seria' exec(@query) END "
                   };
            SqlConnection Connection = new SqlConnection(ConnectionString);
            Connection.Open();
            foreach (string st in queryString)
            {
                SqlCommand command = new SqlCommand(st, Connection);
                if (command.Connection.State == ConnectionState.Open)
                    command.Connection.Close();
                command.Connection.Open();
                string value = (string)command.ExecuteScalar();
                command.Connection.Close();
            }
            Connection.Close();
        }

        #endregion

        #region layout

        private void LoadWorkSpace()
        {
            String LayoutStatic = Path.Combine(LayoutDirectory, "static.xml");
            String LayoutDynamic = Path.Combine(LayoutDirectory, "dynamic.xml");
            String LayoutOnline = Path.Combine(LayoutDirectory, "online.xml");
            String layoutOnlineGrid = Path.Combine(LayoutDirectory, "onlinegrid.xml");
            String LayoutDual = Path.Combine(LayoutDirectory, "dual.xml");
            String LayoutDualStatic = Path.Combine(LayoutDirectory, "dualstatic.xml");
            String LayoutDualDynamic = Path.Combine(LayoutDirectory, "dualdynamic.xml");
            if (File.Exists(LayoutStatic))
                GridStatic.MainView.RestoreLayoutFromXml(LayoutStatic);
            if (File.Exists(LayoutDynamic))
                GridDynamic.MainView.RestoreLayoutFromXml(LayoutDynamic);
            if (File.Exists(LayoutOnline))
                GridOnline.MainView.RestoreLayoutFromXml(LayoutOnline);
            if (File.Exists(LayoutDual))
                dataLayoutControl1.RestoreLayoutFromXml(LayoutDual);
            if (File.Exists(layoutOnlineGrid))
                layoutControl2.RestoreLayoutFromXml(layoutOnlineGrid);
            if (File.Exists(LayoutDualStatic))
                GridDualStatic.MainView.RestoreLayoutFromXml(LayoutDualStatic);
            if (File.Exists(LayoutDualDynamic))
                GridDualDynamic.MainView.RestoreLayoutFromXml(LayoutDualDynamic);
        }

        private void SaveWorkSpace(object sender, FormClosingEventArgs e)
        {
            if (!Directory.Exists(LayoutDirectory))
                Directory.CreateDirectory(LayoutDirectory);
            String LayoutStatic = Path.Combine(LayoutDirectory, "static.xml");
            String LayoutDynamic = Path.Combine(LayoutDirectory, "dynamic.xml");
            String LayoutOnline = Path.Combine(LayoutDirectory, "online.xml");
            String layoutOnlineGrid = Path.Combine(LayoutDirectory, "onlinegrid.xml");
            String LayoutDual = Path.Combine(LayoutDirectory, "dual.xml");
            String LayoutDualStatic = Path.Combine(LayoutDirectory, "dualstatic.xml");
            String LayoutDualDynamic = Path.Combine(LayoutDirectory, "dualdynamic.xml");
            GridStatic.MainView.SaveLayoutToXml(LayoutStatic);
            GridDynamic.MainView.SaveLayoutToXml(LayoutDynamic);
            GridOnline.MainView.SaveLayoutToXml(LayoutOnline);
            dataLayoutControl1.SaveLayoutToXml(LayoutDual);
            layoutControl2.SaveLayoutToXml(layoutOnlineGrid);
            GridDualStatic.MainView.SaveLayoutToXml(LayoutDualDynamic);
            GridDualDynamic.MainView.SaveLayoutToXml(LayoutDualDynamic);
        }

        #endregion

        #region przyciski

        private void Static_itemClick(object sender, ItemClickEventArgs e)
        {
            Time.Stop();
            tabbed.SelectedTabPage = Static;
            LoadGridData(EGridType.Static);
        }

        private void Dynamic_itemClick(object sender, ItemClickEventArgs e)
        {
            Time.Stop();
            tabbed.SelectedTabPage = Dynamic;
            LoadGridData(EGridType.Dynamic);
        }

        private void Update_itemClick(object sender, ItemClickEventArgs e)
        {
            Load(EGridType.Default);
        }

        private void ListReports_ItemClick(object sender, ListItemClickEventArgs e)
        {
            String Name = (e.Item as BarListItem).ItemLinks[e.Index].Caption;
            String Directory = null;
            switch (Name)
            {
                case "Raport Serii Statycznych":
                    Directory = Path.Combine(ReportsDirectory, "StaticReport.repx");
                    LoadReport(EreportType.Static, Directory);
                    break;
                case "Raport Serii Dynamicznych":
                    Directory = Path.Combine(ReportsDirectory, "DynamicReport.repx");
                    LoadReport(EreportType.Dynamic, Directory);
                    break;
                case "Raport Ważeń Porównawczych":
                    Directory = Path.Combine(ReportsDirectory, "DualReport.repx");
                    LoadReport(EreportType.Dual, Directory);
                    break;
                case "Domyślny Serii Statycznych":
                    LoadReport(EreportType.Static, Directory);
                    break;
                case "Domyślny Serii Dynamicznych":
                    LoadReport(EreportType.Dynamic);
                    break;
                case "Domyślny Ważeń Porównawczych":
                    LoadReport(EreportType.Dual);
                    break;
                default:
                    Directory = Path.Combine(ReportsDirectory, Name);
                    LoadReport(EreportType.File, Directory);
                    break;
            }
        }

        private void ChoiceBash_itemClick(object sender, EventArgs e)
        {
            LoadOnline();
        }

        private void StaticOnline_itemClick(object sender, ItemClickEventArgs e)
        {
            Time.Stop();
            Online = EGridType.StaticOnline;
            GroupOnline.Text = "Podgląd ważeń Statycznych";
            TextChoiceBatch.Properties.Items.Clear();
            LoadSeria(EGridType.StaticOnline);
            Timer();
        }

        private void DynamicOnline_itemClick(object sender, ItemClickEventArgs e)
        {
            Time.Stop();
            Online = EGridType.DynamicOnline;
            TextChoiceBatch.Properties.Items.Clear();
            LoadSeria(EGridType.DynamicOnline);
            Timer();
            GroupOnline.Text = "Podgląd ważeń Dynamicznych";
        }

        private void DualButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            Time.Stop();
            Online = EGridType.Dual;
            ComboDualBatch.Properties.Items.Clear();
            LoadSeria(EGridType.Dual);
            Timer();
        }

        private void ButtonDataBase_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetDataBaseList();
            Time.Stop();
            var connectionString = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            string[] con = connectionString.Split(';');
            if (con.Length > 2)
            {
                ComboSerwer.SelectedItem = con[0].Remove(0, 12);
                ComboDataBase.Text = con[1].Remove(0, 16);
                TextUserName.Text = con[2].Remove(0, 8);
                TextUserPassword.Text = con[3].Remove(0, 9);
            }

            tabbed.SelectedTabPage = layoutDataBaseSettings;
        }

        private void SimpleButton1_Click(object sender, EventArgs e)
        {
            EditConnectionString(ComboSerwer.Text, ComboDataBase.Text, TextUserName.Text, TextUserPassword.Text);
        }

        private void ComboDualBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadOnline(EGridType.Dual);
        }
        
        private void ButtonCreateReportList_ListItemClick(object sender, ListItemClickEventArgs e)
        {
            String Name = (e.Item as BarListItem).ItemLinks[e.Index].Caption;
            String Directory = null;
            switch (Name)
            {
                case "Nowy Raport":
                    EditReport(EreportType.New);
                    break;
                case "Raport Serii Statycznych":
                    Directory = Path.Combine(ReportsDirectory, "StaticReport.repx");
                    EditReport(EreportType.Static, Directory);
                    break;
                case "Raport Serii Dynamicznych":
                    Directory = Path.Combine(ReportsDirectory, "DynamicReport.repx");
                    EditReport(EreportType.Dynamic, Directory);
                    break;
                case "Raport Ważeń Porównawczych":
                    Directory = Path.Combine(ReportsDirectory, "DualReport.repx");
                    EditReport(EreportType.Dual, Directory);
                    break;
                case "Domyślny Serii Statycznych":
                    EditReport(EreportType.Static);
                    break;
                case "Domyślny Serii Dynamicznych":
                    EditReport(EreportType.Dynamic, Directory);
                    break;
                case "Domyślny Ważeń Porównawczych":
                    EditReport(EreportType.Dual, Directory);
                    break;
                default:
                    Directory = Path.Combine(ReportsDirectory, Name);
                    EditReport(EreportType.File, Directory);
                    break;
            }
        }

        private void ButtonTest_Click(object sender, EventArgs e)
        {
            String conn = "data source=" + ComboSerwer.SelectedItem.ToString() + ";initial catalog=master" + ";user id=" + TextUserName.Text +
              ";password=" + TextUserPassword.Text + ";MultipleActiveResultSets=True;App=EntityFramework";
            using (SqlConnection connection = new SqlConnection(conn))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("Select*from sys.databases", connection);
                    IDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        if (dr[0].ToString() != "master" && dr[0].ToString() != "model" && dr[0].ToString() != "msdb" && dr[0].ToString() != "tempdb")
                            ComboDataBase.Properties.Items.Add(dr[0].ToString());
                    }
                    ButtonSave.Enabled = true;
                    ComboDataBase.Enabled = true;
                }
                catch (Exception)
                {

                    XtraMessageBox.Show("Czy na pewno wprowadzono poprawne dane?", "błąd połączenia", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }

            }


        }

        #endregion

    }
}