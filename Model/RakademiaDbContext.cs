namespace RAkademia_v1.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RakademiaDbContext : DbContext
    {
        public RakademiaDbContext()
            : base("name=cn")
        {
        }

        public virtual DbSet<RAkademiaDynamiczne> RAkademiaDynamiczne { get; set; }
        public virtual DbSet<RAkademiaStatyczne> RAkademiaStatyczne { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RAkademiaDynamiczne>()
                .Property(e => e.ImieNazwisko)
                .IsUnicode(false);

            modelBuilder.Entity<RAkademiaDynamiczne>()
                .Property(e => e.seria)
                .IsUnicode(false);

            modelBuilder.Entity<RAkademiaDynamiczne>()
                .Property(e => e.Masa)
                .IsUnicode(false);

            modelBuilder.Entity<RAkademiaDynamiczne>()
                .Property(e => e.Tara)
                .IsUnicode(false);

            modelBuilder.Entity<RAkademiaDynamiczne>()
                .Property(e => e.Netto_mass)
                .HasPrecision(26, 2);

            modelBuilder.Entity<RAkademiaStatyczne>()
                .Property(e => e.ImieNazwisko)
                .IsUnicode(false);

            modelBuilder.Entity<RAkademiaStatyczne>()
                .Property(e => e.seria)
                .IsUnicode(false);

            modelBuilder.Entity<RAkademiaStatyczne>()
                .Property(e => e.Netto_Mass)
                .HasPrecision(26, 2);
        }
    }
}
