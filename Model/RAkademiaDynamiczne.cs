namespace RAkademia_v1.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RAkademiaDynamiczne")]
    public partial class RAkademiaDynamiczne
    {
        [StringLength(100)]
        public string ImieNazwisko { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string seria { get; set; }

        [StringLength(500)]
        public string nazwa { get; set; }

        [StringLength(22)]
        public string Masa { get; set; }

        [StringLength(22)]
        public string Tara { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime Date { get; set; }

        public decimal? Netto_mass { get; set; }
    }
}
