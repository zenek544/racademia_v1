﻿namespace RAkademia_v1
{
    partial class ReportViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip29 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem29 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip30 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem30 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip31 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem31 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip32 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem32 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip33 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem33 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem33 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip34 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem34 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem34 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip35 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem35 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem35 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip36 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem36 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem36 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip37 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem37 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem37 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip38 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem38 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem38 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip39 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem39 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem39 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip40 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem40 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem40 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip41 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem41 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem41 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip42 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem42 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem42 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip43 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem43 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem43 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip44 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem44 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem44 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip45 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem45 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem45 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip46 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem46 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem46 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip47 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem47 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem47 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip48 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem48 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem48 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip49 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem49 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem49 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip50 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem50 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem50 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip51 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem51 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem51 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip52 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem52 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem52 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportViewer));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.printPreviewBarItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.documentViewerRibbonController1 = new DevExpress.XtraPrinting.Preview.DocumentViewerRibbonController(this.components);
            this.documentViewer1 = new DevExpress.XtraPrinting.Preview.DocumentViewer();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.printPreviewStaticItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.progressBarEditItem2 = new DevExpress.XtraPrinting.Preview.ProgressBarEditItem();
            this.repositoryItemProgressBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.printPreviewBarItem53 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewStaticItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.zoomTrackBarEditItem2 = new DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem();
            this.repositoryItemZoomTrackBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.printPreviewBarItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem18 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem20 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem21 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem22 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem23 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem24 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem25 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem26 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem27 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem28 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem29 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem30 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem31 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem32 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem33 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem34 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem35 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem36 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem37 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem38 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem39 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem40 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem41 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem42 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem43 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem44 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem45 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem46 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem47 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem48 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem49 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem50 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem51 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem52 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.ribbonPage2 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.printPreviewRibbonPageGroup1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup2 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup3 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup4 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup5 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup6 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup7 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup8 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.backstageViewClientControl1 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.documentViewerBarManager1 = new DevExpress.XtraPrinting.Preview.DocumentViewerBarManager(this.components);
            this.previewBar2 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            this.printPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.verticalSpaceItem = new DevExpress.XtraBars.BarStaticItem();
            this.progressBarEditItem1 = new DevExpress.XtraPrinting.Preview.ProgressBarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.printPreviewBarItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.zoomTrackBarEditItem1 = new DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiDocumentMap = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiParameters = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiThumbnails = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiFind = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiHighlightEditingFields = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiCustomize = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiOpen = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiSave = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiPrint = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiPrintDirect = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiPageSetup = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiEditPageHF = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiScale = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiHandTool = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiMagnifier = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiZoomOut = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiZoom = new DevExpress.XtraPrinting.Preview.ZoomBarEditItem();
            this.printPreviewRepositoryItemComboBox1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox();
            this.bbiZoomIn = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiShowFirstPage = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiShowPrevPage = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiShowNextPage = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiShowLastPage = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiMultiplePages = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiFillBackground = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiWatermark = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiExportFile = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiSendFile = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.bbiClosePreview = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.miFile = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.miView = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.miPageLayout = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.miPageLayoutFacing = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.miPageLayoutContinuous = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.miToolbars = new DevExpress.XtraBars.BarToolbarsListItem();
            this.miBackground = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.printPreviewBarCheckItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem18 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentViewerRibbonController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentViewerBarManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printPreviewRepositoryItemComboBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.AutoHideEmptyItems = true;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.printPreviewBarItem2,
            this.printPreviewBarItem3,
            this.printPreviewBarItem4,
            this.printPreviewBarItem5,
            this.printPreviewBarItem6,
            this.printPreviewBarItem7,
            this.printPreviewBarItem8,
            this.printPreviewBarItem9,
            this.printPreviewBarItem10,
            this.printPreviewBarItem11,
            this.printPreviewBarItem12,
            this.printPreviewBarItem13,
            this.printPreviewBarItem14,
            this.printPreviewBarItem15,
            this.printPreviewBarItem16,
            this.printPreviewBarItem17,
            this.printPreviewBarItem18,
            this.printPreviewBarItem19,
            this.printPreviewBarItem20,
            this.printPreviewBarItem21,
            this.printPreviewBarItem22,
            this.printPreviewBarItem23,
            this.printPreviewBarItem24,
            this.printPreviewBarItem25,
            this.printPreviewBarItem26,
            this.printPreviewBarItem27,
            this.printPreviewBarItem28,
            this.printPreviewBarItem29,
            this.printPreviewBarItem30,
            this.printPreviewBarItem31,
            this.printPreviewBarItem32,
            this.printPreviewBarItem33,
            this.printPreviewBarItem34,
            this.printPreviewBarItem35,
            this.printPreviewBarItem36,
            this.printPreviewBarItem37,
            this.printPreviewBarItem38,
            this.printPreviewBarItem39,
            this.printPreviewBarItem40,
            this.printPreviewBarItem41,
            this.printPreviewBarItem42,
            this.printPreviewBarItem43,
            this.printPreviewBarItem44,
            this.printPreviewBarItem45,
            this.printPreviewBarItem46,
            this.printPreviewBarItem47,
            this.printPreviewBarItem48,
            this.printPreviewBarItem49,
            this.printPreviewBarItem50,
            this.printPreviewBarItem51,
            this.printPreviewBarItem52,
            this.printPreviewStaticItem3,
            this.barStaticItem1,
            this.progressBarEditItem2,
            this.printPreviewBarItem53,
            this.printPreviewStaticItem4,
            this.zoomTrackBarEditItem2});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 58;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage2});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar2,
            this.repositoryItemZoomTrackBar2});
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersInFormCaption = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1062, 144);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            this.ribbon.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // printPreviewBarItem2
            // 
            this.printPreviewBarItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem2.Caption = "Editing Fields";
            this.printPreviewBarItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HighlightEditingFields;
            this.printPreviewBarItem2.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem2.Enabled = false;
            this.printPreviewBarItem2.Id = 1;
            this.printPreviewBarItem2.Name = "printPreviewBarItem2";
            superToolTip1.FixedTooltipWidth = true;
            toolTipTitleItem1.Text = "Highlight Editing Fields";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Highlight all editing fields to quickly discover which of the document elements a" +
    "re editable.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.MaxWidth = 210;
            this.printPreviewBarItem2.SuperTip = superToolTip1;
            // 
            // documentViewerRibbonController1
            // 
            this.documentViewerRibbonController1.DocumentViewer = this.documentViewer1;
            this.documentViewerRibbonController1.RibbonControl = this.ribbon;
            this.documentViewerRibbonController1.RibbonStatusBar = this.ribbonStatusBar;
            // 
            // documentViewer1
            // 
            this.documentViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.documentViewer1.IsMetric = true;
            this.documentViewer1.Location = new System.Drawing.Point(0, 144);
            this.documentViewer1.Name = "documentViewer1";
            this.documentViewer1.Size = new System.Drawing.Size(1062, 468);
            this.documentViewer1.TabIndex = 2;
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.printPreviewStaticItem3);
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItem1);
            this.ribbonStatusBar.ItemLinks.Add(this.progressBarEditItem2);
            this.ribbonStatusBar.ItemLinks.Add(this.printPreviewBarItem53);
            this.ribbonStatusBar.ItemLinks.Add(this.printPreviewStaticItem4);
            this.ribbonStatusBar.ItemLinks.Add(this.zoomTrackBarEditItem2);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 612);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1062, 32);
            // 
            // printPreviewStaticItem3
            // 
            this.printPreviewStaticItem3.Caption = "Nothing";
            this.printPreviewStaticItem3.Id = 52;
            this.printPreviewStaticItem3.LeftIndent = 1;
            this.printPreviewStaticItem3.Name = "printPreviewStaticItem3";
            this.printPreviewStaticItem3.RightIndent = 1;
            this.printPreviewStaticItem3.Type = "PageOfPages";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barStaticItem1.Enabled = false;
            this.barStaticItem1.Id = 53;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInRuntime;
            // 
            // progressBarEditItem2
            // 
            this.progressBarEditItem2.ContextSpecifier = this.documentViewerRibbonController1;
            this.progressBarEditItem2.Edit = this.repositoryItemProgressBar2;
            this.progressBarEditItem2.EditHeight = 12;
            this.progressBarEditItem2.EditWidth = 150;
            this.progressBarEditItem2.Id = 54;
            this.progressBarEditItem2.Name = "progressBarEditItem2";
            this.progressBarEditItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemProgressBar2
            // 
            this.repositoryItemProgressBar2.Name = "repositoryItemProgressBar2";
            // 
            // printPreviewBarItem53
            // 
            this.printPreviewBarItem53.Caption = "Stop";
            this.printPreviewBarItem53.Command = DevExpress.XtraPrinting.PrintingSystemCommand.StopPageBuilding;
            this.printPreviewBarItem53.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem53.Enabled = false;
            this.printPreviewBarItem53.Hint = "Stop";
            this.printPreviewBarItem53.Id = 55;
            this.printPreviewBarItem53.Name = "printPreviewBarItem53";
            this.printPreviewBarItem53.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // printPreviewStaticItem4
            // 
            this.printPreviewStaticItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.printPreviewStaticItem4.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.printPreviewStaticItem4.Caption = "100%";
            this.printPreviewStaticItem4.Id = 56;
            this.printPreviewStaticItem4.Name = "printPreviewStaticItem4";
            this.printPreviewStaticItem4.Type = "ZoomFactorText";
            // 
            // zoomTrackBarEditItem2
            // 
            this.zoomTrackBarEditItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.zoomTrackBarEditItem2.ContextSpecifier = this.documentViewerRibbonController1;
            this.zoomTrackBarEditItem2.Edit = this.repositoryItemZoomTrackBar2;
            this.zoomTrackBarEditItem2.EditValue = 90;
            this.zoomTrackBarEditItem2.EditWidth = 140;
            this.zoomTrackBarEditItem2.Enabled = false;
            this.zoomTrackBarEditItem2.Id = 57;
            this.zoomTrackBarEditItem2.Name = "zoomTrackBarEditItem2";
            this.zoomTrackBarEditItem2.Range = new int[] {
        10,
        500};
            // 
            // repositoryItemZoomTrackBar2
            // 
            this.repositoryItemZoomTrackBar2.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemZoomTrackBar2.AllowFocused = false;
            this.repositoryItemZoomTrackBar2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar2.Maximum = 180;
            this.repositoryItemZoomTrackBar2.Name = "repositoryItemZoomTrackBar2";
            // 
            // printPreviewBarItem3
            // 
            this.printPreviewBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem3.Caption = "Bookmarks";
            this.printPreviewBarItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap;
            this.printPreviewBarItem3.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem3.Enabled = false;
            this.printPreviewBarItem3.Id = 2;
            this.printPreviewBarItem3.Name = "printPreviewBarItem3";
            superToolTip2.FixedTooltipWidth = true;
            toolTipTitleItem2.Text = "Document Map";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Open the Document Map, which allows you to navigate through a structural view of " +
    "the document.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.MaxWidth = 210;
            this.printPreviewBarItem3.SuperTip = superToolTip2;
            // 
            // printPreviewBarItem4
            // 
            this.printPreviewBarItem4.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem4.Caption = "Parameters";
            this.printPreviewBarItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Parameters;
            this.printPreviewBarItem4.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem4.Enabled = false;
            this.printPreviewBarItem4.Id = 3;
            this.printPreviewBarItem4.Name = "printPreviewBarItem4";
            superToolTip3.FixedTooltipWidth = true;
            toolTipTitleItem3.Text = "Parameters";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Open the Parameters pane, which allows you to enter values for report parameters." +
    "";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            superToolTip3.MaxWidth = 210;
            this.printPreviewBarItem4.SuperTip = superToolTip3;
            // 
            // printPreviewBarItem5
            // 
            this.printPreviewBarItem5.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem5.Caption = "Find";
            this.printPreviewBarItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find;
            this.printPreviewBarItem5.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem5.Enabled = false;
            this.printPreviewBarItem5.Id = 4;
            this.printPreviewBarItem5.Name = "printPreviewBarItem5";
            superToolTip4.FixedTooltipWidth = true;
            toolTipTitleItem4.Text = "Find";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Show the Find dialog to find text in the document.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip4.MaxWidth = 210;
            this.printPreviewBarItem5.SuperTip = superToolTip4;
            // 
            // printPreviewBarItem6
            // 
            this.printPreviewBarItem6.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem6.Caption = "Thumbnails";
            this.printPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Thumbnails;
            this.printPreviewBarItem6.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem6.Enabled = false;
            this.printPreviewBarItem6.Id = 5;
            this.printPreviewBarItem6.Name = "printPreviewBarItem6";
            superToolTip5.FixedTooltipWidth = true;
            toolTipTitleItem5.Text = "Thumbnails";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Open the Thumbnails, which allows you to navigate through the document.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            superToolTip5.MaxWidth = 210;
            this.printPreviewBarItem6.SuperTip = superToolTip5;
            // 
            // printPreviewBarItem7
            // 
            this.printPreviewBarItem7.Caption = "Options";
            this.printPreviewBarItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize;
            this.printPreviewBarItem7.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem7.Enabled = false;
            this.printPreviewBarItem7.Id = 6;
            this.printPreviewBarItem7.Name = "printPreviewBarItem7";
            superToolTip6.FixedTooltipWidth = true;
            toolTipTitleItem6.Text = "Options";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Open the Print Options dialog, in which you can change printing options.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip6.MaxWidth = 210;
            this.printPreviewBarItem7.SuperTip = superToolTip6;
            // 
            // printPreviewBarItem8
            // 
            this.printPreviewBarItem8.Caption = "Print";
            this.printPreviewBarItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print;
            this.printPreviewBarItem8.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem8.Enabled = false;
            this.printPreviewBarItem8.Id = 7;
            this.printPreviewBarItem8.Name = "printPreviewBarItem8";
            superToolTip7.FixedTooltipWidth = true;
            toolTipTitleItem7.Text = "Print (Ctrl+P)";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Select a printer, number of copies and other printing options before printing.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            superToolTip7.MaxWidth = 210;
            this.printPreviewBarItem8.SuperTip = superToolTip7;
            // 
            // printPreviewBarItem9
            // 
            this.printPreviewBarItem9.Caption = "Quick Print";
            this.printPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect;
            this.printPreviewBarItem9.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem9.Enabled = false;
            this.printPreviewBarItem9.Id = 8;
            this.printPreviewBarItem9.Name = "printPreviewBarItem9";
            superToolTip8.FixedTooltipWidth = true;
            toolTipTitleItem8.Text = "Quick Print";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Send the document directly to the default printer without making changes.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            superToolTip8.MaxWidth = 210;
            this.printPreviewBarItem9.SuperTip = superToolTip8;
            // 
            // printPreviewBarItem10
            // 
            this.printPreviewBarItem10.Caption = "Custom Margins...";
            this.printPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.printPreviewBarItem10.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem10.Enabled = false;
            this.printPreviewBarItem10.Id = 9;
            this.printPreviewBarItem10.Name = "printPreviewBarItem10";
            superToolTip9.FixedTooltipWidth = true;
            toolTipTitleItem9.Text = "Page Setup";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Show the Page Setup dialog.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            superToolTip9.MaxWidth = 210;
            this.printPreviewBarItem10.SuperTip = superToolTip9;
            // 
            // printPreviewBarItem11
            // 
            this.printPreviewBarItem11.Caption = "Header/Footer";
            this.printPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF;
            this.printPreviewBarItem11.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem11.Enabled = false;
            this.printPreviewBarItem11.Id = 10;
            this.printPreviewBarItem11.Name = "printPreviewBarItem11";
            superToolTip10.FixedTooltipWidth = true;
            toolTipTitleItem10.Text = "Header and Footer";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Edit the header and footer of the document.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip10.MaxWidth = 210;
            this.printPreviewBarItem11.SuperTip = superToolTip10;
            // 
            // printPreviewBarItem12
            // 
            this.printPreviewBarItem12.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem12.Caption = "Scale";
            this.printPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.printPreviewBarItem12.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem12.Enabled = false;
            this.printPreviewBarItem12.Id = 11;
            this.printPreviewBarItem12.Name = "printPreviewBarItem12";
            superToolTip11.FixedTooltipWidth = true;
            toolTipTitleItem11.Text = "Scale";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Stretch or shrink the printed output to a percentage of its actual size.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            superToolTip11.MaxWidth = 210;
            this.printPreviewBarItem12.SuperTip = superToolTip11;
            // 
            // printPreviewBarItem13
            // 
            this.printPreviewBarItem13.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem13.Caption = "Pointer";
            this.printPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Pointer;
            this.printPreviewBarItem13.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem13.Down = true;
            this.printPreviewBarItem13.Enabled = false;
            this.printPreviewBarItem13.GroupIndex = 1;
            this.printPreviewBarItem13.Id = 12;
            this.printPreviewBarItem13.Name = "printPreviewBarItem13";
            this.printPreviewBarItem13.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip12.FixedTooltipWidth = true;
            toolTipTitleItem12.Text = "Mouse Pointer";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Show the mouse pointer.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            superToolTip12.MaxWidth = 210;
            this.printPreviewBarItem13.SuperTip = superToolTip12;
            // 
            // printPreviewBarItem14
            // 
            this.printPreviewBarItem14.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem14.Caption = "Hand Tool";
            this.printPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.printPreviewBarItem14.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem14.Enabled = false;
            this.printPreviewBarItem14.GroupIndex = 1;
            this.printPreviewBarItem14.Id = 13;
            this.printPreviewBarItem14.Name = "printPreviewBarItem14";
            this.printPreviewBarItem14.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip13.FixedTooltipWidth = true;
            toolTipTitleItem13.Text = "Hand Tool";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Invoke the Hand tool to manually scroll through pages.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            superToolTip13.MaxWidth = 210;
            this.printPreviewBarItem14.SuperTip = superToolTip13;
            // 
            // printPreviewBarItem15
            // 
            this.printPreviewBarItem15.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem15.Caption = "Magnifier";
            this.printPreviewBarItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.printPreviewBarItem15.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem15.Enabled = false;
            this.printPreviewBarItem15.GroupIndex = 1;
            this.printPreviewBarItem15.Id = 14;
            this.printPreviewBarItem15.Name = "printPreviewBarItem15";
            this.printPreviewBarItem15.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip14.FixedTooltipWidth = true;
            toolTipTitleItem14.Text = "Magnifier";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Invoke the Magnifier tool.\r\n\r\nClicking once on a document zooms it so that a sing" +
    "le page becomes entirely visible, while clicking another time zooms it to 100% o" +
    "f the normal size.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            superToolTip14.MaxWidth = 210;
            this.printPreviewBarItem15.SuperTip = superToolTip14;
            // 
            // printPreviewBarItem16
            // 
            this.printPreviewBarItem16.Caption = "Zoom Out";
            this.printPreviewBarItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.printPreviewBarItem16.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem16.Enabled = false;
            this.printPreviewBarItem16.Id = 15;
            this.printPreviewBarItem16.Name = "printPreviewBarItem16";
            superToolTip15.FixedTooltipWidth = true;
            toolTipTitleItem15.Text = "Zoom Out";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Zoom out to see more of the page at a reduced size.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            superToolTip15.MaxWidth = 210;
            this.printPreviewBarItem16.SuperTip = superToolTip15;
            // 
            // printPreviewBarItem17
            // 
            this.printPreviewBarItem17.Caption = "Zoom In";
            this.printPreviewBarItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.printPreviewBarItem17.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem17.Enabled = false;
            this.printPreviewBarItem17.Id = 16;
            this.printPreviewBarItem17.Name = "printPreviewBarItem17";
            superToolTip16.FixedTooltipWidth = true;
            toolTipTitleItem16.Text = "Zoom In";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Zoom in to get a close-up view of the document.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            superToolTip16.MaxWidth = 210;
            this.printPreviewBarItem17.SuperTip = superToolTip16;
            // 
            // printPreviewBarItem18
            // 
            this.printPreviewBarItem18.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem18.Caption = "Zoom";
            this.printPreviewBarItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Zoom;
            this.printPreviewBarItem18.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem18.Enabled = false;
            this.printPreviewBarItem18.Id = 17;
            this.printPreviewBarItem18.Name = "printPreviewBarItem18";
            superToolTip17.FixedTooltipWidth = true;
            toolTipTitleItem17.Text = "Zoom";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Change the zoom level of the document preview.";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            superToolTip17.MaxWidth = 210;
            this.printPreviewBarItem18.SuperTip = superToolTip17;
            // 
            // printPreviewBarItem19
            // 
            this.printPreviewBarItem19.Caption = "First Page";
            this.printPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage;
            this.printPreviewBarItem19.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem19.Enabled = false;
            this.printPreviewBarItem19.Id = 18;
            this.printPreviewBarItem19.Name = "printPreviewBarItem19";
            superToolTip18.FixedTooltipWidth = true;
            toolTipTitleItem18.Text = "First Page (Home)";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Navigate to the first page of the document.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            superToolTip18.MaxWidth = 210;
            this.printPreviewBarItem19.SuperTip = superToolTip18;
            // 
            // printPreviewBarItem20
            // 
            this.printPreviewBarItem20.Caption = "Previous Page";
            this.printPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage;
            this.printPreviewBarItem20.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem20.Enabled = false;
            this.printPreviewBarItem20.Id = 19;
            this.printPreviewBarItem20.Name = "printPreviewBarItem20";
            superToolTip19.FixedTooltipWidth = true;
            toolTipTitleItem19.Text = "Previous Page (Left Arrow)";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Navigate to the previous page of the document.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            superToolTip19.MaxWidth = 210;
            this.printPreviewBarItem20.SuperTip = superToolTip19;
            // 
            // printPreviewBarItem21
            // 
            this.printPreviewBarItem21.Caption = "Next  Page ";
            this.printPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage;
            this.printPreviewBarItem21.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem21.Enabled = false;
            this.printPreviewBarItem21.Id = 20;
            this.printPreviewBarItem21.Name = "printPreviewBarItem21";
            superToolTip20.FixedTooltipWidth = true;
            toolTipTitleItem20.Text = "Next Page (Right Arrow)";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Navigate to the next page of the document.";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            superToolTip20.MaxWidth = 210;
            this.printPreviewBarItem21.SuperTip = superToolTip20;
            // 
            // printPreviewBarItem22
            // 
            this.printPreviewBarItem22.Caption = "Last  Page ";
            this.printPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage;
            this.printPreviewBarItem22.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem22.Enabled = false;
            this.printPreviewBarItem22.Id = 21;
            this.printPreviewBarItem22.Name = "printPreviewBarItem22";
            superToolTip21.FixedTooltipWidth = true;
            toolTipTitleItem21.Text = "Last Page (End)";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Navigate to the last page of the document.";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            superToolTip21.MaxWidth = 210;
            this.printPreviewBarItem22.SuperTip = superToolTip21;
            // 
            // printPreviewBarItem23
            // 
            this.printPreviewBarItem23.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem23.Caption = "Many Pages";
            this.printPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.printPreviewBarItem23.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem23.Enabled = false;
            this.printPreviewBarItem23.Id = 22;
            this.printPreviewBarItem23.Name = "printPreviewBarItem23";
            superToolTip22.FixedTooltipWidth = true;
            toolTipTitleItem22.Text = "View Many Pages";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Choose the page layout to arrange the document pages in preview.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            superToolTip22.MaxWidth = 210;
            this.printPreviewBarItem23.SuperTip = superToolTip22;
            // 
            // printPreviewBarItem24
            // 
            this.printPreviewBarItem24.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem24.Caption = "Page Color";
            this.printPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.printPreviewBarItem24.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem24.Enabled = false;
            this.printPreviewBarItem24.Id = 23;
            this.printPreviewBarItem24.Name = "printPreviewBarItem24";
            superToolTip23.FixedTooltipWidth = true;
            toolTipTitleItem23.Text = "Background Color";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "Choose a color for the background of the document pages.";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem23);
            superToolTip23.MaxWidth = 210;
            this.printPreviewBarItem24.SuperTip = superToolTip23;
            // 
            // printPreviewBarItem25
            // 
            this.printPreviewBarItem25.Caption = "Watermark";
            this.printPreviewBarItem25.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.printPreviewBarItem25.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem25.Enabled = false;
            this.printPreviewBarItem25.Id = 24;
            this.printPreviewBarItem25.Name = "printPreviewBarItem25";
            superToolTip24.FixedTooltipWidth = true;
            toolTipTitleItem24.Text = "Watermark";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "Insert ghosted text or image behind the content of a page.\r\n\r\nThis is often used " +
    "to indicate that a document is to be treated specially.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem24);
            superToolTip24.MaxWidth = 210;
            this.printPreviewBarItem25.SuperTip = superToolTip24;
            // 
            // printPreviewBarItem26
            // 
            this.printPreviewBarItem26.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem26.Caption = "Export To";
            this.printPreviewBarItem26.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.printPreviewBarItem26.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem26.Enabled = false;
            this.printPreviewBarItem26.Id = 25;
            this.printPreviewBarItem26.Name = "printPreviewBarItem26";
            superToolTip25.FixedTooltipWidth = true;
            toolTipTitleItem25.Text = "Export To...";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "Export the current document in one of the available formats, and save it to the f" +
    "ile on a disk.";
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem25);
            superToolTip25.MaxWidth = 210;
            this.printPreviewBarItem26.SuperTip = superToolTip25;
            // 
            // printPreviewBarItem27
            // 
            this.printPreviewBarItem27.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem27.Caption = "E-Mail As";
            this.printPreviewBarItem27.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.printPreviewBarItem27.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem27.Enabled = false;
            this.printPreviewBarItem27.Id = 26;
            this.printPreviewBarItem27.Name = "printPreviewBarItem27";
            superToolTip26.FixedTooltipWidth = true;
            toolTipTitleItem26.Text = "E-Mail As...";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "Export the current document in one of the available formats, and attach it to the" +
    " e-mail.";
            superToolTip26.Items.Add(toolTipTitleItem26);
            superToolTip26.Items.Add(toolTipItem26);
            superToolTip26.MaxWidth = 210;
            this.printPreviewBarItem27.SuperTip = superToolTip26;
            // 
            // printPreviewBarItem28
            // 
            this.printPreviewBarItem28.Caption = "Close";
            this.printPreviewBarItem28.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.printPreviewBarItem28.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem28.Enabled = false;
            this.printPreviewBarItem28.Id = 27;
            this.printPreviewBarItem28.Name = "printPreviewBarItem28";
            superToolTip27.FixedTooltipWidth = true;
            toolTipTitleItem27.Text = "Close Print Preview";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "Close Print Preview of the document.";
            superToolTip27.Items.Add(toolTipTitleItem27);
            superToolTip27.Items.Add(toolTipItem27);
            superToolTip27.MaxWidth = 210;
            this.printPreviewBarItem28.SuperTip = superToolTip27;
            // 
            // printPreviewBarItem29
            // 
            this.printPreviewBarItem29.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem29.Caption = "Orientation";
            this.printPreviewBarItem29.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageOrientation;
            this.printPreviewBarItem29.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem29.Enabled = false;
            this.printPreviewBarItem29.Id = 28;
            this.printPreviewBarItem29.Name = "printPreviewBarItem29";
            superToolTip28.FixedTooltipWidth = true;
            toolTipTitleItem28.Text = "Page Orientation";
            toolTipItem28.LeftIndent = 6;
            toolTipItem28.Text = "Switch the pages between portrait and landscape layouts.";
            superToolTip28.Items.Add(toolTipTitleItem28);
            superToolTip28.Items.Add(toolTipItem28);
            superToolTip28.MaxWidth = 210;
            this.printPreviewBarItem29.SuperTip = superToolTip28;
            // 
            // printPreviewBarItem30
            // 
            this.printPreviewBarItem30.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem30.Caption = "Size";
            this.printPreviewBarItem30.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PaperSize;
            this.printPreviewBarItem30.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem30.Enabled = false;
            this.printPreviewBarItem30.Id = 29;
            this.printPreviewBarItem30.Name = "printPreviewBarItem30";
            superToolTip29.FixedTooltipWidth = true;
            toolTipTitleItem29.Text = "Page Size";
            toolTipItem29.LeftIndent = 6;
            toolTipItem29.Text = "Choose the paper size of the document.";
            superToolTip29.Items.Add(toolTipTitleItem29);
            superToolTip29.Items.Add(toolTipItem29);
            superToolTip29.MaxWidth = 210;
            this.printPreviewBarItem30.SuperTip = superToolTip29;
            // 
            // printPreviewBarItem31
            // 
            this.printPreviewBarItem31.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem31.Caption = "Margins";
            this.printPreviewBarItem31.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageMargins;
            this.printPreviewBarItem31.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem31.Enabled = false;
            this.printPreviewBarItem31.Id = 30;
            this.printPreviewBarItem31.Name = "printPreviewBarItem31";
            superToolTip30.FixedTooltipWidth = true;
            toolTipTitleItem30.Text = "Page Margins";
            toolTipItem30.LeftIndent = 6;
            toolTipItem30.Text = "Select the margin sizes for the entire document.\r\n\r\nTo apply specific margin size" +
    "s to the document, click Custom Margins.";
            superToolTip30.Items.Add(toolTipTitleItem30);
            superToolTip30.Items.Add(toolTipItem30);
            superToolTip30.MaxWidth = 210;
            this.printPreviewBarItem31.SuperTip = superToolTip30;
            // 
            // printPreviewBarItem32
            // 
            this.printPreviewBarItem32.Caption = "PDF File";
            this.printPreviewBarItem32.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.printPreviewBarItem32.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem32.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem32.Enabled = false;
            this.printPreviewBarItem32.Id = 31;
            this.printPreviewBarItem32.Name = "printPreviewBarItem32";
            superToolTip31.FixedTooltipWidth = true;
            toolTipTitleItem31.Text = "E-Mail As PDF";
            toolTipItem31.LeftIndent = 6;
            toolTipItem31.Text = "Export the document to PDF and attach it to the e-mail.";
            superToolTip31.Items.Add(toolTipTitleItem31);
            superToolTip31.Items.Add(toolTipItem31);
            superToolTip31.MaxWidth = 210;
            this.printPreviewBarItem32.SuperTip = superToolTip31;
            // 
            // printPreviewBarItem33
            // 
            this.printPreviewBarItem33.Caption = "Text File";
            this.printPreviewBarItem33.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.printPreviewBarItem33.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem33.Description = "Plain Text";
            this.printPreviewBarItem33.Enabled = false;
            this.printPreviewBarItem33.Id = 32;
            this.printPreviewBarItem33.Name = "printPreviewBarItem33";
            superToolTip32.FixedTooltipWidth = true;
            toolTipTitleItem32.Text = "E-Mail As Text";
            toolTipItem32.LeftIndent = 6;
            toolTipItem32.Text = "Export the document to Text and attach it to the e-mail.";
            superToolTip32.Items.Add(toolTipTitleItem32);
            superToolTip32.Items.Add(toolTipItem32);
            superToolTip32.MaxWidth = 210;
            this.printPreviewBarItem33.SuperTip = superToolTip32;
            // 
            // printPreviewBarItem34
            // 
            this.printPreviewBarItem34.Caption = "CSV File";
            this.printPreviewBarItem34.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.printPreviewBarItem34.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem34.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem34.Enabled = false;
            this.printPreviewBarItem34.Id = 33;
            this.printPreviewBarItem34.Name = "printPreviewBarItem34";
            superToolTip33.FixedTooltipWidth = true;
            toolTipTitleItem33.Text = "E-Mail As CSV";
            toolTipItem33.LeftIndent = 6;
            toolTipItem33.Text = "Export the document to CSV and attach it to the e-mail.";
            superToolTip33.Items.Add(toolTipTitleItem33);
            superToolTip33.Items.Add(toolTipItem33);
            superToolTip33.MaxWidth = 210;
            this.printPreviewBarItem34.SuperTip = superToolTip33;
            // 
            // printPreviewBarItem35
            // 
            this.printPreviewBarItem35.Caption = "MHT File";
            this.printPreviewBarItem35.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.printPreviewBarItem35.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem35.Description = "Single File Web Page";
            this.printPreviewBarItem35.Enabled = false;
            this.printPreviewBarItem35.Id = 34;
            this.printPreviewBarItem35.Name = "printPreviewBarItem35";
            superToolTip34.FixedTooltipWidth = true;
            toolTipTitleItem34.Text = "E-Mail As MHT";
            toolTipItem34.LeftIndent = 6;
            toolTipItem34.Text = "Export the document to MHT and attach it to the e-mail.";
            superToolTip34.Items.Add(toolTipTitleItem34);
            superToolTip34.Items.Add(toolTipItem34);
            superToolTip34.MaxWidth = 210;
            this.printPreviewBarItem35.SuperTip = superToolTip34;
            // 
            // printPreviewBarItem36
            // 
            this.printPreviewBarItem36.Caption = "XLS File";
            this.printPreviewBarItem36.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.printPreviewBarItem36.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem36.Description = "Microsoft Excel 2000-2003 Workbook";
            this.printPreviewBarItem36.Enabled = false;
            this.printPreviewBarItem36.Id = 35;
            this.printPreviewBarItem36.Name = "printPreviewBarItem36";
            superToolTip35.FixedTooltipWidth = true;
            toolTipTitleItem35.Text = "E-Mail As XLS";
            toolTipItem35.LeftIndent = 6;
            toolTipItem35.Text = "Export the document to XLS and attach it to the e-mail.";
            superToolTip35.Items.Add(toolTipTitleItem35);
            superToolTip35.Items.Add(toolTipItem35);
            superToolTip35.MaxWidth = 210;
            this.printPreviewBarItem36.SuperTip = superToolTip35;
            // 
            // printPreviewBarItem37
            // 
            this.printPreviewBarItem37.Caption = "XLSX File";
            this.printPreviewBarItem37.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx;
            this.printPreviewBarItem37.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem37.Description = "Microsoft Excel 2007 Workbook";
            this.printPreviewBarItem37.Enabled = false;
            this.printPreviewBarItem37.Id = 36;
            this.printPreviewBarItem37.Name = "printPreviewBarItem37";
            superToolTip36.FixedTooltipWidth = true;
            toolTipTitleItem36.Text = "E-Mail As XLSX";
            toolTipItem36.LeftIndent = 6;
            toolTipItem36.Text = "Export the document to XLSX and attach it to the e-mail.";
            superToolTip36.Items.Add(toolTipTitleItem36);
            superToolTip36.Items.Add(toolTipItem36);
            superToolTip36.MaxWidth = 210;
            this.printPreviewBarItem37.SuperTip = superToolTip36;
            // 
            // printPreviewBarItem38
            // 
            this.printPreviewBarItem38.Caption = "RTF File";
            this.printPreviewBarItem38.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.printPreviewBarItem38.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem38.Description = "Rich Text Format";
            this.printPreviewBarItem38.Enabled = false;
            this.printPreviewBarItem38.Id = 37;
            this.printPreviewBarItem38.Name = "printPreviewBarItem38";
            superToolTip37.FixedTooltipWidth = true;
            toolTipTitleItem37.Text = "E-Mail As RTF";
            toolTipItem37.LeftIndent = 6;
            toolTipItem37.Text = "Export the document to RTF and attach it to the e-mail.";
            superToolTip37.Items.Add(toolTipTitleItem37);
            superToolTip37.Items.Add(toolTipItem37);
            superToolTip37.MaxWidth = 210;
            this.printPreviewBarItem38.SuperTip = superToolTip37;
            // 
            // printPreviewBarItem39
            // 
            this.printPreviewBarItem39.Caption = "DOCX File";
            this.printPreviewBarItem39.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendDocx;
            this.printPreviewBarItem39.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem39.Description = "Microsoft Word 2007 Document";
            this.printPreviewBarItem39.Enabled = false;
            this.printPreviewBarItem39.Id = 38;
            this.printPreviewBarItem39.Name = "printPreviewBarItem39";
            superToolTip38.FixedTooltipWidth = true;
            toolTipTitleItem38.Text = "E-Mail As DOCX";
            toolTipItem38.LeftIndent = 6;
            toolTipItem38.Text = "Export the document to DOCX and attach it to the e-mail.";
            superToolTip38.Items.Add(toolTipTitleItem38);
            superToolTip38.Items.Add(toolTipItem38);
            superToolTip38.MaxWidth = 210;
            this.printPreviewBarItem39.SuperTip = superToolTip38;
            // 
            // printPreviewBarItem40
            // 
            this.printPreviewBarItem40.Caption = "Image File";
            this.printPreviewBarItem40.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.printPreviewBarItem40.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem40.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem40.Enabled = false;
            this.printPreviewBarItem40.Id = 39;
            this.printPreviewBarItem40.Name = "printPreviewBarItem40";
            superToolTip39.FixedTooltipWidth = true;
            toolTipTitleItem39.Text = "E-Mail As Image";
            toolTipItem39.LeftIndent = 6;
            toolTipItem39.Text = "Export the document to Image and attach it to the e-mail.";
            superToolTip39.Items.Add(toolTipTitleItem39);
            superToolTip39.Items.Add(toolTipItem39);
            superToolTip39.MaxWidth = 210;
            this.printPreviewBarItem40.SuperTip = superToolTip39;
            // 
            // printPreviewBarItem41
            // 
            this.printPreviewBarItem41.Caption = "PDF File";
            this.printPreviewBarItem41.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.printPreviewBarItem41.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem41.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem41.Enabled = false;
            this.printPreviewBarItem41.Id = 40;
            this.printPreviewBarItem41.Name = "printPreviewBarItem41";
            superToolTip40.FixedTooltipWidth = true;
            toolTipTitleItem40.Text = "Export to PDF";
            toolTipItem40.LeftIndent = 6;
            toolTipItem40.Text = "Export the document to PDF and save it to the file on a disk.";
            superToolTip40.Items.Add(toolTipTitleItem40);
            superToolTip40.Items.Add(toolTipItem40);
            superToolTip40.MaxWidth = 210;
            this.printPreviewBarItem41.SuperTip = superToolTip40;
            // 
            // printPreviewBarItem42
            // 
            this.printPreviewBarItem42.Caption = "HTML File";
            this.printPreviewBarItem42.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.printPreviewBarItem42.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem42.Description = "Web Page";
            this.printPreviewBarItem42.Enabled = false;
            this.printPreviewBarItem42.Id = 41;
            this.printPreviewBarItem42.Name = "printPreviewBarItem42";
            superToolTip41.FixedTooltipWidth = true;
            toolTipTitleItem41.Text = "Export to HTML";
            toolTipItem41.LeftIndent = 6;
            toolTipItem41.Text = "Export the document to HTML and save it to the file on a disk.";
            superToolTip41.Items.Add(toolTipTitleItem41);
            superToolTip41.Items.Add(toolTipItem41);
            superToolTip41.MaxWidth = 210;
            this.printPreviewBarItem42.SuperTip = superToolTip41;
            // 
            // printPreviewBarItem43
            // 
            this.printPreviewBarItem43.Caption = "Text File";
            this.printPreviewBarItem43.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.printPreviewBarItem43.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem43.Description = "Plain Text";
            this.printPreviewBarItem43.Enabled = false;
            this.printPreviewBarItem43.Id = 42;
            this.printPreviewBarItem43.Name = "printPreviewBarItem43";
            superToolTip42.FixedTooltipWidth = true;
            toolTipTitleItem42.Text = "Export to Text";
            toolTipItem42.LeftIndent = 6;
            toolTipItem42.Text = "Export the document to Text and save it to the file on a disk.";
            superToolTip42.Items.Add(toolTipTitleItem42);
            superToolTip42.Items.Add(toolTipItem42);
            superToolTip42.MaxWidth = 210;
            this.printPreviewBarItem43.SuperTip = superToolTip42;
            // 
            // printPreviewBarItem44
            // 
            this.printPreviewBarItem44.Caption = "CSV File";
            this.printPreviewBarItem44.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.printPreviewBarItem44.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem44.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem44.Enabled = false;
            this.printPreviewBarItem44.Id = 43;
            this.printPreviewBarItem44.Name = "printPreviewBarItem44";
            superToolTip43.FixedTooltipWidth = true;
            toolTipTitleItem43.Text = "Export to CSV";
            toolTipItem43.LeftIndent = 6;
            toolTipItem43.Text = "Export the document to CSV and save it to the file on a disk.";
            superToolTip43.Items.Add(toolTipTitleItem43);
            superToolTip43.Items.Add(toolTipItem43);
            superToolTip43.MaxWidth = 210;
            this.printPreviewBarItem44.SuperTip = superToolTip43;
            // 
            // printPreviewBarItem45
            // 
            this.printPreviewBarItem45.Caption = "MHT File";
            this.printPreviewBarItem45.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.printPreviewBarItem45.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem45.Description = "Single File Web Page";
            this.printPreviewBarItem45.Enabled = false;
            this.printPreviewBarItem45.Id = 44;
            this.printPreviewBarItem45.Name = "printPreviewBarItem45";
            superToolTip44.FixedTooltipWidth = true;
            toolTipTitleItem44.Text = "Export to MHT";
            toolTipItem44.LeftIndent = 6;
            toolTipItem44.Text = "Export the document to MHT and save it to the file on a disk.";
            superToolTip44.Items.Add(toolTipTitleItem44);
            superToolTip44.Items.Add(toolTipItem44);
            superToolTip44.MaxWidth = 210;
            this.printPreviewBarItem45.SuperTip = superToolTip44;
            // 
            // printPreviewBarItem46
            // 
            this.printPreviewBarItem46.Caption = "XLS File";
            this.printPreviewBarItem46.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.printPreviewBarItem46.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem46.Description = "Microsoft Excel 2000-2003 Workbook";
            this.printPreviewBarItem46.Enabled = false;
            this.printPreviewBarItem46.Id = 45;
            this.printPreviewBarItem46.Name = "printPreviewBarItem46";
            superToolTip45.FixedTooltipWidth = true;
            toolTipTitleItem45.Text = "Export to XLS";
            toolTipItem45.LeftIndent = 6;
            toolTipItem45.Text = "Export the document to XLS and save it to the file on a disk.";
            superToolTip45.Items.Add(toolTipTitleItem45);
            superToolTip45.Items.Add(toolTipItem45);
            superToolTip45.MaxWidth = 210;
            this.printPreviewBarItem46.SuperTip = superToolTip45;
            // 
            // printPreviewBarItem47
            // 
            this.printPreviewBarItem47.Caption = "XLSX File";
            this.printPreviewBarItem47.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx;
            this.printPreviewBarItem47.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem47.Description = "Microsoft Excel 2007 Workbook";
            this.printPreviewBarItem47.Enabled = false;
            this.printPreviewBarItem47.Id = 46;
            this.printPreviewBarItem47.Name = "printPreviewBarItem47";
            superToolTip46.FixedTooltipWidth = true;
            toolTipTitleItem46.Text = "Export to XLSX";
            toolTipItem46.LeftIndent = 6;
            toolTipItem46.Text = "Export the document to XLSX and save it to the file on a disk.";
            superToolTip46.Items.Add(toolTipTitleItem46);
            superToolTip46.Items.Add(toolTipItem46);
            superToolTip46.MaxWidth = 210;
            this.printPreviewBarItem47.SuperTip = superToolTip46;
            // 
            // printPreviewBarItem48
            // 
            this.printPreviewBarItem48.Caption = "RTF File";
            this.printPreviewBarItem48.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.printPreviewBarItem48.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem48.Description = "Rich Text Format";
            this.printPreviewBarItem48.Enabled = false;
            this.printPreviewBarItem48.Id = 47;
            this.printPreviewBarItem48.Name = "printPreviewBarItem48";
            superToolTip47.FixedTooltipWidth = true;
            toolTipTitleItem47.Text = "Export to RTF";
            toolTipItem47.LeftIndent = 6;
            toolTipItem47.Text = "Export the document to RTF and save it to the file on a disk.";
            superToolTip47.Items.Add(toolTipTitleItem47);
            superToolTip47.Items.Add(toolTipItem47);
            superToolTip47.MaxWidth = 210;
            this.printPreviewBarItem48.SuperTip = superToolTip47;
            // 
            // printPreviewBarItem49
            // 
            this.printPreviewBarItem49.Caption = "DOCX File";
            this.printPreviewBarItem49.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportDocx;
            this.printPreviewBarItem49.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem49.Description = "Microsoft Word 2007 Document";
            this.printPreviewBarItem49.Enabled = false;
            this.printPreviewBarItem49.Id = 48;
            this.printPreviewBarItem49.Name = "printPreviewBarItem49";
            superToolTip48.FixedTooltipWidth = true;
            toolTipTitleItem48.Text = "Export to DOCX";
            toolTipItem48.LeftIndent = 6;
            toolTipItem48.Text = "Export the document to DOCX and save it to the file on a disk.";
            superToolTip48.Items.Add(toolTipTitleItem48);
            superToolTip48.Items.Add(toolTipItem48);
            superToolTip48.MaxWidth = 210;
            this.printPreviewBarItem49.SuperTip = superToolTip48;
            // 
            // printPreviewBarItem50
            // 
            this.printPreviewBarItem50.Caption = "Image File";
            this.printPreviewBarItem50.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.printPreviewBarItem50.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem50.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem50.Enabled = false;
            this.printPreviewBarItem50.Id = 49;
            this.printPreviewBarItem50.Name = "printPreviewBarItem50";
            superToolTip49.FixedTooltipWidth = true;
            toolTipTitleItem49.Text = "Export to Image";
            toolTipItem49.LeftIndent = 6;
            toolTipItem49.Text = "Export the document to Image and save it to the file on a disk.";
            superToolTip49.Items.Add(toolTipTitleItem49);
            superToolTip49.Items.Add(toolTipItem49);
            superToolTip49.MaxWidth = 210;
            this.printPreviewBarItem50.SuperTip = superToolTip49;
            // 
            // printPreviewBarItem51
            // 
            this.printPreviewBarItem51.Caption = "Open";
            this.printPreviewBarItem51.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Open;
            this.printPreviewBarItem51.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem51.Enabled = false;
            this.printPreviewBarItem51.Id = 50;
            this.printPreviewBarItem51.Name = "printPreviewBarItem51";
            superToolTip50.FixedTooltipWidth = true;
            toolTipTitleItem50.Text = "Open (Ctrl + O)";
            toolTipItem50.LeftIndent = 6;
            toolTipItem50.Text = "Open a document.";
            superToolTip50.Items.Add(toolTipTitleItem50);
            superToolTip50.Items.Add(toolTipItem50);
            superToolTip50.MaxWidth = 210;
            this.printPreviewBarItem51.SuperTip = superToolTip50;
            // 
            // printPreviewBarItem52
            // 
            this.printPreviewBarItem52.Caption = "Save";
            this.printPreviewBarItem52.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Save;
            this.printPreviewBarItem52.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem52.Enabled = false;
            this.printPreviewBarItem52.Id = 51;
            this.printPreviewBarItem52.Name = "printPreviewBarItem52";
            superToolTip51.FixedTooltipWidth = true;
            toolTipTitleItem51.Text = "Save (Ctrl + S)";
            toolTipItem51.LeftIndent = 6;
            toolTipItem51.Text = "Save the document.";
            superToolTip51.Items.Add(toolTipTitleItem51);
            superToolTip51.Items.Add(toolTipItem51);
            superToolTip51.MaxWidth = 210;
            this.printPreviewBarItem52.SuperTip = superToolTip51;
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.ContextSpecifier = this.documentViewerRibbonController1;
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.printPreviewRibbonPageGroup1,
            this.printPreviewRibbonPageGroup2,
            this.printPreviewRibbonPageGroup3,
            this.printPreviewRibbonPageGroup4,
            this.printPreviewRibbonPageGroup5,
            this.printPreviewRibbonPageGroup6,
            this.printPreviewRibbonPageGroup7,
            this.printPreviewRibbonPageGroup8});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Print Preview";
            // 
            // printPreviewRibbonPageGroup1
            // 
            this.printPreviewRibbonPageGroup1.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup1.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.printPreviewBarItem51);
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.printPreviewBarItem52);
            this.printPreviewRibbonPageGroup1.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Document;
            this.printPreviewRibbonPageGroup1.Name = "printPreviewRibbonPageGroup1";
            this.printPreviewRibbonPageGroup1.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup1.Text = "Document";
            // 
            // printPreviewRibbonPageGroup2
            // 
            this.printPreviewRibbonPageGroup2.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup2.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem8);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem9);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem7);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem4);
            this.printPreviewRibbonPageGroup2.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Print;
            this.printPreviewRibbonPageGroup2.Name = "printPreviewRibbonPageGroup2";
            this.printPreviewRibbonPageGroup2.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup2.Text = "Print";
            // 
            // printPreviewRibbonPageGroup3
            // 
            this.printPreviewRibbonPageGroup3.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup3.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem11);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem12);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem31);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem29);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem30);
            this.printPreviewRibbonPageGroup3.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.PageSetup;
            this.printPreviewRibbonPageGroup3.Name = "printPreviewRibbonPageGroup3";
            superToolTip52.FixedTooltipWidth = true;
            toolTipTitleItem52.Text = "Page Setup";
            toolTipItem52.LeftIndent = 6;
            toolTipItem52.Text = "Show the Page Setup dialog.";
            superToolTip52.Items.Add(toolTipTitleItem52);
            superToolTip52.Items.Add(toolTipItem52);
            superToolTip52.MaxWidth = 210;
            this.printPreviewRibbonPageGroup3.SuperTip = superToolTip52;
            this.printPreviewRibbonPageGroup3.Text = "Page Setup";
            // 
            // printPreviewRibbonPageGroup4
            // 
            this.printPreviewRibbonPageGroup4.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup4.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem5);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem6);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem3);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem2);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem19, true);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem20);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem21);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem22);
            this.printPreviewRibbonPageGroup4.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.printPreviewRibbonPageGroup4.Name = "printPreviewRibbonPageGroup4";
            this.printPreviewRibbonPageGroup4.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup4.Text = "Navigation";
            // 
            // printPreviewRibbonPageGroup5
            // 
            this.printPreviewRibbonPageGroup5.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup5.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem13);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem14);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem15);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem23);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem16);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem18);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem17);
            this.printPreviewRibbonPageGroup5.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Zoom;
            this.printPreviewRibbonPageGroup5.Name = "printPreviewRibbonPageGroup5";
            this.printPreviewRibbonPageGroup5.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup5.Text = "Zoom";
            // 
            // printPreviewRibbonPageGroup6
            // 
            this.printPreviewRibbonPageGroup6.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup6.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem24);
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem25);
            this.printPreviewRibbonPageGroup6.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Background;
            this.printPreviewRibbonPageGroup6.Name = "printPreviewRibbonPageGroup6";
            this.printPreviewRibbonPageGroup6.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup6.Text = "Page Background";
            // 
            // printPreviewRibbonPageGroup7
            // 
            this.printPreviewRibbonPageGroup7.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup7.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem26);
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem27);
            this.printPreviewRibbonPageGroup7.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Export;
            this.printPreviewRibbonPageGroup7.Name = "printPreviewRibbonPageGroup7";
            this.printPreviewRibbonPageGroup7.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup7.Text = "Export";
            // 
            // printPreviewRibbonPageGroup8
            // 
            this.printPreviewRibbonPageGroup8.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup8.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup8.ItemLinks.Add(this.printPreviewBarItem28);
            this.printPreviewRibbonPageGroup8.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Close;
            this.printPreviewRibbonPageGroup8.Name = "printPreviewRibbonPageGroup8";
            this.printPreviewRibbonPageGroup8.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup8.Text = "Close";
            // 
            // backstageViewClientControl1
            // 
            this.backstageViewClientControl1.Location = new System.Drawing.Point(132, 65);
            this.backstageViewClientControl1.Name = "backstageViewClientControl1";
            this.backstageViewClientControl1.Size = new System.Drawing.Size(347, 84);
            this.backstageViewClientControl1.TabIndex = 1;
            // 
            // documentViewerBarManager1
            // 
            this.documentViewerBarManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.previewBar2});
            this.documentViewerBarManager1.DockControls.Add(this.barDockControlTop);
            this.documentViewerBarManager1.DockControls.Add(this.barDockControlBottom);
            this.documentViewerBarManager1.DockControls.Add(this.barDockControlLeft);
            this.documentViewerBarManager1.DockControls.Add(this.barDockControlRight);
            this.documentViewerBarManager1.DocumentViewer = this.documentViewer1;
            this.documentViewerBarManager1.Form = this;
            this.documentViewerBarManager1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("documentViewerBarManager1.ImageStream")));
            this.documentViewerBarManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.printPreviewStaticItem1,
            this.verticalSpaceItem,
            this.progressBarEditItem1,
            this.printPreviewBarItem1,
            this.printPreviewStaticItem2,
            this.zoomTrackBarEditItem1,
            this.bbiDocumentMap,
            this.bbiParameters,
            this.bbiThumbnails,
            this.bbiFind,
            this.bbiHighlightEditingFields,
            this.bbiCustomize,
            this.bbiOpen,
            this.bbiSave,
            this.bbiPrint,
            this.bbiPrintDirect,
            this.bbiPageSetup,
            this.bbiEditPageHF,
            this.bbiScale,
            this.bbiHandTool,
            this.bbiMagnifier,
            this.bbiZoomOut,
            this.bbiZoom,
            this.bbiZoomIn,
            this.bbiShowFirstPage,
            this.bbiShowPrevPage,
            this.bbiShowNextPage,
            this.bbiShowLastPage,
            this.bbiMultiplePages,
            this.bbiFillBackground,
            this.bbiWatermark,
            this.bbiExportFile,
            this.bbiSendFile,
            this.bbiClosePreview,
            this.miFile,
            this.miView,
            this.miBackground,
            this.miPageLayout,
            this.miPageLayoutFacing,
            this.miPageLayoutContinuous,
            this.miToolbars,
            this.printPreviewBarCheckItem1,
            this.printPreviewBarCheckItem2,
            this.printPreviewBarCheckItem3,
            this.printPreviewBarCheckItem4,
            this.printPreviewBarCheckItem5,
            this.printPreviewBarCheckItem6,
            this.printPreviewBarCheckItem7,
            this.printPreviewBarCheckItem8,
            this.printPreviewBarCheckItem9,
            this.printPreviewBarCheckItem10,
            this.printPreviewBarCheckItem11,
            this.printPreviewBarCheckItem12,
            this.printPreviewBarCheckItem13,
            this.printPreviewBarCheckItem14,
            this.printPreviewBarCheckItem15,
            this.printPreviewBarCheckItem16,
            this.printPreviewBarCheckItem17,
            this.printPreviewBarCheckItem18,
            this.printPreviewBarCheckItem19});
            this.documentViewerBarManager1.MaxItemId = 60;
            this.documentViewerBarManager1.PreviewBar = null;
            this.documentViewerBarManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1,
            this.printPreviewRepositoryItemComboBox1});
            this.documentViewerBarManager1.StatusBar = this.previewBar2;
            this.documentViewerBarManager1.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // previewBar2
            // 
            this.previewBar2.BarName = "Status Bar";
            this.previewBar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.previewBar2.DockCol = 0;
            this.previewBar2.DockRow = 0;
            this.previewBar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.previewBar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.verticalSpaceItem, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.progressBarEditItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewStaticItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.zoomTrackBarEditItem1)});
            this.previewBar2.OptionsBar.AllowQuickCustomization = false;
            this.previewBar2.OptionsBar.DrawDragBorder = false;
            this.previewBar2.OptionsBar.UseWholeRow = true;
            this.previewBar2.Text = "Status Bar";
            // 
            // printPreviewStaticItem1
            // 
            this.printPreviewStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.printPreviewStaticItem1.Caption = "Nothing";
            this.printPreviewStaticItem1.Id = 0;
            this.printPreviewStaticItem1.LeftIndent = 1;
            this.printPreviewStaticItem1.Name = "printPreviewStaticItem1";
            this.printPreviewStaticItem1.RightIndent = 1;
            this.printPreviewStaticItem1.Type = "PageOfPages";
            // 
            // verticalSpaceItem
            // 
            this.verticalSpaceItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.verticalSpaceItem.Enabled = false;
            this.verticalSpaceItem.Id = 1;
            this.verticalSpaceItem.Name = "verticalSpaceItem";
            this.verticalSpaceItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInRuntime;
            // 
            // progressBarEditItem1
            // 
            this.progressBarEditItem1.Edit = this.repositoryItemProgressBar1;
            this.progressBarEditItem1.EditHeight = 12;
            this.progressBarEditItem1.EditWidth = 150;
            this.progressBarEditItem1.Id = 2;
            this.progressBarEditItem1.Name = "progressBarEditItem1";
            this.progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // printPreviewBarItem1
            // 
            this.printPreviewBarItem1.Caption = "Stop";
            this.printPreviewBarItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.StopPageBuilding;
            this.printPreviewBarItem1.Enabled = false;
            this.printPreviewBarItem1.Hint = "Stop";
            this.printPreviewBarItem1.Id = 3;
            this.printPreviewBarItem1.Name = "printPreviewBarItem1";
            this.printPreviewBarItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // printPreviewStaticItem2
            // 
            this.printPreviewStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.printPreviewStaticItem2.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.printPreviewStaticItem2.Caption = "100%";
            this.printPreviewStaticItem2.Id = 4;
            this.printPreviewStaticItem2.Name = "printPreviewStaticItem2";
            this.printPreviewStaticItem2.TextAlignment = System.Drawing.StringAlignment.Far;
            this.printPreviewStaticItem2.Type = "ZoomFactor";
            // 
            // zoomTrackBarEditItem1
            // 
            this.zoomTrackBarEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.zoomTrackBarEditItem1.Edit = this.repositoryItemZoomTrackBar1;
            this.zoomTrackBarEditItem1.EditValue = 90;
            this.zoomTrackBarEditItem1.EditWidth = 140;
            this.zoomTrackBarEditItem1.Enabled = false;
            this.zoomTrackBarEditItem1.Id = 5;
            this.zoomTrackBarEditItem1.Name = "zoomTrackBarEditItem1";
            this.zoomTrackBarEditItem1.Range = new int[] {
        10,
        500};
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemZoomTrackBar1.AllowFocused = false;
            this.repositoryItemZoomTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar1.Maximum = 180;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.documentViewerBarManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1062, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 644);
            this.barDockControlBottom.Manager = this.documentViewerBarManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1062, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.documentViewerBarManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 644);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1062, 0);
            this.barDockControlRight.Manager = this.documentViewerBarManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 644);
            // 
            // bbiDocumentMap
            // 
            this.bbiDocumentMap.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiDocumentMap.Caption = "Document Map";
            this.bbiDocumentMap.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap;
            this.bbiDocumentMap.Enabled = false;
            this.bbiDocumentMap.Hint = "Document Map";
            this.bbiDocumentMap.Id = 6;
            this.bbiDocumentMap.Name = "bbiDocumentMap";
            // 
            // bbiParameters
            // 
            this.bbiParameters.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiParameters.Caption = "Parameters";
            this.bbiParameters.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Parameters;
            this.bbiParameters.Enabled = false;
            this.bbiParameters.Hint = "Parameters";
            this.bbiParameters.Id = 7;
            this.bbiParameters.Name = "bbiParameters";
            // 
            // bbiThumbnails
            // 
            this.bbiThumbnails.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiThumbnails.Caption = "Thumbnails";
            this.bbiThumbnails.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Thumbnails;
            this.bbiThumbnails.Enabled = false;
            this.bbiThumbnails.Hint = "Thumbnails";
            this.bbiThumbnails.Id = 8;
            this.bbiThumbnails.Name = "bbiThumbnails";
            // 
            // bbiFind
            // 
            this.bbiFind.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiFind.Caption = "Search";
            this.bbiFind.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find;
            this.bbiFind.Enabled = false;
            this.bbiFind.Hint = "Search";
            this.bbiFind.Id = 9;
            this.bbiFind.Name = "bbiFind";
            // 
            // bbiHighlightEditingFields
            // 
            this.bbiHighlightEditingFields.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiHighlightEditingFields.Caption = "Editing Fields";
            this.bbiHighlightEditingFields.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HighlightEditingFields;
            this.bbiHighlightEditingFields.Enabled = false;
            this.bbiHighlightEditingFields.Hint = "Highlight Editing Fields";
            this.bbiHighlightEditingFields.Id = 10;
            this.bbiHighlightEditingFields.Name = "bbiHighlightEditingFields";
            // 
            // bbiCustomize
            // 
            this.bbiCustomize.Caption = "Customize";
            this.bbiCustomize.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize;
            this.bbiCustomize.Enabled = false;
            this.bbiCustomize.Hint = "Customize";
            this.bbiCustomize.Id = 11;
            this.bbiCustomize.Name = "bbiCustomize";
            // 
            // bbiOpen
            // 
            this.bbiOpen.Caption = "Open";
            this.bbiOpen.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Open;
            this.bbiOpen.Enabled = false;
            this.bbiOpen.Hint = "Open a document";
            this.bbiOpen.Id = 12;
            this.bbiOpen.Name = "bbiOpen";
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Save";
            this.bbiSave.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Save;
            this.bbiSave.Enabled = false;
            this.bbiSave.Hint = "Save the document";
            this.bbiSave.Id = 13;
            this.bbiSave.Name = "bbiSave";
            // 
            // bbiPrint
            // 
            this.bbiPrint.Caption = "&Print...";
            this.bbiPrint.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print;
            this.bbiPrint.Enabled = false;
            this.bbiPrint.Hint = "Print";
            this.bbiPrint.Id = 14;
            this.bbiPrint.Name = "bbiPrint";
            // 
            // bbiPrintDirect
            // 
            this.bbiPrintDirect.Caption = "P&rint";
            this.bbiPrintDirect.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect;
            this.bbiPrintDirect.Enabled = false;
            this.bbiPrintDirect.Hint = "Quick Print";
            this.bbiPrintDirect.Id = 15;
            this.bbiPrintDirect.Name = "bbiPrintDirect";
            // 
            // bbiPageSetup
            // 
            this.bbiPageSetup.Caption = "Page Set&up...";
            this.bbiPageSetup.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.bbiPageSetup.Enabled = false;
            this.bbiPageSetup.Hint = "Page Setup";
            this.bbiPageSetup.Id = 16;
            this.bbiPageSetup.Name = "bbiPageSetup";
            // 
            // bbiEditPageHF
            // 
            this.bbiEditPageHF.Caption = "Header And Footer";
            this.bbiEditPageHF.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF;
            this.bbiEditPageHF.Enabled = false;
            this.bbiEditPageHF.Hint = "Header And Footer";
            this.bbiEditPageHF.Id = 17;
            this.bbiEditPageHF.Name = "bbiEditPageHF";
            // 
            // bbiScale
            // 
            this.bbiScale.ActAsDropDown = true;
            this.bbiScale.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiScale.Caption = "Scale";
            this.bbiScale.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.bbiScale.Enabled = false;
            this.bbiScale.Hint = "Scale";
            this.bbiScale.Id = 18;
            this.bbiScale.Name = "bbiScale";
            // 
            // bbiHandTool
            // 
            this.bbiHandTool.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiHandTool.Caption = "Hand Tool";
            this.bbiHandTool.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.bbiHandTool.Enabled = false;
            this.bbiHandTool.Hint = "Hand Tool";
            this.bbiHandTool.Id = 19;
            this.bbiHandTool.Name = "bbiHandTool";
            // 
            // bbiMagnifier
            // 
            this.bbiMagnifier.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiMagnifier.Caption = "Magnifier";
            this.bbiMagnifier.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.bbiMagnifier.Enabled = false;
            this.bbiMagnifier.Hint = "Magnifier";
            this.bbiMagnifier.Id = 20;
            this.bbiMagnifier.Name = "bbiMagnifier";
            // 
            // bbiZoomOut
            // 
            this.bbiZoomOut.Caption = "Zoom Out";
            this.bbiZoomOut.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.bbiZoomOut.Enabled = false;
            this.bbiZoomOut.Hint = "Zoom Out";
            this.bbiZoomOut.Id = 21;
            this.bbiZoomOut.Name = "bbiZoomOut";
            // 
            // bbiZoom
            // 
            this.bbiZoom.Caption = "Zoom";
            this.bbiZoom.Edit = this.printPreviewRepositoryItemComboBox1;
            this.bbiZoom.EditValue = "100%";
            this.bbiZoom.EditWidth = 70;
            this.bbiZoom.Enabled = false;
            this.bbiZoom.Hint = "Zoom";
            this.bbiZoom.Id = 22;
            this.bbiZoom.Name = "bbiZoom";
            // 
            // printPreviewRepositoryItemComboBox1
            // 
            this.printPreviewRepositoryItemComboBox1.AutoComplete = false;
            this.printPreviewRepositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.printPreviewRepositoryItemComboBox1.DropDownRows = 11;
            this.printPreviewRepositoryItemComboBox1.Name = "printPreviewRepositoryItemComboBox1";
            // 
            // bbiZoomIn
            // 
            this.bbiZoomIn.Caption = "Zoom In";
            this.bbiZoomIn.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.bbiZoomIn.Enabled = false;
            this.bbiZoomIn.Hint = "Zoom In";
            this.bbiZoomIn.Id = 23;
            this.bbiZoomIn.Name = "bbiZoomIn";
            // 
            // bbiShowFirstPage
            // 
            this.bbiShowFirstPage.Caption = "First Page";
            this.bbiShowFirstPage.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage;
            this.bbiShowFirstPage.Enabled = false;
            this.bbiShowFirstPage.Hint = "First Page";
            this.bbiShowFirstPage.Id = 24;
            this.bbiShowFirstPage.Name = "bbiShowFirstPage";
            // 
            // bbiShowPrevPage
            // 
            this.bbiShowPrevPage.Caption = "Previous Page";
            this.bbiShowPrevPage.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage;
            this.bbiShowPrevPage.Enabled = false;
            this.bbiShowPrevPage.Hint = "Previous Page";
            this.bbiShowPrevPage.Id = 25;
            this.bbiShowPrevPage.Name = "bbiShowPrevPage";
            // 
            // bbiShowNextPage
            // 
            this.bbiShowNextPage.Caption = "Next Page";
            this.bbiShowNextPage.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage;
            this.bbiShowNextPage.Enabled = false;
            this.bbiShowNextPage.Hint = "Next Page";
            this.bbiShowNextPage.Id = 26;
            this.bbiShowNextPage.Name = "bbiShowNextPage";
            // 
            // bbiShowLastPage
            // 
            this.bbiShowLastPage.Caption = "Last Page";
            this.bbiShowLastPage.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage;
            this.bbiShowLastPage.Enabled = false;
            this.bbiShowLastPage.Hint = "Last Page";
            this.bbiShowLastPage.Id = 27;
            this.bbiShowLastPage.Name = "bbiShowLastPage";
            // 
            // bbiMultiplePages
            // 
            this.bbiMultiplePages.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiMultiplePages.Caption = "Multiple Pages";
            this.bbiMultiplePages.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.bbiMultiplePages.Enabled = false;
            this.bbiMultiplePages.Hint = "Multiple Pages";
            this.bbiMultiplePages.Id = 28;
            this.bbiMultiplePages.Name = "bbiMultiplePages";
            // 
            // bbiFillBackground
            // 
            this.bbiFillBackground.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiFillBackground.Caption = "&Color...";
            this.bbiFillBackground.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.bbiFillBackground.Enabled = false;
            this.bbiFillBackground.Hint = "Background";
            this.bbiFillBackground.Id = 29;
            this.bbiFillBackground.Name = "bbiFillBackground";
            // 
            // bbiWatermark
            // 
            this.bbiWatermark.Caption = "&Watermark...";
            this.bbiWatermark.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.bbiWatermark.Enabled = false;
            this.bbiWatermark.Hint = "Watermark";
            this.bbiWatermark.Id = 30;
            this.bbiWatermark.Name = "bbiWatermark";
            // 
            // bbiExportFile
            // 
            this.bbiExportFile.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiExportFile.Caption = "Export Document...";
            this.bbiExportFile.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.bbiExportFile.Enabled = false;
            this.bbiExportFile.Hint = "Export Document...";
            this.bbiExportFile.Id = 31;
            this.bbiExportFile.Name = "bbiExportFile";
            // 
            // bbiSendFile
            // 
            this.bbiSendFile.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiSendFile.Caption = "Send via E-Mail...";
            this.bbiSendFile.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.bbiSendFile.Enabled = false;
            this.bbiSendFile.Hint = "Send via E-Mail...";
            this.bbiSendFile.Id = 32;
            this.bbiSendFile.Name = "bbiSendFile";
            // 
            // bbiClosePreview
            // 
            this.bbiClosePreview.Caption = "E&xit";
            this.bbiClosePreview.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.bbiClosePreview.Enabled = false;
            this.bbiClosePreview.Hint = "Close Preview";
            this.bbiClosePreview.Id = 33;
            this.bbiClosePreview.Name = "bbiClosePreview";
            // 
            // miFile
            // 
            this.miFile.Caption = "&File";
            this.miFile.Command = DevExpress.XtraPrinting.PrintingSystemCommand.File;
            this.miFile.Id = 34;
            this.miFile.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPageSetup),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrintDirect),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportFile, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendFile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClosePreview, true)});
            this.miFile.Name = "miFile";
            // 
            // miView
            // 
            this.miView.Caption = "&View";
            this.miView.Command = DevExpress.XtraPrinting.PrintingSystemCommand.View;
            this.miView.Id = 35;
            this.miView.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.miPageLayout, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.miToolbars, true)});
            this.miView.Name = "miView";
            // 
            // miPageLayout
            // 
            this.miPageLayout.Caption = "&Page Layout";
            this.miPageLayout.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayout;
            this.miPageLayout.Id = 37;
            this.miPageLayout.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.miPageLayoutFacing),
            new DevExpress.XtraBars.LinkPersistInfo(this.miPageLayoutContinuous)});
            this.miPageLayout.Name = "miPageLayout";
            // 
            // miPageLayoutFacing
            // 
            this.miPageLayoutFacing.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.miPageLayoutFacing.Caption = "&Facing";
            this.miPageLayoutFacing.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutFacing;
            this.miPageLayoutFacing.Enabled = false;
            this.miPageLayoutFacing.GroupIndex = 100;
            this.miPageLayoutFacing.Id = 38;
            this.miPageLayoutFacing.Name = "miPageLayoutFacing";
            // 
            // miPageLayoutContinuous
            // 
            this.miPageLayoutContinuous.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.miPageLayoutContinuous.Caption = "&Continuous";
            this.miPageLayoutContinuous.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutContinuous;
            this.miPageLayoutContinuous.Enabled = false;
            this.miPageLayoutContinuous.GroupIndex = 100;
            this.miPageLayoutContinuous.Id = 39;
            this.miPageLayoutContinuous.Name = "miPageLayoutContinuous";
            // 
            // miToolbars
            // 
            this.miToolbars.Caption = "Bars";
            this.miToolbars.Id = 40;
            this.miToolbars.Name = "miToolbars";
            // 
            // miBackground
            // 
            this.miBackground.Caption = "&Background";
            this.miBackground.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Background;
            this.miBackground.Id = 36;
            this.miBackground.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFillBackground),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiWatermark)});
            this.miBackground.Name = "miBackground";
            // 
            // printPreviewBarCheckItem1
            // 
            this.printPreviewBarCheckItem1.BindableChecked = true;
            this.printPreviewBarCheckItem1.Caption = "PDF File";
            this.printPreviewBarCheckItem1.Checked = true;
            this.printPreviewBarCheckItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.printPreviewBarCheckItem1.Enabled = false;
            this.printPreviewBarCheckItem1.GroupIndex = 2;
            this.printPreviewBarCheckItem1.Hint = "PDF File";
            this.printPreviewBarCheckItem1.Id = 41;
            this.printPreviewBarCheckItem1.Name = "printPreviewBarCheckItem1";
            // 
            // printPreviewBarCheckItem2
            // 
            this.printPreviewBarCheckItem2.Caption = "HTML File";
            this.printPreviewBarCheckItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.printPreviewBarCheckItem2.Enabled = false;
            this.printPreviewBarCheckItem2.GroupIndex = 2;
            this.printPreviewBarCheckItem2.Hint = "HTML File";
            this.printPreviewBarCheckItem2.Id = 42;
            this.printPreviewBarCheckItem2.Name = "printPreviewBarCheckItem2";
            // 
            // printPreviewBarCheckItem3
            // 
            this.printPreviewBarCheckItem3.Caption = "MHT File";
            this.printPreviewBarCheckItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.printPreviewBarCheckItem3.Enabled = false;
            this.printPreviewBarCheckItem3.GroupIndex = 2;
            this.printPreviewBarCheckItem3.Hint = "MHT File";
            this.printPreviewBarCheckItem3.Id = 43;
            this.printPreviewBarCheckItem3.Name = "printPreviewBarCheckItem3";
            // 
            // printPreviewBarCheckItem4
            // 
            this.printPreviewBarCheckItem4.Caption = "RTF File";
            this.printPreviewBarCheckItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.printPreviewBarCheckItem4.Enabled = false;
            this.printPreviewBarCheckItem4.GroupIndex = 2;
            this.printPreviewBarCheckItem4.Hint = "RTF File";
            this.printPreviewBarCheckItem4.Id = 44;
            this.printPreviewBarCheckItem4.Name = "printPreviewBarCheckItem4";
            // 
            // printPreviewBarCheckItem5
            // 
            this.printPreviewBarCheckItem5.Caption = "DOCX File";
            this.printPreviewBarCheckItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportDocx;
            this.printPreviewBarCheckItem5.Enabled = false;
            this.printPreviewBarCheckItem5.GroupIndex = 2;
            this.printPreviewBarCheckItem5.Hint = "DOCX File";
            this.printPreviewBarCheckItem5.Id = 45;
            this.printPreviewBarCheckItem5.Name = "printPreviewBarCheckItem5";
            // 
            // printPreviewBarCheckItem6
            // 
            this.printPreviewBarCheckItem6.Caption = "XLS File";
            this.printPreviewBarCheckItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.printPreviewBarCheckItem6.Enabled = false;
            this.printPreviewBarCheckItem6.GroupIndex = 2;
            this.printPreviewBarCheckItem6.Hint = "XLS File";
            this.printPreviewBarCheckItem6.Id = 46;
            this.printPreviewBarCheckItem6.Name = "printPreviewBarCheckItem6";
            // 
            // printPreviewBarCheckItem7
            // 
            this.printPreviewBarCheckItem7.Caption = "XLSX File";
            this.printPreviewBarCheckItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx;
            this.printPreviewBarCheckItem7.Enabled = false;
            this.printPreviewBarCheckItem7.GroupIndex = 2;
            this.printPreviewBarCheckItem7.Hint = "XLSX File";
            this.printPreviewBarCheckItem7.Id = 47;
            this.printPreviewBarCheckItem7.Name = "printPreviewBarCheckItem7";
            // 
            // printPreviewBarCheckItem8
            // 
            this.printPreviewBarCheckItem8.Caption = "CSV File";
            this.printPreviewBarCheckItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.printPreviewBarCheckItem8.Enabled = false;
            this.printPreviewBarCheckItem8.GroupIndex = 2;
            this.printPreviewBarCheckItem8.Hint = "CSV File";
            this.printPreviewBarCheckItem8.Id = 48;
            this.printPreviewBarCheckItem8.Name = "printPreviewBarCheckItem8";
            // 
            // printPreviewBarCheckItem9
            // 
            this.printPreviewBarCheckItem9.Caption = "Text File";
            this.printPreviewBarCheckItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.printPreviewBarCheckItem9.Enabled = false;
            this.printPreviewBarCheckItem9.GroupIndex = 2;
            this.printPreviewBarCheckItem9.Hint = "Text File";
            this.printPreviewBarCheckItem9.Id = 49;
            this.printPreviewBarCheckItem9.Name = "printPreviewBarCheckItem9";
            // 
            // printPreviewBarCheckItem10
            // 
            this.printPreviewBarCheckItem10.Caption = "Image File";
            this.printPreviewBarCheckItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.printPreviewBarCheckItem10.Enabled = false;
            this.printPreviewBarCheckItem10.GroupIndex = 2;
            this.printPreviewBarCheckItem10.Hint = "Image File";
            this.printPreviewBarCheckItem10.Id = 50;
            this.printPreviewBarCheckItem10.Name = "printPreviewBarCheckItem10";
            // 
            // printPreviewBarCheckItem11
            // 
            this.printPreviewBarCheckItem11.BindableChecked = true;
            this.printPreviewBarCheckItem11.Caption = "PDF File";
            this.printPreviewBarCheckItem11.Checked = true;
            this.printPreviewBarCheckItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.printPreviewBarCheckItem11.Enabled = false;
            this.printPreviewBarCheckItem11.GroupIndex = 1;
            this.printPreviewBarCheckItem11.Hint = "PDF File";
            this.printPreviewBarCheckItem11.Id = 51;
            this.printPreviewBarCheckItem11.Name = "printPreviewBarCheckItem11";
            // 
            // printPreviewBarCheckItem12
            // 
            this.printPreviewBarCheckItem12.Caption = "MHT File";
            this.printPreviewBarCheckItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.printPreviewBarCheckItem12.Enabled = false;
            this.printPreviewBarCheckItem12.GroupIndex = 1;
            this.printPreviewBarCheckItem12.Hint = "MHT File";
            this.printPreviewBarCheckItem12.Id = 52;
            this.printPreviewBarCheckItem12.Name = "printPreviewBarCheckItem12";
            // 
            // printPreviewBarCheckItem13
            // 
            this.printPreviewBarCheckItem13.Caption = "RTF File";
            this.printPreviewBarCheckItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.printPreviewBarCheckItem13.Enabled = false;
            this.printPreviewBarCheckItem13.GroupIndex = 1;
            this.printPreviewBarCheckItem13.Hint = "RTF File";
            this.printPreviewBarCheckItem13.Id = 53;
            this.printPreviewBarCheckItem13.Name = "printPreviewBarCheckItem13";
            // 
            // printPreviewBarCheckItem14
            // 
            this.printPreviewBarCheckItem14.Caption = "DOCX File";
            this.printPreviewBarCheckItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendDocx;
            this.printPreviewBarCheckItem14.Enabled = false;
            this.printPreviewBarCheckItem14.GroupIndex = 1;
            this.printPreviewBarCheckItem14.Hint = "DOCX File";
            this.printPreviewBarCheckItem14.Id = 54;
            this.printPreviewBarCheckItem14.Name = "printPreviewBarCheckItem14";
            // 
            // printPreviewBarCheckItem15
            // 
            this.printPreviewBarCheckItem15.Caption = "XLS File";
            this.printPreviewBarCheckItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.printPreviewBarCheckItem15.Enabled = false;
            this.printPreviewBarCheckItem15.GroupIndex = 1;
            this.printPreviewBarCheckItem15.Hint = "XLS File";
            this.printPreviewBarCheckItem15.Id = 55;
            this.printPreviewBarCheckItem15.Name = "printPreviewBarCheckItem15";
            // 
            // printPreviewBarCheckItem16
            // 
            this.printPreviewBarCheckItem16.Caption = "XLSX File";
            this.printPreviewBarCheckItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx;
            this.printPreviewBarCheckItem16.Enabled = false;
            this.printPreviewBarCheckItem16.GroupIndex = 1;
            this.printPreviewBarCheckItem16.Hint = "XLSX File";
            this.printPreviewBarCheckItem16.Id = 56;
            this.printPreviewBarCheckItem16.Name = "printPreviewBarCheckItem16";
            // 
            // printPreviewBarCheckItem17
            // 
            this.printPreviewBarCheckItem17.Caption = "CSV File";
            this.printPreviewBarCheckItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.printPreviewBarCheckItem17.Enabled = false;
            this.printPreviewBarCheckItem17.GroupIndex = 1;
            this.printPreviewBarCheckItem17.Hint = "CSV File";
            this.printPreviewBarCheckItem17.Id = 57;
            this.printPreviewBarCheckItem17.Name = "printPreviewBarCheckItem17";
            // 
            // printPreviewBarCheckItem18
            // 
            this.printPreviewBarCheckItem18.Caption = "Text File";
            this.printPreviewBarCheckItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.printPreviewBarCheckItem18.Enabled = false;
            this.printPreviewBarCheckItem18.GroupIndex = 1;
            this.printPreviewBarCheckItem18.Hint = "Text File";
            this.printPreviewBarCheckItem18.Id = 58;
            this.printPreviewBarCheckItem18.Name = "printPreviewBarCheckItem18";
            // 
            // printPreviewBarCheckItem19
            // 
            this.printPreviewBarCheckItem19.Caption = "Image File";
            this.printPreviewBarCheckItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.printPreviewBarCheckItem19.Enabled = false;
            this.printPreviewBarCheckItem19.GroupIndex = 1;
            this.printPreviewBarCheckItem19.Hint = "Image File";
            this.printPreviewBarCheckItem19.Id = 59;
            this.printPreviewBarCheckItem19.Name = "printPreviewBarCheckItem19";
            // 
            // ReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 671);
            this.Controls.Add(this.documentViewer1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ReportViewer";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "ReportViewer";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentViewerRibbonController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentViewerBarManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printPreviewRepositoryItemComboBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem2;
        private DevExpress.XtraPrinting.Preview.DocumentViewerRibbonController documentViewerRibbonController1;
        private DevExpress.XtraPrinting.Preview.DocumentViewer documentViewer1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraPrinting.Preview.ProgressBarEditItem progressBarEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem53;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem4;
        private DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem zoomTrackBarEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem15;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem17;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem18;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem19;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem20;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem21;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem22;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem23;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem24;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem25;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem26;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem27;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem28;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem29;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem30;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem31;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem32;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem33;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem34;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem35;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem36;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem37;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem38;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem39;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem40;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem41;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem42;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem43;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem44;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem45;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem46;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem47;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem48;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem49;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem50;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem51;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem52;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage ribbonPage2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup8;
        private DevExpress.XtraPrinting.Preview.DocumentViewerBarManager documentViewerBarManager1;
        private DevExpress.XtraPrinting.Preview.PreviewBar previewBar2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem1;
        private DevExpress.XtraBars.BarStaticItem verticalSpaceItem;
        private DevExpress.XtraPrinting.Preview.ProgressBarEditItem progressBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem2;
        private DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem zoomTrackBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem miFile;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiPageSetup;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiPrint;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiPrintDirect;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiExportFile;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiSendFile;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiClosePreview;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem miView;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem miPageLayout;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem miPageLayoutFacing;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem miPageLayoutContinuous;
        private DevExpress.XtraBars.BarToolbarsListItem miToolbars;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem miBackground;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiFillBackground;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiWatermark;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiDocumentMap;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiParameters;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiThumbnails;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiFind;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiHighlightEditingFields;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiCustomize;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiOpen;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiSave;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiEditPageHF;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiScale;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiHandTool;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiMagnifier;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiZoomOut;
        private DevExpress.XtraPrinting.Preview.ZoomBarEditItem bbiZoom;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox printPreviewRepositoryItemComboBox1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiZoomIn;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiShowFirstPage;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiShowPrevPage;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiShowNextPage;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiShowLastPage;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem bbiMultiplePages;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem15;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem17;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem18;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem19;
    }
}