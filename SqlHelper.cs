﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAkademia_v1
{
    public class SqlHelper
    {
        SqlConnection connection;
        public SqlHelper (string Con)
        {
            connection = new SqlConnection(Con);
        }
        public bool IsConnected
        {
            get
            {
                if (connection.State == ConnectionState.Closed)
                {
                    try
                    {
                        connection.Open();
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}
