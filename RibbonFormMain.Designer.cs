﻿namespace RAkademia_v1
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::RAkademia_v1.SplashScreen1), true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.MainRibbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ButtonStatic = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonDynamic = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonReportList = new DevExpress.XtraBars.BarListItem();
            this.ButtonStaticOnline = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonDynamicOnline = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonDualOnline = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonDataBase = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonCreateReportList = new DevExpress.XtraBars.BarListItem();
            this.Ribbon1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.RibbonGroupWeighting = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.RibbonGroupReport = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonGroupData = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.defaultLookAndFeelManager = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.rAkademiaDynamiczneBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rAkademiaStatyczneBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridStatic = new DevExpress.XtraGrid.GridControl();
            this.GridViewStatic = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colImieNazwisko = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colseria = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnazwa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTara = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.LabelMedian = new DevExpress.XtraLayout.LayoutControlItem();
            this.TextMedian = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.GridOnline = new DevExpress.XtraGrid.GridControl();
            this.GridViewOnline = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colImieNazwisko5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colseria5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnazwa5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTara5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetto_mass = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TextChoiceBatch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Count = new DevExpress.XtraEditors.TextEdit();
            this.TextSum = new DevExpress.XtraEditors.TextEdit();
            this.TextAvg = new DevExpress.XtraEditors.TextEdit();
            this.TextVar = new DevExpress.XtraEditors.TextEdit();
            this.TextDev = new DevExpress.XtraEditors.TextEdit();
            this.TextMax = new DevExpress.XtraEditors.TextEdit();
            this.TextMin = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.GroupOnline = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.GridDualDynamic = new DevExpress.XtraGrid.GridControl();
            this.GridViewDualDynamic = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colImieNazwisko4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colseria4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnazwa4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasa4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTara4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridDualStatic = new DevExpress.XtraGrid.GridControl();
            this.GridViewDualStatic = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colImieNazwisko3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colseria3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnazwa3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTara3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasa3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TextDualDynamicMedian = new DevExpress.XtraEditors.TextEdit();
            this.TextDualDynamicStd = new DevExpress.XtraEditors.TextEdit();
            this.TextDualDynamicVar = new DevExpress.XtraEditors.TextEdit();
            this.TextDualDynamicAvg = new DevExpress.XtraEditors.TextEdit();
            this.TextDualDynamicMin = new DevExpress.XtraEditors.TextEdit();
            this.TextDualDynamicSum = new DevExpress.XtraEditors.TextEdit();
            this.TextDualDynamicCount = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticMedian = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticVar = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticMax = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticSum = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticStd = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticMin = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticAvg = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticCount = new DevExpress.XtraEditors.TextEdit();
            this.ComboDualBatch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TextDualDynamicMax = new DevExpress.XtraEditors.TextEdit();
            this.TextDualStaticPrecent = new DevExpress.XtraEditors.TextEdit();
            this.TextDualDynamicPrecent = new DevExpress.XtraEditors.TextEdit();
            this.DualControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.GroupStatic = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LabelDualStaticMedian = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticStd = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticVar = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticAvg = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticMax = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticMin = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticSum = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticCount1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.GroupDynamic = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LabelDualDynamicCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualDynamicSum = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualDynamicMin = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualDynamicMax = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualDynamicAvg = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualDynamicVar = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualDynamicStd = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualDynamicMedian = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelDualStaticCount2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.GridDynamic = new DevExpress.XtraGrid.GridControl();
            this.GridViewDynamic = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colImieNazwisko2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colseria2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnazwa2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasa1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTara2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dataLayoutControl2 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.TextUserPassword = new DevExpress.XtraEditors.TextEdit();
            this.TextUserName = new DevExpress.XtraEditors.TextEdit();
            this.ComboSerwer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ButtonTest = new DevExpress.XtraEditors.SimpleButton();
            this.ComboDataBase = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LabelUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelUserPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelSerwer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.TabbedMainGroup = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutDataBaseSettings = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Static = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.WeighingOnline = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Dynamic = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.WeightingDual = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelVar = new DevExpress.XtraLayout.LayoutControlItem();
            this.ChoiceBatch = new DevExpress.XtraLayout.LayoutControlItem();
            this.colImieNazwisko1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colseria1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnazwa1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasa2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTara1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainRibbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAkademiaDynamiczneBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAkademiaStatyczneBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStatic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewStatic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelMedian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMedian.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridOnline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewOnline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextChoiceBatch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Count.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextAvg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextVar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDev.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupOnline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDualDynamic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDualDynamic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDualStatic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDualStatic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicMedian.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicStd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicVar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicAvg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicSum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticMedian.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticVar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticSum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticStd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticAvg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboDualBatch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticPrecent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicPrecent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DualControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupStatic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticMedian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticStd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticVar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticAvg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticSum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupDynamic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicSum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicAvg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicVar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicStd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicMedian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticCount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDynamic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDynamic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).BeginInit();
            this.dataLayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextUserPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboSerwer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboDataBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelUserPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelSerwer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedMainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDataBaseSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Static)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeighingOnline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dynamic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeightingDual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelVar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChoiceBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            this.SuspendLayout();
            // 
            // splashScreenManager1
            // 
            splashScreenManager1.ClosingDelay = 1000;
            // 
            // MainRibbon
            // 
            this.MainRibbon.AutoHideEmptyItems = true;
            this.MainRibbon.ExpandCollapseItem.AllowDrawArrow = false;
            this.MainRibbon.ExpandCollapseItem.AllowDrawArrowInMenu = false;
            this.MainRibbon.ExpandCollapseItem.Id = 0;
            this.MainRibbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.MainRibbon.ExpandCollapseItem,
            this.ButtonStatic,
            this.ButtonDynamic,
            this.ButtonUpdate,
            this.ButtonReportList,
            this.ButtonStaticOnline,
            this.ButtonDynamicOnline,
            this.ButtonDualOnline,
            this.ButtonDataBase,
            this.ButtonCreateReportList});
            this.MainRibbon.Location = new System.Drawing.Point(0, 0);
            this.MainRibbon.MaxItemId = 158;
            this.MainRibbon.Name = "MainRibbon";
            this.MainRibbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.Ribbon1});
            this.MainRibbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.MainRibbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.MainRibbon.ShowQatLocationSelector = false;
            this.MainRibbon.ShowToolbarCustomizeItem = false;
            this.MainRibbon.Size = new System.Drawing.Size(1160, 144);
            this.MainRibbon.Toolbar.ShowCustomizeItem = false;
            this.MainRibbon.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // ButtonStatic
            // 
            this.ButtonStatic.Caption = "Statyczne";
            this.ButtonStatic.Id = 1;
            this.ButtonStatic.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonStatic.ImageOptions.Image")));
            this.ButtonStatic.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonStatic.ImageOptions.LargeImage")));
            this.ButtonStatic.Name = "ButtonStatic";
            this.ButtonStatic.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.Static_itemClick);
            // 
            // ButtonDynamic
            // 
            this.ButtonDynamic.Caption = "Dynamiczne";
            this.ButtonDynamic.Id = 2;
            this.ButtonDynamic.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDynamic.ImageOptions.Image")));
            this.ButtonDynamic.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonDynamic.ImageOptions.LargeImage")));
            this.ButtonDynamic.Name = "ButtonDynamic";
            this.ButtonDynamic.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.Dynamic_itemClick);
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Caption = "Aktualizuj Dane";
            this.ButtonUpdate.Id = 1;
            this.ButtonUpdate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonUpdate.ImageOptions.Image")));
            this.ButtonUpdate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonUpdate.ImageOptions.LargeImage")));
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.Update_itemClick);
            // 
            // ButtonReportList
            // 
            this.ButtonReportList.Caption = "Raporty";
            this.ButtonReportList.Id = 152;
            this.ButtonReportList.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonReportList.ImageOptions.Image")));
            this.ButtonReportList.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonReportList.ImageOptions.LargeImage")));
            this.ButtonReportList.Name = "ButtonReportList";
            this.ButtonReportList.ListItemClick += new DevExpress.XtraBars.ListItemClickEventHandler(this.ListReports_ItemClick);
            // 
            // ButtonStaticOnline
            // 
            this.ButtonStaticOnline.Caption = "Statyczne Online";
            this.ButtonStaticOnline.Id = 153;
            this.ButtonStaticOnline.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonStaticOnline.ImageOptions.Image")));
            this.ButtonStaticOnline.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonStaticOnline.ImageOptions.LargeImage")));
            this.ButtonStaticOnline.Name = "ButtonStaticOnline";
            this.ButtonStaticOnline.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.StaticOnline_itemClick);
            // 
            // ButtonDynamicOnline
            // 
            this.ButtonDynamicOnline.Caption = "Dynamiczne Online";
            this.ButtonDynamicOnline.Id = 154;
            this.ButtonDynamicOnline.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDynamicOnline.ImageOptions.Image")));
            this.ButtonDynamicOnline.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonDynamicOnline.ImageOptions.LargeImage")));
            this.ButtonDynamicOnline.Name = "ButtonDynamicOnline";
            this.ButtonDynamicOnline.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.DynamicOnline_itemClick);
            // 
            // ButtonDualOnline
            // 
            this.ButtonDualOnline.Caption = "Porównawcze";
            this.ButtonDualOnline.Id = 155;
            this.ButtonDualOnline.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDualOnline.ImageOptions.Image")));
            this.ButtonDualOnline.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonDualOnline.ImageOptions.LargeImage")));
            this.ButtonDualOnline.Name = "ButtonDualOnline";
            this.ButtonDualOnline.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.DualButton_ItemClick);
            // 
            // ButtonDataBase
            // 
            this.ButtonDataBase.Caption = "Ustawienie Bazy Danych";
            this.ButtonDataBase.Id = 156;
            this.ButtonDataBase.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDataBase.ImageOptions.Image")));
            this.ButtonDataBase.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonDataBase.ImageOptions.LargeImage")));
            this.ButtonDataBase.Name = "ButtonDataBase";
            this.ButtonDataBase.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ButtonDataBase_ItemClick);
            // 
            // ButtonCreateReportList
            // 
            this.ButtonCreateReportList.Caption = "Kreator Raportów";
            this.ButtonCreateReportList.Id = 157;
            this.ButtonCreateReportList.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ButtonCreateReportList.ImageOptions.Image")));
            this.ButtonCreateReportList.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ButtonCreateReportList.ImageOptions.LargeImage")));
            this.ButtonCreateReportList.Name = "ButtonCreateReportList";
            this.ButtonCreateReportList.ListItemClick += new DevExpress.XtraBars.ListItemClickEventHandler(this.ButtonCreateReportList_ListItemClick);
            // 
            // Ribbon1
            // 
            this.Ribbon1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.RibbonGroupWeighting,
            this.RibbonGroupReport,
            this.ribbonGroupData});
            this.Ribbon1.Name = "Ribbon1";
            this.Ribbon1.Text = "ważenia";
            // 
            // RibbonGroupWeighting
            // 
            this.RibbonGroupWeighting.ItemLinks.Add(this.ButtonStatic);
            this.RibbonGroupWeighting.ItemLinks.Add(this.ButtonDynamic);
            this.RibbonGroupWeighting.ItemLinks.Add(this.ButtonStaticOnline);
            this.RibbonGroupWeighting.ItemLinks.Add(this.ButtonDynamicOnline);
            this.RibbonGroupWeighting.ItemLinks.Add(this.ButtonDualOnline);
            this.RibbonGroupWeighting.Name = "RibbonGroupWeighting";
            this.RibbonGroupWeighting.Text = "Ważenia";
            // 
            // RibbonGroupReport
            // 
            this.RibbonGroupReport.ItemLinks.Add(this.ButtonReportList);
            this.RibbonGroupReport.ItemLinks.Add(this.ButtonCreateReportList);
            this.RibbonGroupReport.Name = "RibbonGroupReport";
            this.RibbonGroupReport.Text = "Raportowanie";
            // 
            // ribbonGroupData
            // 
            this.ribbonGroupData.ItemLinks.Add(this.ButtonUpdate, true);
            this.ribbonGroupData.ItemLinks.Add(this.ButtonDataBase);
            this.ribbonGroupData.Name = "ribbonGroupData";
            this.ribbonGroupData.Text = "Inne";
            // 
            // defaultLookAndFeelManager
            // 
            this.defaultLookAndFeelManager.LookAndFeel.SkinName = "Office 2010 Blue";
            // 
            // rAkademiaDynamiczneBindingSource
            // 
            this.rAkademiaDynamiczneBindingSource.DataSource = typeof(RAkademia_v1.Model.RAkademiaDynamiczne);
            // 
            // rAkademiaStatyczneBindingSource
            // 
            this.rAkademiaStatyczneBindingSource.DataSource = typeof(RAkademia_v1.Model.RAkademiaStatyczne);
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // GridStatic
            // 
            this.GridStatic.DataSource = this.rAkademiaDynamiczneBindingSource;
            this.GridStatic.Location = new System.Drawing.Point(24, 24);
            this.GridStatic.MainView = this.GridViewStatic;
            this.GridStatic.MenuManager = this.MainRibbon;
            this.GridStatic.Name = "GridStatic";
            this.GridStatic.Size = new System.Drawing.Size(1112, 331);
            this.GridStatic.TabIndex = 4;
            this.GridStatic.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewStatic,
            this.gridView2});
            // 
            // GridViewStatic
            // 
            this.GridViewStatic.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colImieNazwisko,
            this.colid,
            this.colseria,
            this.colnazwa,
            this.colMasa,
            this.colTara,
            this.colDate});
            this.GridViewStatic.GridControl = this.GridStatic;
            this.GridViewStatic.GroupPanelText = " Przeciągnij i upuść kolumnę aby grupować";
            this.GridViewStatic.Name = "GridViewStatic";
            this.GridViewStatic.OptionsBehavior.Editable = false;
            this.GridViewStatic.OptionsMenu.ShowAutoFilterRowItem = false;
            this.GridViewStatic.OptionsView.ShowAutoFilterRow = true;
            // 
            // colImieNazwisko
            // 
            this.colImieNazwisko.FieldName = "ImieNazwisko";
            this.colImieNazwisko.Name = "colImieNazwisko";
            this.colImieNazwisko.Visible = true;
            this.colImieNazwisko.VisibleIndex = 1;
            // 
            // colid
            // 
            this.colid.Caption = "Id";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colseria
            // 
            this.colseria.Caption = "Seria";
            this.colseria.FieldName = "seria";
            this.colseria.Name = "colseria";
            this.colseria.Visible = true;
            this.colseria.VisibleIndex = 2;
            // 
            // colnazwa
            // 
            this.colnazwa.Caption = "Nazwa";
            this.colnazwa.FieldName = "nazwa";
            this.colnazwa.Name = "colnazwa";
            this.colnazwa.Visible = true;
            this.colnazwa.VisibleIndex = 3;
            // 
            // colMasa
            // 
            this.colMasa.FieldName = "Masa";
            this.colMasa.Name = "colMasa";
            this.colMasa.Visible = true;
            this.colMasa.VisibleIndex = 6;
            // 
            // colTara
            // 
            this.colTara.FieldName = "Tara";
            this.colTara.Name = "colTara";
            this.colTara.Visible = true;
            this.colTara.VisibleIndex = 4;
            // 
            // colDate
            // 
            this.colDate.Caption = "Data";
            this.colDate.FieldName = "Date";
            this.colDate.Name = "colDate";
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 5;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.GridStatic;
            this.gridView2.Name = "gridView2";
            // 
            // LabelMedian
            // 
            this.LabelMedian.Location = new System.Drawing.Point(973, 24);
            this.LabelMedian.Name = "LabelMedian";
            this.LabelMedian.Size = new System.Drawing.Size(139, 24);
            this.LabelMedian.Text = "Mediana:";
            this.LabelMedian.TextSize = new System.Drawing.Size(82, 13);
            // 
            // TextMedian
            // 
            this.TextMedian.Location = new System.Drawing.Point(958, 83);
            this.TextMedian.MenuManager = this.MainRibbon;
            this.TextMedian.Name = "TextMedian";
            this.TextMedian.Size = new System.Drawing.Size(130, 20);
            this.TextMedian.StyleController = this.layoutControl2;
            this.TextMedian.TabIndex = 11;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.GridOnline);
            this.layoutControl2.Controls.Add(this.TextChoiceBatch);
            this.layoutControl2.Controls.Add(this.Count);
            this.layoutControl2.Controls.Add(this.TextSum);
            this.layoutControl2.Controls.Add(this.TextMedian);
            this.layoutControl2.Controls.Add(this.TextAvg);
            this.layoutControl2.Controls.Add(this.TextVar);
            this.layoutControl2.Controls.Add(this.TextDev);
            this.layoutControl2.Controls.Add(this.TextMax);
            this.layoutControl2.Controls.Add(this.TextMin);
            this.layoutControl2.Location = new System.Drawing.Point(24, 24);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(1112, 331);
            this.layoutControl2.TabIndex = 20;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // GridOnline
            // 
            this.GridOnline.DataSource = this.rAkademiaDynamiczneBindingSource;
            this.GridOnline.Location = new System.Drawing.Point(12, 119);
            this.GridOnline.MainView = this.GridViewOnline;
            this.GridOnline.MenuManager = this.MainRibbon;
            this.GridOnline.Name = "GridOnline";
            this.GridOnline.Size = new System.Drawing.Size(1088, 200);
            this.GridOnline.TabIndex = 16;
            this.GridOnline.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewOnline,
            this.gridView6});
            // 
            // GridViewOnline
            // 
            this.GridViewOnline.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colImieNazwisko5,
            this.colid5,
            this.colseria5,
            this.colnazwa5,
            this.colTara5,
            this.colDate5,
            this.colNetto_mass});
            this.GridViewOnline.GridControl = this.GridOnline;
            this.GridViewOnline.GroupPanelText = " Przeciągnij i upuść kolumnę aby grupować";
            this.GridViewOnline.Name = "GridViewOnline";
            this.GridViewOnline.OptionsBehavior.Editable = false;
            this.GridViewOnline.OptionsView.ShowAutoFilterRow = true;
            // 
            // colImieNazwisko5
            // 
            this.colImieNazwisko5.FieldName = "ImieNazwisko";
            this.colImieNazwisko5.Name = "colImieNazwisko5";
            this.colImieNazwisko5.Visible = true;
            this.colImieNazwisko5.VisibleIndex = 0;
            // 
            // colid5
            // 
            this.colid5.Caption = "Id";
            this.colid5.FieldName = "id";
            this.colid5.Name = "colid5";
            this.colid5.Visible = true;
            this.colid5.VisibleIndex = 1;
            // 
            // colseria5
            // 
            this.colseria5.Caption = "Seria";
            this.colseria5.FieldName = "seria";
            this.colseria5.Name = "colseria5";
            this.colseria5.Visible = true;
            this.colseria5.VisibleIndex = 2;
            // 
            // colnazwa5
            // 
            this.colnazwa5.Caption = "Nazwa";
            this.colnazwa5.FieldName = "nazwa";
            this.colnazwa5.Name = "colnazwa5";
            this.colnazwa5.Visible = true;
            this.colnazwa5.VisibleIndex = 3;
            // 
            // colTara5
            // 
            this.colTara5.FieldName = "Tara";
            this.colTara5.Name = "colTara5";
            this.colTara5.Visible = true;
            this.colTara5.VisibleIndex = 4;
            // 
            // colDate5
            // 
            this.colDate5.Caption = "Data";
            this.colDate5.FieldName = "Date";
            this.colDate5.Name = "colDate5";
            this.colDate5.Visible = true;
            this.colDate5.VisibleIndex = 5;
            // 
            // colNetto_mass
            // 
            this.colNetto_mass.Caption = "Masa";
            this.colNetto_mass.FieldName = "Masa";
            this.colNetto_mass.Name = "colNetto_mass";
            this.colNetto_mass.Visible = true;
            this.colNetto_mass.VisibleIndex = 6;
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.GridOnline;
            this.gridView6.Name = "gridView6";
            // 
            // TextChoiceBatch
            // 
            this.TextChoiceBatch.Location = new System.Drawing.Point(71, 43);
            this.TextChoiceBatch.MenuManager = this.MainRibbon;
            this.TextChoiceBatch.Name = "TextChoiceBatch";
            this.TextChoiceBatch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TextChoiceBatch.Size = new System.Drawing.Size(1017, 20);
            this.TextChoiceBatch.StyleController = this.layoutControl2;
            this.TextChoiceBatch.TabIndex = 6;
            this.TextChoiceBatch.SelectedIndexChanged += new System.EventHandler(this.ChoiceBash_itemClick);
            // 
            // Count
            // 
            this.Count.Location = new System.Drawing.Point(24, 83);
            this.Count.MenuManager = this.MainRibbon;
            this.Count.Name = "Count";
            this.Count.Size = new System.Drawing.Size(129, 20);
            this.Count.StyleController = this.layoutControl2;
            this.Count.TabIndex = 8;
            // 
            // TextSum
            // 
            this.TextSum.Location = new System.Drawing.Point(157, 83);
            this.TextSum.Margin = new System.Windows.Forms.Padding(0);
            this.TextSum.MenuManager = this.MainRibbon;
            this.TextSum.Name = "TextSum";
            this.TextSum.Size = new System.Drawing.Size(130, 20);
            this.TextSum.StyleController = this.layoutControl2;
            this.TextSum.TabIndex = 9;
            // 
            // TextAvg
            // 
            this.TextAvg.Location = new System.Drawing.Point(291, 83);
            this.TextAvg.MenuManager = this.MainRibbon;
            this.TextAvg.Name = "TextAvg";
            this.TextAvg.Size = new System.Drawing.Size(129, 20);
            this.TextAvg.StyleController = this.layoutControl2;
            this.TextAvg.TabIndex = 10;
            // 
            // TextVar
            // 
            this.TextVar.Location = new System.Drawing.Point(825, 83);
            this.TextVar.MenuManager = this.MainRibbon;
            this.TextVar.Name = "TextVar";
            this.TextVar.Size = new System.Drawing.Size(129, 20);
            this.TextVar.StyleController = this.layoutControl2;
            this.TextVar.TabIndex = 15;
            // 
            // TextDev
            // 
            this.TextDev.Location = new System.Drawing.Point(691, 83);
            this.TextDev.MenuManager = this.MainRibbon;
            this.TextDev.Name = "TextDev";
            this.TextDev.Size = new System.Drawing.Size(130, 20);
            this.TextDev.StyleController = this.layoutControl2;
            this.TextDev.TabIndex = 14;
            // 
            // TextMax
            // 
            this.TextMax.Location = new System.Drawing.Point(558, 83);
            this.TextMax.MenuManager = this.MainRibbon;
            this.TextMax.Name = "TextMax";
            this.TextMax.Size = new System.Drawing.Size(129, 20);
            this.TextMax.StyleController = this.layoutControl2;
            this.TextMax.TabIndex = 13;
            // 
            // TextMin
            // 
            this.TextMin.Location = new System.Drawing.Point(424, 83);
            this.TextMin.MenuManager = this.MainRibbon;
            this.TextMin.Name = "TextMin";
            this.TextMin.Size = new System.Drawing.Size(130, 20);
            this.TextMin.StyleController = this.layoutControl2;
            this.TextMin.TabIndex = 12;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.GroupOnline});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1112, 331);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.GridOnline;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 107);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(1092, 204);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // GroupOnline
            // 
            this.GroupOnline.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem10,
            this.layoutControlItem12});
            this.GroupOnline.Location = new System.Drawing.Point(0, 0);
            this.GroupOnline.Name = "GroupOnline";
            this.GroupOnline.Size = new System.Drawing.Size(1092, 107);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.TextChoiceBatch;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(1068, 24);
            this.layoutControlItem13.Text = "Seria:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.Count;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(133, 40);
            this.layoutControlItem14.Text = "Ilość:";
            this.layoutControlItem14.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.TextSum;
            this.layoutControlItem15.Location = new System.Drawing.Point(133, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(134, 40);
            this.layoutControlItem15.Text = "Suma:";
            this.layoutControlItem15.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.TextAvg;
            this.layoutControlItem16.Location = new System.Drawing.Point(267, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(133, 40);
            this.layoutControlItem16.Text = "Avg:";
            this.layoutControlItem16.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.TextMin;
            this.layoutControlItem17.Location = new System.Drawing.Point(400, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(134, 40);
            this.layoutControlItem17.Text = "Min:";
            this.layoutControlItem17.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.TextMax;
            this.layoutControlItem18.Location = new System.Drawing.Point(534, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(133, 40);
            this.layoutControlItem18.Text = "Max:";
            this.layoutControlItem18.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.TextDev;
            this.layoutControlItem19.Location = new System.Drawing.Point(667, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(134, 40);
            this.layoutControlItem19.Text = "Dev:";
            this.layoutControlItem19.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.TextVar;
            this.layoutControlItem10.Location = new System.Drawing.Point(801, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(133, 40);
            this.layoutControlItem10.Text = "Var:";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.TextMedian;
            this.layoutControlItem12.Location = new System.Drawing.Point(934, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(134, 40);
            this.layoutControlItem12.Text = "Mediana:";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.dataLayoutControl1);
            this.layoutControl1.Controls.Add(this.GridDynamic);
            this.layoutControl1.Controls.Add(this.dataLayoutControl2);
            this.layoutControl1.Controls.Add(this.GridStatic);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 144);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(542, 404, 650, 400);
            this.layoutControl1.Root = this.LayoutMain;
            this.layoutControl1.Size = new System.Drawing.Size(1160, 379);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.GridDualDynamic);
            this.dataLayoutControl1.Controls.Add(this.GridDualStatic);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicMedian);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicStd);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicVar);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicAvg);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicMin);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicSum);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicCount);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticMedian);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticVar);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticMax);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticSum);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticStd);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticMin);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticAvg);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticCount);
            this.dataLayoutControl1.Controls.Add(this.ComboDualBatch);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicMax);
            this.dataLayoutControl1.Controls.Add(this.TextDualStaticPrecent);
            this.dataLayoutControl1.Controls.Add(this.TextDualDynamicPrecent);
            this.dataLayoutControl1.Location = new System.Drawing.Point(24, 24);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.DualControlGroup;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1112, 331);
            this.dataLayoutControl1.TabIndex = 18;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // GridDualDynamic
            // 
            this.GridDualDynamic.DataSource = this.rAkademiaDynamiczneBindingSource;
            this.GridDualDynamic.Location = new System.Drawing.Point(570, 147);
            this.GridDualDynamic.MainView = this.GridViewDualDynamic;
            this.GridDualDynamic.MenuManager = this.MainRibbon;
            this.GridDualDynamic.Name = "GridDualDynamic";
            this.GridDualDynamic.Size = new System.Drawing.Size(518, 160);
            this.GridDualDynamic.TabIndex = 22;
            this.GridDualDynamic.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewDualDynamic,
            this.gridView5});
            // 
            // GridViewDualDynamic
            // 
            this.GridViewDualDynamic.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colImieNazwisko4,
            this.colid4,
            this.colseria4,
            this.colnazwa4,
            this.colMasa4,
            this.colTara4,
            this.colDate4});
            this.GridViewDualDynamic.GridControl = this.GridDualDynamic;
            this.GridViewDualDynamic.GroupPanelText = " Przeciągnij i upuść kolumnę aby grupować";
            this.GridViewDualDynamic.Name = "GridViewDualDynamic";
            this.GridViewDualDynamic.OptionsBehavior.Editable = false;
            this.GridViewDualDynamic.OptionsView.ShowAutoFilterRow = true;
            // 
            // colImieNazwisko4
            // 
            this.colImieNazwisko4.FieldName = "ImieNazwisko";
            this.colImieNazwisko4.Name = "colImieNazwisko4";
            this.colImieNazwisko4.Visible = true;
            this.colImieNazwisko4.VisibleIndex = 1;
            // 
            // colid4
            // 
            this.colid4.Caption = "Id";
            this.colid4.FieldName = "id";
            this.colid4.Name = "colid4";
            this.colid4.Visible = true;
            this.colid4.VisibleIndex = 0;
            // 
            // colseria4
            // 
            this.colseria4.Caption = "Seria";
            this.colseria4.FieldName = "seria";
            this.colseria4.Name = "colseria4";
            this.colseria4.Visible = true;
            this.colseria4.VisibleIndex = 2;
            // 
            // colnazwa4
            // 
            this.colnazwa4.Caption = "Nazwa";
            this.colnazwa4.FieldName = "nazwa";
            this.colnazwa4.Name = "colnazwa4";
            this.colnazwa4.Visible = true;
            this.colnazwa4.VisibleIndex = 3;
            // 
            // colMasa4
            // 
            this.colMasa4.FieldName = "Masa";
            this.colMasa4.Name = "colMasa4";
            this.colMasa4.Visible = true;
            this.colMasa4.VisibleIndex = 6;
            // 
            // colTara4
            // 
            this.colTara4.Caption = "Tara";
            this.colTara4.FieldName = "Tara";
            this.colTara4.Name = "colTara4";
            this.colTara4.Visible = true;
            this.colTara4.VisibleIndex = 4;
            // 
            // colDate4
            // 
            this.colDate4.Caption = "Data";
            this.colDate4.FieldName = "Date";
            this.colDate4.Name = "colDate4";
            this.colDate4.Visible = true;
            this.colDate4.VisibleIndex = 5;
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.GridDualDynamic;
            this.gridView5.Name = "gridView5";
            // 
            // GridDualStatic
            // 
            this.GridDualStatic.DataSource = this.rAkademiaStatyczneBindingSource;
            this.GridDualStatic.Location = new System.Drawing.Point(24, 147);
            this.GridDualStatic.MainView = this.GridViewDualStatic;
            this.GridDualStatic.MenuManager = this.MainRibbon;
            this.GridDualStatic.Name = "GridDualStatic";
            this.GridDualStatic.Size = new System.Drawing.Size(518, 160);
            this.GridDualStatic.TabIndex = 21;
            this.GridDualStatic.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewDualStatic,
            this.gridView4});
            // 
            // GridViewDualStatic
            // 
            this.GridViewDualStatic.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colImieNazwisko3,
            this.colid3,
            this.colseria3,
            this.colnazwa3,
            this.colTara3,
            this.colMasa3,
            this.colDate3});
            this.GridViewDualStatic.GridControl = this.GridDualStatic;
            this.GridViewDualStatic.GroupPanelText = " Przeciągnij i upuść kolumnę aby grupować";
            this.GridViewDualStatic.Name = "GridViewDualStatic";
            this.GridViewDualStatic.OptionsBehavior.Editable = false;
            this.GridViewDualStatic.OptionsView.ShowAutoFilterRow = true;
            // 
            // colImieNazwisko3
            // 
            this.colImieNazwisko3.FieldName = "ImieNazwisko";
            this.colImieNazwisko3.Name = "colImieNazwisko3";
            this.colImieNazwisko3.Visible = true;
            this.colImieNazwisko3.VisibleIndex = 1;
            // 
            // colid3
            // 
            this.colid3.Caption = "Id";
            this.colid3.FieldName = "id";
            this.colid3.Name = "colid3";
            this.colid3.Visible = true;
            this.colid3.VisibleIndex = 0;
            // 
            // colseria3
            // 
            this.colseria3.Caption = "Seria";
            this.colseria3.FieldName = "seria";
            this.colseria3.Name = "colseria3";
            this.colseria3.Visible = true;
            this.colseria3.VisibleIndex = 2;
            // 
            // colnazwa3
            // 
            this.colnazwa3.Caption = "Nazwa";
            this.colnazwa3.FieldName = "nazwa";
            this.colnazwa3.Name = "colnazwa3";
            this.colnazwa3.Visible = true;
            this.colnazwa3.VisibleIndex = 3;
            // 
            // colTara3
            // 
            this.colTara3.Caption = "Tara";
            this.colTara3.FieldName = "Tara";
            this.colTara3.Name = "colTara3";
            this.colTara3.Visible = true;
            this.colTara3.VisibleIndex = 4;
            // 
            // colMasa3
            // 
            this.colMasa3.FieldName = "Masa";
            this.colMasa3.Name = "colMasa3";
            this.colMasa3.Visible = true;
            this.colMasa3.VisibleIndex = 6;
            // 
            // colDate3
            // 
            this.colDate3.Caption = "Data";
            this.colDate3.FieldName = "Date";
            this.colDate3.Name = "colDate3";
            this.colDate3.Visible = true;
            this.colDate3.VisibleIndex = 5;
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.GridDualStatic;
            this.gridView4.Name = "gridView4";
            // 
            // TextDualDynamicMedian
            // 
            this.TextDualDynamicMedian.Location = new System.Drawing.Point(1027, 123);
            this.TextDualDynamicMedian.MenuManager = this.MainRibbon;
            this.TextDualDynamicMedian.Name = "TextDualDynamicMedian";
            this.TextDualDynamicMedian.Size = new System.Drawing.Size(61, 20);
            this.TextDualDynamicMedian.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicMedian.TabIndex = 20;
            // 
            // TextDualDynamicStd
            // 
            this.TextDualDynamicStd.Location = new System.Drawing.Point(962, 123);
            this.TextDualDynamicStd.MenuManager = this.MainRibbon;
            this.TextDualDynamicStd.Name = "TextDualDynamicStd";
            this.TextDualDynamicStd.Size = new System.Drawing.Size(61, 20);
            this.TextDualDynamicStd.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicStd.TabIndex = 19;
            // 
            // TextDualDynamicVar
            // 
            this.TextDualDynamicVar.Location = new System.Drawing.Point(897, 123);
            this.TextDualDynamicVar.MenuManager = this.MainRibbon;
            this.TextDualDynamicVar.Name = "TextDualDynamicVar";
            this.TextDualDynamicVar.Size = new System.Drawing.Size(61, 20);
            this.TextDualDynamicVar.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicVar.TabIndex = 18;
            // 
            // TextDualDynamicAvg
            // 
            this.TextDualDynamicAvg.Location = new System.Drawing.Point(832, 123);
            this.TextDualDynamicAvg.MenuManager = this.MainRibbon;
            this.TextDualDynamicAvg.Name = "TextDualDynamicAvg";
            this.TextDualDynamicAvg.Size = new System.Drawing.Size(61, 20);
            this.TextDualDynamicAvg.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicAvg.TabIndex = 17;
            // 
            // TextDualDynamicMin
            // 
            this.TextDualDynamicMin.Location = new System.Drawing.Point(701, 123);
            this.TextDualDynamicMin.MenuManager = this.MainRibbon;
            this.TextDualDynamicMin.Name = "TextDualDynamicMin";
            this.TextDualDynamicMin.Size = new System.Drawing.Size(61, 20);
            this.TextDualDynamicMin.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicMin.TabIndex = 15;
            // 
            // TextDualDynamicSum
            // 
            this.TextDualDynamicSum.Location = new System.Drawing.Point(635, 123);
            this.TextDualDynamicSum.MenuManager = this.MainRibbon;
            this.TextDualDynamicSum.Name = "TextDualDynamicSum";
            this.TextDualDynamicSum.Size = new System.Drawing.Size(62, 20);
            this.TextDualDynamicSum.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicSum.TabIndex = 14;
            // 
            // TextDualDynamicCount
            // 
            this.TextDualDynamicCount.Location = new System.Drawing.Point(570, 123);
            this.TextDualDynamicCount.MenuManager = this.MainRibbon;
            this.TextDualDynamicCount.Name = "TextDualDynamicCount";
            this.TextDualDynamicCount.Size = new System.Drawing.Size(61, 20);
            this.TextDualDynamicCount.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicCount.TabIndex = 13;
            // 
            // TextDualStaticMedian
            // 
            this.TextDualStaticMedian.Location = new System.Drawing.Point(481, 123);
            this.TextDualStaticMedian.MenuManager = this.MainRibbon;
            this.TextDualStaticMedian.Name = "TextDualStaticMedian";
            this.TextDualStaticMedian.Size = new System.Drawing.Size(61, 20);
            this.TextDualStaticMedian.StyleController = this.dataLayoutControl1;
            this.TextDualStaticMedian.TabIndex = 12;
            // 
            // TextDualStaticVar
            // 
            this.TextDualStaticVar.Location = new System.Drawing.Point(351, 123);
            this.TextDualStaticVar.MenuManager = this.MainRibbon;
            this.TextDualStaticVar.Name = "TextDualStaticVar";
            this.TextDualStaticVar.Size = new System.Drawing.Size(61, 20);
            this.TextDualStaticVar.StyleController = this.dataLayoutControl1;
            this.TextDualStaticVar.TabIndex = 11;
            // 
            // TextDualStaticMax
            // 
            this.TextDualStaticMax.Location = new System.Drawing.Point(220, 123);
            this.TextDualStaticMax.MenuManager = this.MainRibbon;
            this.TextDualStaticMax.Name = "TextDualStaticMax";
            this.TextDualStaticMax.Size = new System.Drawing.Size(62, 20);
            this.TextDualStaticMax.StyleController = this.dataLayoutControl1;
            this.TextDualStaticMax.TabIndex = 10;
            // 
            // TextDualStaticSum
            // 
            this.TextDualStaticSum.Location = new System.Drawing.Point(89, 123);
            this.TextDualStaticSum.MenuManager = this.MainRibbon;
            this.TextDualStaticSum.Name = "TextDualStaticSum";
            this.TextDualStaticSum.Size = new System.Drawing.Size(62, 20);
            this.TextDualStaticSum.StyleController = this.dataLayoutControl1;
            this.TextDualStaticSum.TabIndex = 9;
            // 
            // TextDualStaticStd
            // 
            this.TextDualStaticStd.Location = new System.Drawing.Point(416, 123);
            this.TextDualStaticStd.MenuManager = this.MainRibbon;
            this.TextDualStaticStd.Name = "TextDualStaticStd";
            this.TextDualStaticStd.Size = new System.Drawing.Size(61, 20);
            this.TextDualStaticStd.StyleController = this.dataLayoutControl1;
            this.TextDualStaticStd.TabIndex = 8;
            // 
            // TextDualStaticMin
            // 
            this.TextDualStaticMin.Location = new System.Drawing.Point(155, 123);
            this.TextDualStaticMin.MenuManager = this.MainRibbon;
            this.TextDualStaticMin.Name = "TextDualStaticMin";
            this.TextDualStaticMin.Size = new System.Drawing.Size(61, 20);
            this.TextDualStaticMin.StyleController = this.dataLayoutControl1;
            this.TextDualStaticMin.TabIndex = 7;
            // 
            // TextDualStaticAvg
            // 
            this.TextDualStaticAvg.Location = new System.Drawing.Point(286, 123);
            this.TextDualStaticAvg.MenuManager = this.MainRibbon;
            this.TextDualStaticAvg.Name = "TextDualStaticAvg";
            this.TextDualStaticAvg.Size = new System.Drawing.Size(61, 20);
            this.TextDualStaticAvg.StyleController = this.dataLayoutControl1;
            this.TextDualStaticAvg.TabIndex = 6;
            // 
            // TextDualStaticCount
            // 
            this.TextDualStaticCount.Location = new System.Drawing.Point(24, 123);
            this.TextDualStaticCount.MenuManager = this.MainRibbon;
            this.TextDualStaticCount.Name = "TextDualStaticCount";
            this.TextDualStaticCount.Size = new System.Drawing.Size(61, 20);
            this.TextDualStaticCount.StyleController = this.dataLayoutControl1;
            this.TextDualStaticCount.TabIndex = 5;
            // 
            // ComboDualBatch
            // 
            this.ComboDualBatch.Location = new System.Drawing.Point(69, 12);
            this.ComboDualBatch.MenuManager = this.MainRibbon;
            this.ComboDualBatch.Name = "ComboDualBatch";
            this.ComboDualBatch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboDualBatch.Size = new System.Drawing.Size(1031, 20);
            this.ComboDualBatch.StyleController = this.dataLayoutControl1;
            this.ComboDualBatch.TabIndex = 4;
            this.ComboDualBatch.SelectedIndexChanged += new System.EventHandler(this.ComboDualBatch_SelectedIndexChanged);
            // 
            // TextDualDynamicMax
            // 
            this.TextDualDynamicMax.Location = new System.Drawing.Point(766, 123);
            this.TextDualDynamicMax.MenuManager = this.MainRibbon;
            this.TextDualDynamicMax.Name = "TextDualDynamicMax";
            this.TextDualDynamicMax.Size = new System.Drawing.Size(62, 20);
            this.TextDualDynamicMax.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicMax.TabIndex = 16;
            // 
            // TextDualStaticPrecent
            // 
            this.TextDualStaticPrecent.Location = new System.Drawing.Point(24, 83);
            this.TextDualStaticPrecent.Name = "TextDualStaticPrecent";
            this.TextDualStaticPrecent.Size = new System.Drawing.Size(518, 20);
            this.TextDualStaticPrecent.StyleController = this.dataLayoutControl1;
            this.TextDualStaticPrecent.TabIndex = 5;
            // 
            // TextDualDynamicPrecent
            // 
            this.TextDualDynamicPrecent.Location = new System.Drawing.Point(570, 83);
            this.TextDualDynamicPrecent.Name = "TextDualDynamicPrecent";
            this.TextDualDynamicPrecent.Size = new System.Drawing.Size(518, 20);
            this.TextDualDynamicPrecent.StyleController = this.dataLayoutControl1;
            this.TextDualDynamicPrecent.TabIndex = 5;
            // 
            // DualControlGroup
            // 
            this.DualControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.DualControlGroup.GroupBordersVisible = false;
            this.DualControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.GroupStatic,
            this.GroupDynamic});
            this.DualControlGroup.Name = "Root";
            this.DualControlGroup.Size = new System.Drawing.Size(1112, 331);
            this.DualControlGroup.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ComboDualBatch;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(1092, 24);
            this.layoutControlItem6.Text = "Seria:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(54, 13);
            // 
            // GroupStatic
            // 
            this.GroupStatic.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LabelDualStaticMedian,
            this.LabelDualStaticStd,
            this.LabelDualStaticVar,
            this.LabelDualStaticAvg,
            this.LabelDualStaticMax,
            this.LabelDualStaticMin,
            this.LabelDualStaticSum,
            this.LabelDualStaticCount,
            this.layoutControlItem4,
            this.LabelDualStaticCount1});
            this.GroupStatic.Location = new System.Drawing.Point(0, 24);
            this.GroupStatic.Name = "GroupStatic";
            this.GroupStatic.Size = new System.Drawing.Size(546, 287);
            this.GroupStatic.Text = "Statyczne";
            // 
            // LabelDualStaticMedian
            // 
            this.LabelDualStaticMedian.Control = this.TextDualStaticMedian;
            this.LabelDualStaticMedian.Location = new System.Drawing.Point(457, 40);
            this.LabelDualStaticMedian.Name = "LabelDualStaticMedian";
            this.LabelDualStaticMedian.Size = new System.Drawing.Size(65, 40);
            this.LabelDualStaticMedian.Text = "Mediana:";
            this.LabelDualStaticMedian.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticMedian.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualStaticStd
            // 
            this.LabelDualStaticStd.Control = this.TextDualStaticStd;
            this.LabelDualStaticStd.Location = new System.Drawing.Point(392, 40);
            this.LabelDualStaticStd.Name = "LabelDualStaticStd";
            this.LabelDualStaticStd.Size = new System.Drawing.Size(65, 40);
            this.LabelDualStaticStd.Text = "Std:";
            this.LabelDualStaticStd.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticStd.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualStaticVar
            // 
            this.LabelDualStaticVar.Control = this.TextDualStaticVar;
            this.LabelDualStaticVar.Location = new System.Drawing.Point(327, 40);
            this.LabelDualStaticVar.Name = "LabelDualStaticVar";
            this.LabelDualStaticVar.Size = new System.Drawing.Size(65, 40);
            this.LabelDualStaticVar.Text = "Var:";
            this.LabelDualStaticVar.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticVar.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualStaticAvg
            // 
            this.LabelDualStaticAvg.Control = this.TextDualStaticAvg;
            this.LabelDualStaticAvg.Location = new System.Drawing.Point(262, 40);
            this.LabelDualStaticAvg.Name = "LabelDualStaticAvg";
            this.LabelDualStaticAvg.Size = new System.Drawing.Size(65, 40);
            this.LabelDualStaticAvg.Text = "Avg:";
            this.LabelDualStaticAvg.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticAvg.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualStaticMax
            // 
            this.LabelDualStaticMax.Control = this.TextDualStaticMax;
            this.LabelDualStaticMax.Location = new System.Drawing.Point(196, 40);
            this.LabelDualStaticMax.Name = "LabelDualStaticMax";
            this.LabelDualStaticMax.Size = new System.Drawing.Size(66, 40);
            this.LabelDualStaticMax.Text = "Max:";
            this.LabelDualStaticMax.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticMax.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualStaticMin
            // 
            this.LabelDualStaticMin.Control = this.TextDualStaticMin;
            this.LabelDualStaticMin.Location = new System.Drawing.Point(131, 40);
            this.LabelDualStaticMin.Name = "LabelDualStaticMin";
            this.LabelDualStaticMin.Size = new System.Drawing.Size(65, 40);
            this.LabelDualStaticMin.Text = "Min:";
            this.LabelDualStaticMin.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticMin.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualStaticSum
            // 
            this.LabelDualStaticSum.Control = this.TextDualStaticSum;
            this.LabelDualStaticSum.Location = new System.Drawing.Point(65, 40);
            this.LabelDualStaticSum.Name = "LabelDualStaticSum";
            this.LabelDualStaticSum.Size = new System.Drawing.Size(66, 40);
            this.LabelDualStaticSum.Text = "Suma:";
            this.LabelDualStaticSum.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticSum.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualStaticCount
            // 
            this.LabelDualStaticCount.Control = this.TextDualStaticCount;
            this.LabelDualStaticCount.Location = new System.Drawing.Point(0, 40);
            this.LabelDualStaticCount.Name = "LabelDualStaticCount";
            this.LabelDualStaticCount.Size = new System.Drawing.Size(65, 40);
            this.LabelDualStaticCount.Text = "Ilość:";
            this.LabelDualStaticCount.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticCount.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.GridDualStatic;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(522, 164);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // LabelDualStaticCount1
            // 
            this.LabelDualStaticCount1.Control = this.TextDualStaticPrecent;
            this.LabelDualStaticCount1.CustomizationFormText = "Ilość:";
            this.LabelDualStaticCount1.Location = new System.Drawing.Point(0, 0);
            this.LabelDualStaticCount1.Name = "LabelDualStaticCount1";
            this.LabelDualStaticCount1.Size = new System.Drawing.Size(522, 40);
            this.LabelDualStaticCount1.Text = "Zawartość:";
            this.LabelDualStaticCount1.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticCount1.TextSize = new System.Drawing.Size(54, 13);
            // 
            // GroupDynamic
            // 
            this.GroupDynamic.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LabelDualDynamicCount,
            this.LabelDualDynamicSum,
            this.LabelDualDynamicMin,
            this.LabelDualDynamicMax,
            this.LabelDualDynamicAvg,
            this.LabelDualDynamicVar,
            this.LabelDualDynamicStd,
            this.LabelDualDynamicMedian,
            this.layoutControlItem7,
            this.LabelDualStaticCount2});
            this.GroupDynamic.Location = new System.Drawing.Point(546, 24);
            this.GroupDynamic.Name = "GroupDynamic";
            this.GroupDynamic.Size = new System.Drawing.Size(546, 287);
            this.GroupDynamic.Text = "Dynamiczne";
            // 
            // LabelDualDynamicCount
            // 
            this.LabelDualDynamicCount.Control = this.TextDualDynamicCount;
            this.LabelDualDynamicCount.Location = new System.Drawing.Point(0, 40);
            this.LabelDualDynamicCount.Name = "LabelDualDynamicCount";
            this.LabelDualDynamicCount.Size = new System.Drawing.Size(65, 40);
            this.LabelDualDynamicCount.Text = "Ilość:";
            this.LabelDualDynamicCount.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicCount.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualDynamicSum
            // 
            this.LabelDualDynamicSum.Control = this.TextDualDynamicSum;
            this.LabelDualDynamicSum.Location = new System.Drawing.Point(65, 40);
            this.LabelDualDynamicSum.Name = "LabelDualDynamicSum";
            this.LabelDualDynamicSum.Size = new System.Drawing.Size(66, 40);
            this.LabelDualDynamicSum.Text = "Suma:";
            this.LabelDualDynamicSum.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicSum.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualDynamicMin
            // 
            this.LabelDualDynamicMin.Control = this.TextDualDynamicMin;
            this.LabelDualDynamicMin.Location = new System.Drawing.Point(131, 40);
            this.LabelDualDynamicMin.Name = "LabelDualDynamicMin";
            this.LabelDualDynamicMin.Size = new System.Drawing.Size(65, 40);
            this.LabelDualDynamicMin.Text = "Min:";
            this.LabelDualDynamicMin.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicMin.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualDynamicMax
            // 
            this.LabelDualDynamicMax.Control = this.TextDualDynamicMax;
            this.LabelDualDynamicMax.Location = new System.Drawing.Point(196, 40);
            this.LabelDualDynamicMax.Name = "LabelDualDynamicMax";
            this.LabelDualDynamicMax.Size = new System.Drawing.Size(66, 40);
            this.LabelDualDynamicMax.Text = "Max:";
            this.LabelDualDynamicMax.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicMax.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualDynamicAvg
            // 
            this.LabelDualDynamicAvg.Control = this.TextDualDynamicAvg;
            this.LabelDualDynamicAvg.Location = new System.Drawing.Point(262, 40);
            this.LabelDualDynamicAvg.Name = "LabelDualDynamicAvg";
            this.LabelDualDynamicAvg.Size = new System.Drawing.Size(65, 40);
            this.LabelDualDynamicAvg.Text = "Avg:";
            this.LabelDualDynamicAvg.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicAvg.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualDynamicVar
            // 
            this.LabelDualDynamicVar.Control = this.TextDualDynamicVar;
            this.LabelDualDynamicVar.Location = new System.Drawing.Point(327, 40);
            this.LabelDualDynamicVar.Name = "LabelDualDynamicVar";
            this.LabelDualDynamicVar.Size = new System.Drawing.Size(65, 40);
            this.LabelDualDynamicVar.Text = "Var:";
            this.LabelDualDynamicVar.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicVar.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualDynamicStd
            // 
            this.LabelDualDynamicStd.Control = this.TextDualDynamicStd;
            this.LabelDualDynamicStd.Location = new System.Drawing.Point(392, 40);
            this.LabelDualDynamicStd.Name = "LabelDualDynamicStd";
            this.LabelDualDynamicStd.Size = new System.Drawing.Size(65, 40);
            this.LabelDualDynamicStd.Text = "Std:";
            this.LabelDualDynamicStd.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicStd.TextSize = new System.Drawing.Size(54, 13);
            // 
            // LabelDualDynamicMedian
            // 
            this.LabelDualDynamicMedian.Control = this.TextDualDynamicMedian;
            this.LabelDualDynamicMedian.Location = new System.Drawing.Point(457, 40);
            this.LabelDualDynamicMedian.Name = "LabelDualDynamicMedian";
            this.LabelDualDynamicMedian.Size = new System.Drawing.Size(65, 40);
            this.LabelDualDynamicMedian.Text = "Mediana:";
            this.LabelDualDynamicMedian.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualDynamicMedian.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.GridDualDynamic;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(522, 164);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // LabelDualStaticCount2
            // 
            this.LabelDualStaticCount2.Control = this.TextDualDynamicPrecent;
            this.LabelDualStaticCount2.CustomizationFormText = "Ilość:";
            this.LabelDualStaticCount2.Location = new System.Drawing.Point(0, 0);
            this.LabelDualStaticCount2.Name = "LabelDualStaticCount2";
            this.LabelDualStaticCount2.Size = new System.Drawing.Size(522, 40);
            this.LabelDualStaticCount2.Text = "Zawartość:";
            this.LabelDualStaticCount2.TextLocation = DevExpress.Utils.Locations.Top;
            this.LabelDualStaticCount2.TextSize = new System.Drawing.Size(54, 13);
            // 
            // GridDynamic
            // 
            this.GridDynamic.DataSource = this.rAkademiaDynamiczneBindingSource;
            this.GridDynamic.Location = new System.Drawing.Point(24, 24);
            this.GridDynamic.MainView = this.GridViewDynamic;
            this.GridDynamic.MenuManager = this.MainRibbon;
            this.GridDynamic.Name = "GridDynamic";
            this.GridDynamic.Size = new System.Drawing.Size(1112, 331);
            this.GridDynamic.TabIndex = 5;
            this.GridDynamic.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridViewDynamic,
            this.gridView3});
            // 
            // GridViewDynamic
            // 
            this.GridViewDynamic.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colImieNazwisko2,
            this.colid2,
            this.colseria2,
            this.colnazwa2,
            this.colMasa1,
            this.colTara2,
            this.colDate2});
            this.GridViewDynamic.GridControl = this.GridDynamic;
            this.GridViewDynamic.GroupPanelText = " Przeciągnij i upuść kolumnę aby grupować";
            this.GridViewDynamic.Name = "GridViewDynamic";
            this.GridViewDynamic.OptionsBehavior.Editable = false;
            this.GridViewDynamic.OptionsView.ShowAutoFilterRow = true;
            // 
            // colImieNazwisko2
            // 
            this.colImieNazwisko2.FieldName = "ImieNazwisko";
            this.colImieNazwisko2.Name = "colImieNazwisko2";
            this.colImieNazwisko2.Visible = true;
            this.colImieNazwisko2.VisibleIndex = 1;
            // 
            // colid2
            // 
            this.colid2.Caption = "Id";
            this.colid2.FieldName = "id";
            this.colid2.Name = "colid2";
            this.colid2.Visible = true;
            this.colid2.VisibleIndex = 0;
            // 
            // colseria2
            // 
            this.colseria2.Caption = "Seria";
            this.colseria2.FieldName = "seria";
            this.colseria2.Name = "colseria2";
            this.colseria2.Visible = true;
            this.colseria2.VisibleIndex = 2;
            // 
            // colnazwa2
            // 
            this.colnazwa2.Caption = "Nazwa";
            this.colnazwa2.FieldName = "nazwa";
            this.colnazwa2.Name = "colnazwa2";
            this.colnazwa2.Visible = true;
            this.colnazwa2.VisibleIndex = 3;
            // 
            // colMasa1
            // 
            this.colMasa1.FieldName = "Masa";
            this.colMasa1.Name = "colMasa1";
            this.colMasa1.Visible = true;
            this.colMasa1.VisibleIndex = 6;
            // 
            // colTara2
            // 
            this.colTara2.FieldName = "Tara";
            this.colTara2.Name = "colTara2";
            this.colTara2.Visible = true;
            this.colTara2.VisibleIndex = 4;
            // 
            // colDate2
            // 
            this.colDate2.Caption = "Data";
            this.colDate2.FieldName = "Date";
            this.colDate2.Name = "colDate2";
            this.colDate2.Visible = true;
            this.colDate2.VisibleIndex = 5;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.GridDynamic;
            this.gridView3.Name = "gridView3";
            // 
            // dataLayoutControl2
            // 
            this.dataLayoutControl2.Controls.Add(this.ButtonSave);
            this.dataLayoutControl2.Controls.Add(this.TextUserPassword);
            this.dataLayoutControl2.Controls.Add(this.TextUserName);
            this.dataLayoutControl2.Controls.Add(this.ComboSerwer);
            this.dataLayoutControl2.Controls.Add(this.ButtonTest);
            this.dataLayoutControl2.Controls.Add(this.ComboDataBase);
            this.dataLayoutControl2.Location = new System.Drawing.Point(24, 24);
            this.dataLayoutControl2.Name = "dataLayoutControl2";
            this.dataLayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(808, 220, 650, 400);
            this.dataLayoutControl2.Root = this.layoutControlGroup1;
            this.dataLayoutControl2.Size = new System.Drawing.Size(1112, 331);
            this.dataLayoutControl2.TabIndex = 19;
            this.dataLayoutControl2.Text = "dataLayoutControl2";
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(12, 134);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(98, 22);
            this.ButtonSave.StyleController = this.dataLayoutControl2;
            this.ButtonSave.TabIndex = 8;
            this.ButtonSave.Text = "Save Settings";
            this.ButtonSave.Click += new System.EventHandler(this.SimpleButton1_Click);
            // 
            // TextUserPassword
            // 
            this.TextUserPassword.Location = new System.Drawing.Point(113, 60);
            this.TextUserPassword.MenuManager = this.MainRibbon;
            this.TextUserPassword.Name = "TextUserPassword";
            this.TextUserPassword.Properties.PasswordChar = '*';
            this.TextUserPassword.Properties.UseSystemPasswordChar = true;
            this.TextUserPassword.Size = new System.Drawing.Size(987, 20);
            this.TextUserPassword.StyleController = this.dataLayoutControl2;
            this.TextUserPassword.TabIndex = 7;
            // 
            // TextUserName
            // 
            this.TextUserName.Location = new System.Drawing.Point(113, 36);
            this.TextUserName.MenuManager = this.MainRibbon;
            this.TextUserName.Name = "TextUserName";
            this.TextUserName.Size = new System.Drawing.Size(987, 20);
            this.TextUserName.StyleController = this.dataLayoutControl2;
            this.TextUserName.TabIndex = 6;
            // 
            // ComboSerwer
            // 
            this.ComboSerwer.Location = new System.Drawing.Point(113, 12);
            this.ComboSerwer.MenuManager = this.MainRibbon;
            this.ComboSerwer.Name = "ComboSerwer";
            this.ComboSerwer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboSerwer.Size = new System.Drawing.Size(987, 20);
            this.ComboSerwer.StyleController = this.dataLayoutControl2;
            this.ComboSerwer.TabIndex = 9;
            // 
            // ButtonTest
            // 
            this.ButtonTest.Location = new System.Drawing.Point(12, 84);
            this.ButtonTest.Name = "ButtonTest";
            this.ButtonTest.Size = new System.Drawing.Size(98, 22);
            this.ButtonTest.StyleController = this.dataLayoutControl2;
            this.ButtonTest.TabIndex = 10;
            this.ButtonTest.Text = "TestPołączenia";
            this.ButtonTest.Click += new System.EventHandler(this.ButtonTest_Click);
            // 
            // ComboDataBase
            // 
            this.ComboDataBase.Location = new System.Drawing.Point(113, 110);
            this.ComboDataBase.MenuManager = this.MainRibbon;
            this.ComboDataBase.Name = "ComboDataBase";
            this.ComboDataBase.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboDataBase.Size = new System.Drawing.Size(987, 20);
            this.ComboDataBase.StyleController = this.dataLayoutControl2;
            this.ComboDataBase.TabIndex = 11;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LabelUserName,
            this.LabelUserPassword,
            this.LabelSerwer,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.layoutControlItem21,
            this.emptySpaceItem1,
            this.layoutControlItem22});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1112, 331);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // LabelUserName
            // 
            this.LabelUserName.Control = this.TextUserName;
            this.LabelUserName.Location = new System.Drawing.Point(0, 24);
            this.LabelUserName.Name = "LabelUserName";
            this.LabelUserName.Size = new System.Drawing.Size(1092, 24);
            this.LabelUserName.Text = "Nazwa użytkownika:";
            this.LabelUserName.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LabelUserPassword
            // 
            this.LabelUserPassword.Control = this.TextUserPassword;
            this.LabelUserPassword.Location = new System.Drawing.Point(0, 48);
            this.LabelUserPassword.Name = "LabelUserPassword";
            this.LabelUserPassword.Size = new System.Drawing.Size(1092, 24);
            this.LabelUserPassword.Text = "Hasło użytkownika:";
            this.LabelUserPassword.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LabelSerwer
            // 
            this.LabelSerwer.Control = this.ComboSerwer;
            this.LabelSerwer.Location = new System.Drawing.Point(0, 0);
            this.LabelSerwer.Name = "LabelSerwer";
            this.LabelSerwer.Size = new System.Drawing.Size(1092, 24);
            this.LabelSerwer.Text = "Serwer SQL:";
            this.LabelSerwer.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.ButtonSave;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(102, 189);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(102, 122);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(990, 189);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.ButtonTest;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(102, 26);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(102, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(990, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.ComboDataBase;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(1092, 24);
            this.layoutControlItem22.Text = "Baza danych:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutMain
            // 
            this.LayoutMain.CustomizationFormText = "Raport";
            this.LayoutMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutMain.GroupBordersVisible = false;
            this.LayoutMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.TabbedMainGroup});
            this.LayoutMain.Name = "LayoutMain";
            this.LayoutMain.Size = new System.Drawing.Size(1160, 379);
            this.LayoutMain.TextVisible = false;
            // 
            // TabbedMainGroup
            // 
            this.TabbedMainGroup.Location = new System.Drawing.Point(0, 0);
            this.TabbedMainGroup.Name = "TabbedMainGroup";
            this.TabbedMainGroup.SelectedTabPage = this.layoutDataBaseSettings;
            this.TabbedMainGroup.SelectedTabPageIndex = 4;
            this.TabbedMainGroup.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.TabbedMainGroup.Size = new System.Drawing.Size(1140, 359);
            this.TabbedMainGroup.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Static,
            this.WeighingOnline,
            this.Dynamic,
            this.WeightingDual,
            this.layoutDataBaseSettings});
            // 
            // layoutDataBaseSettings
            // 
            this.layoutDataBaseSettings.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.layoutDataBaseSettings.Location = new System.Drawing.Point(0, 0);
            this.layoutDataBaseSettings.Name = "layoutDataBaseSettings";
            this.layoutDataBaseSettings.Size = new System.Drawing.Size(1116, 335);
            this.layoutDataBaseSettings.Text = "DataBaseSeting";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.dataLayoutControl2;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1116, 335);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // Static
            // 
            this.Static.AppearanceTabPage.HeaderDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Static.AppearanceTabPage.HeaderDisabled.Options.UseBackColor = true;
            this.Static.CustomizationFormText = "Statyczne";
            this.Static.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Static.Location = new System.Drawing.Point(0, 0);
            this.Static.Name = "Static";
            this.Static.Size = new System.Drawing.Size(1116, 335);
            this.Static.Text = "Statyczne";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.GridStatic;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1116, 335);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // WeighingOnline
            // 
            this.WeighingOnline.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.WeighingOnline.Location = new System.Drawing.Point(0, 0);
            this.WeighingOnline.Name = "WeighingOnline";
            this.WeighingOnline.Size = new System.Drawing.Size(1116, 335);
            this.WeighingOnline.Text = "WazenieOnline";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.layoutControl2;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1116, 335);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // Dynamic
            // 
            this.Dynamic.CustomizationFormText = "Dynamic";
            this.Dynamic.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.Dynamic.Location = new System.Drawing.Point(0, 0);
            this.Dynamic.Name = "Dynamic";
            this.Dynamic.Size = new System.Drawing.Size(1116, 335);
            this.Dynamic.Text = "Dynamiczne";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.GridDynamic;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1116, 335);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // WeightingDual
            // 
            this.WeightingDual.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.WeightingDual.Location = new System.Drawing.Point(0, 0);
            this.WeightingDual.Name = "WeightingDual";
            this.WeightingDual.Size = new System.Drawing.Size(1116, 335);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dataLayoutControl1;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1116, 335);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // LabelVar
            // 
            this.LabelVar.Control = this.TextVar;
            this.LabelVar.Location = new System.Drawing.Point(834, 24);
            this.LabelVar.Name = "LabelVar";
            this.LabelVar.Size = new System.Drawing.Size(139, 24);
            this.LabelVar.Text = "VAR:";
            this.LabelVar.TextSize = new System.Drawing.Size(82, 13);
            // 
            // ChoiceBatch
            // 
            this.ChoiceBatch.Control = this.TextChoiceBatch;
            this.ChoiceBatch.Location = new System.Drawing.Point(0, 0);
            this.ChoiceBatch.Name = "ChoiceBatch";
            this.ChoiceBatch.Size = new System.Drawing.Size(1112, 24);
            this.ChoiceBatch.Text = "Seria";
            this.ChoiceBatch.TextSize = new System.Drawing.Size(82, 13);
            // 
            // colImieNazwisko1
            // 
            this.colImieNazwisko1.FieldName = "ImieNazwisko";
            this.colImieNazwisko1.Name = "colImieNazwisko1";
            this.colImieNazwisko1.Visible = true;
            this.colImieNazwisko1.VisibleIndex = 1;
            // 
            // colid1
            // 
            this.colid1.Caption = "Id";
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            this.colid1.Visible = true;
            this.colid1.VisibleIndex = 0;
            // 
            // colseria1
            // 
            this.colseria1.Caption = "Seria";
            this.colseria1.FieldName = "seria";
            this.colseria1.Name = "colseria1";
            this.colseria1.Visible = true;
            this.colseria1.VisibleIndex = 2;
            // 
            // colnazwa1
            // 
            this.colnazwa1.Caption = "Nazwa";
            this.colnazwa1.FieldName = "nazwa";
            this.colnazwa1.Name = "colnazwa1";
            this.colnazwa1.Visible = true;
            this.colnazwa1.VisibleIndex = 3;
            // 
            // colMasa2
            // 
            this.colMasa2.FieldName = "Masa";
            this.colMasa2.Name = "colMasa2";
            this.colMasa2.Visible = true;
            this.colMasa2.VisibleIndex = 6;
            // 
            // colTara1
            // 
            this.colTara1.Caption = "Tara";
            this.colTara1.FieldName = "Tara";
            this.colTara1.Name = "colTara1";
            this.colTara1.Visible = true;
            this.colTara1.VisibleIndex = 4;
            // 
            // colDate1
            // 
            this.colDate1.Caption = "Data";
            this.colDate1.FieldName = "Date";
            this.colDate1.Name = "colDate1";
            this.colDate1.Visible = true;
            this.colDate1.VisibleIndex = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(50, 20);
            // 
            // MainWindow
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.True;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 523);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.MainRibbon);
            this.IsMdiContainer = true;
            this.Name = "MainWindow";
            this.Ribbon = this.MainRibbon;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RAkademia";
            ((System.ComponentModel.ISupportInitialize)(this.MainRibbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAkademiaDynamiczneBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rAkademiaStatyczneBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStatic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewStatic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelMedian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMedian.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridOnline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewOnline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextChoiceBatch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Count.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextAvg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextVar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDev.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupOnline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridDualDynamic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDualDynamic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDualStatic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDualStatic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicMedian.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicStd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicVar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicAvg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicSum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticMedian.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticVar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticSum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticStd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticAvg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboDualBatch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualStaticPrecent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextDualDynamicPrecent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DualControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupStatic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticMedian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticStd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticVar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticAvg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticSum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupDynamic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicSum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicAvg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicVar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicStd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualDynamicMedian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelDualStaticCount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDynamic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewDynamic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl2)).EndInit();
            this.dataLayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TextUserPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboSerwer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboDataBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelUserPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelSerwer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedMainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDataBaseSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Static)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeighingOnline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dynamic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeightingDual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelVar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChoiceBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl MainRibbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage Ribbon1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup RibbonGroupWeighting;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeelManager;
        private DevExpress.XtraBars.BarButtonItem ButtonDynamic;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup RibbonGroupReport;
        private DevExpress.XtraBars.BarButtonItem ButtonUpdate;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonGroupData;
        private DevExpress.XtraBars.BarListItem ButtonReportList;
        private DevExpress.XtraBars.BarButtonItem ButtonStaticOnline;
        private DevExpress.XtraBars.BarButtonItem ButtonDynamicOnline;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarButtonItem ButtonDualOnline;
        private DevExpress.XtraBars.BarButtonItem ButtonDataBase;
        private System.Windows.Forms.BindingSource rAkademiaDynamiczneBindingSource;
        private System.Windows.Forms.BindingSource rAkademiaStatyczneBindingSource;
        public DevExpress.XtraBars.BarButtonItem ButtonStatic;
        private DevExpress.XtraGrid.GridControl GridStatic;
        private DevExpress.XtraGrid.Views.Grid.GridView GridViewStatic;
        private DevExpress.XtraGrid.Columns.GridColumn colImieNazwisko;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colseria;
        private DevExpress.XtraGrid.Columns.GridColumn colnazwa;
        private DevExpress.XtraGrid.Columns.GridColumn colMasa;
        private DevExpress.XtraGrid.Columns.GridColumn colTara;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem LabelMedian;
        private DevExpress.XtraEditors.TextEdit TextMedian;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl GridOnline;
        private DevExpress.XtraGrid.Views.Grid.GridView GridViewOnline;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraGrid.GridControl GridDualDynamic;
        private DevExpress.XtraGrid.Views.Grid.GridView GridViewDualDynamic;
        private DevExpress.XtraGrid.Columns.GridColumn colImieNazwisko4;
        private DevExpress.XtraGrid.Columns.GridColumn colid4;
        private DevExpress.XtraGrid.Columns.GridColumn colseria4;
        private DevExpress.XtraGrid.Columns.GridColumn colnazwa4;
        private DevExpress.XtraGrid.Columns.GridColumn colMasa4;
        private DevExpress.XtraGrid.Columns.GridColumn colTara4;
        private DevExpress.XtraGrid.Columns.GridColumn colDate4;
        private DevExpress.XtraGrid.GridControl GridDualStatic;
        private DevExpress.XtraGrid.Views.Grid.GridView GridViewDualStatic;
        private DevExpress.XtraGrid.Columns.GridColumn colImieNazwisko3;
        private DevExpress.XtraGrid.Columns.GridColumn colid3;
        private DevExpress.XtraGrid.Columns.GridColumn colseria3;
        private DevExpress.XtraGrid.Columns.GridColumn colnazwa3;
        private DevExpress.XtraGrid.Columns.GridColumn colTara3;
        private DevExpress.XtraGrid.Columns.GridColumn colMasa3;
        private DevExpress.XtraGrid.Columns.GridColumn colDate3;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicMedian;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicStd;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicVar;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicAvg;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicMin;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicSum;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicCount;
        private DevExpress.XtraEditors.TextEdit TextDualStaticMedian;
        private DevExpress.XtraEditors.TextEdit TextDualStaticVar;
        private DevExpress.XtraEditors.TextEdit TextDualStaticMax;
        private DevExpress.XtraEditors.TextEdit TextDualStaticSum;
        private DevExpress.XtraEditors.TextEdit TextDualStaticStd;
        private DevExpress.XtraEditors.TextEdit TextDualStaticMin;
        private DevExpress.XtraEditors.TextEdit TextDualStaticAvg;
        private DevExpress.XtraEditors.TextEdit TextDualStaticCount;
        private DevExpress.XtraEditors.ComboBoxEdit ComboDualBatch;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicMax;
        private DevExpress.XtraEditors.TextEdit TextDualStaticPrecent;
        private DevExpress.XtraEditors.TextEdit TextDualDynamicPrecent;
        private DevExpress.XtraLayout.LayoutControlGroup DualControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup GroupStatic;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticMedian;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticStd;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticVar;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticAvg;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticMax;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticMin;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticSum;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticCount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticCount1;
        private DevExpress.XtraLayout.LayoutControlGroup GroupDynamic;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicCount;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicSum;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicMin;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicMax;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicAvg;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicVar;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicStd;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualDynamicMedian;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem LabelDualStaticCount2;
        private DevExpress.XtraGrid.GridControl GridDynamic;
        private DevExpress.XtraGrid.Views.Grid.GridView GridViewDynamic;
        private DevExpress.XtraGrid.Columns.GridColumn colImieNazwisko2;
        private DevExpress.XtraGrid.Columns.GridColumn colid2;
        private DevExpress.XtraGrid.Columns.GridColumn colseria2;
        private DevExpress.XtraGrid.Columns.GridColumn colnazwa2;
        private DevExpress.XtraGrid.Columns.GridColumn colMasa1;
        private DevExpress.XtraGrid.Columns.GridColumn colTara2;
        private DevExpress.XtraGrid.Columns.GridColumn colDate2;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl2;
        private DevExpress.XtraEditors.SimpleButton ButtonSave;
        private DevExpress.XtraEditors.TextEdit TextUserPassword;
        private DevExpress.XtraEditors.TextEdit TextUserName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem LabelUserName;
        private DevExpress.XtraLayout.LayoutControlItem LabelUserPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit Count;
        private DevExpress.XtraEditors.TextEdit TextSum;
        private DevExpress.XtraEditors.TextEdit TextAvg;
        private DevExpress.XtraEditors.TextEdit TextMin;
        private DevExpress.XtraEditors.TextEdit TextMax;
        private DevExpress.XtraEditors.TextEdit TextDev;
        private DevExpress.XtraEditors.TextEdit TextVar;
        private DevExpress.XtraEditors.ComboBoxEdit TextChoiceBatch;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutMain;
        private DevExpress.XtraLayout.TabbedControlGroup TabbedMainGroup;
        private DevExpress.XtraLayout.LayoutControlGroup WeighingOnline;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup Static;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup Dynamic;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup WeightingDual;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutDataBaseSettings;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem LabelVar;
        private DevExpress.XtraLayout.LayoutControlItem ChoiceBatch;
        private DevExpress.XtraGrid.Columns.GridColumn colImieNazwisko1;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colseria1;
        private DevExpress.XtraGrid.Columns.GridColumn colnazwa1;
        private DevExpress.XtraGrid.Columns.GridColumn colMasa2;
        private DevExpress.XtraGrid.Columns.GridColumn colTara1;
        private DevExpress.XtraGrid.Columns.GridColumn colDate1;
        private DevExpress.XtraLayout.LayoutControlGroup GroupOnline;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.Columns.GridColumn colImieNazwisko5;
        private DevExpress.XtraGrid.Columns.GridColumn colid5;
        private DevExpress.XtraGrid.Columns.GridColumn colseria5;
        private DevExpress.XtraGrid.Columns.GridColumn colnazwa5;
        private DevExpress.XtraGrid.Columns.GridColumn colTara5;
        private DevExpress.XtraGrid.Columns.GridColumn colDate5;
        private DevExpress.XtraGrid.Columns.GridColumn colNetto_mass;
        private DevExpress.XtraBars.BarListItem ButtonCreateReportList;
        private DevExpress.XtraEditors.ComboBoxEdit ComboSerwer;
        private DevExpress.XtraLayout.LayoutControlItem LabelSerwer;
        private DevExpress.XtraEditors.SimpleButton ButtonTest;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.ComboBoxEdit ComboDataBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
    }
}